# Face G crossing F

- Given edge E of G embeds in F
  - Discard **if** the verticies before and after E are outside F

- Given edge E of G crosses F at a vertex V of G
  - Discard **if** the vertices before and after V are outside F

- Given edge E crosses F at point J (within E) which is on the perimeter of F
  - Discard **if** the both vertices of E are outside F
  - Discard **if** E exits F at J from V to U, V is on the plane of F and U is outside F
  - Discard **if** E enters F at J from V to U, U is on the plane of F and V is outside F