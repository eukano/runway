package interpreter

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"path"
	"reflect"
	"strings"

	"github.com/traefik/yaegi/interp"
	"github.com/traefik/yaegi/stdlib"
	"gitlab.com/eukano/runway/internal/geom"
	"gitlab.com/eukano/runway/internal/model"
	"gitlab.com/eukano/runway/pkg/geom/manifold"
	math "gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/shape"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
	"gitlab.com/eukano/runway/script"
	"gitlab.com/eukano/runway/script/object"
	"golang.org/x/mod/semver"
)

type Interpreter struct {
	I      *interp.Interpreter
	objCtx *object.Context
}

type Options struct {
	ObjectContext *object.Context
}

func New(opts *Options) *Interpreter {
	if opts == nil {
		opts = new(Options)
	}

	objCtx := opts.ObjectContext
	if objCtx == nil {
		objCtx = new(object.Context)
	}

	pkg := func(sym interp.Exports, importPath string) map[string]reflect.Value {
		pkgName := path.Base(importPath)
		pkg, ok := sym[path.Join(importPath, pkgName)]
		if !ok {
			panic(fmt.Errorf("cannot find package %s", importPath))
		}
		return pkg
	}

	I := interp.New(interp.Options{})
	I.Use(interp.Exports{
		"fmt/fmt":   pkg(stdlib.Symbols, "fmt"),
		"log/log":   pkg(stdlib.Symbols, "log"),
		"math/math": pkg(stdlib.Symbols, "math"),

		"geom/geom": map[string]reflect.Value{
			"Vec":  reflect.ValueOf((*tensor.Vec3)(nil)),
			"Vec4": reflect.ValueOf((*tensor.Vec4)(nil)),
			"Face": reflect.ValueOf((*manifold.Polygon)(nil)),

			"Tri":  reflect.ValueOf((*geom.Tri)(nil)),
			"Quad": reflect.ValueOf((*geom.Quad)(nil)),
			"Pent": reflect.ValueOf((*geom.Pent)(nil)),
			"Hex":  reflect.ValueOf((*geom.Hex)(nil)),
			"Poly": reflect.ValueOf((*geom.Poly)(nil)),

			"NewFace": reflect.ValueOf(geom.NewFace),
			"Flip":    reflect.ValueOf(manifold.Flip),
			"Normal":  reflect.ValueOf(math.Normal),
			"Center":  reflect.ValueOf(math.Center),
			"Circle":  reflect.ValueOf(shape.Circle),
		},

		"object/object": map[string]reflect.Value{
			"Cube":        reflect.ValueOf(objCtx.Cube),
			"Sphere":      reflect.ValueOf(objCtx.Sphere),
			"Cylinder":    reflect.ValueOf(objCtx.Cylinder),
			"Octahedron":  reflect.ValueOf(objCtx.Octahedron),
			"Cube2":       reflect.ValueOf(objCtx.Cube2),
			"Sphere2":     reflect.ValueOf(objCtx.Sphere2),
			"Cylinder2":   reflect.ValueOf(objCtx.Cylinder2),
			"Octahedron2": reflect.ValueOf(objCtx.Octahedron2),
			"Torus":       reflect.ValueOf(objCtx.Torus),
			"Extrude":     reflect.ValueOf(objCtx.Extrude),
			"Revolve":     reflect.ValueOf(objCtx.Revolve),
			"Body":        reflect.ValueOf(objCtx.Body),
			"Line":        reflect.ValueOf(objCtx.Line),
		},

		"script/script": map[string]reflect.Value{
			"Version": reflect.ValueOf(func(v string) {
				if semver.Compare(script.Version, v) < 0 {
					panic(fmt.Errorf("script wants %s but interpreter is %s", v, script.Version))
				}
			}),
			"IsVersion": reflect.ValueOf(func(v string) bool {
				return semver.Compare(script.Version, v) >= 0
			}),
		},
	})

	I.ImportUsed()

	_, err := I.Eval(`
		type (
			Vec = geom.Vec
			Vec4 = geom.Vec4
		)

		const (
			Pi = math.Pi
		)

		var (
			AxisX = Vec{1, 0, 0}
			AxisY = Vec{0, 1, 0}
			AxisZ = Vec{0, 0, 1}
		)
	`)
	if err != nil {
		panic(fmt.Errorf("failed to initialize interpreter: %w", err))
	}

	return &Interpreter{I, objCtx}
}

func (i *Interpreter) Models() []model.Model {
	return i.objCtx.Models()
}

func (i *Interpreter) Eval(src string) error {
	_, err := i.I.Eval(src)
	return err
}

func (i *Interpreter) EvalPath(path string) error {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	rd := bufio.NewReader(bytes.NewReader(b))
	first, err := rd.ReadString('\n')
	if err != nil {
		return err
	}

	if strings.HasPrefix(first, "script.Version(") {
		_, err = i.I.Eval(first)
		if err != nil {
			return err
		}
	}

	_, err = i.I.Eval(string(b))
	return err
}
