package object

import (
	"sync"

	"gitlab.com/eukano/runway/internal/geom"
	"gitlab.com/eukano/runway/internal/geomath"
	"gitlab.com/eukano/runway/internal/mesh"
	"gitlab.com/eukano/runway/internal/model"
	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/shape"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

type primitive struct {
	y  sync.Once
	s  manifold.Polyhedron
	n  func(manifold.Polyhedron) mesh.Normalizer
	fn func() (manifold.Polyhedron, func(manifold.Polyhedron) mesh.Normalizer)
}

func (s *primitive) build() (manifold.Polyhedron, func(manifold.Polyhedron) mesh.Normalizer) {
	s.y.Do(func() { s.s, s.n = s.fn() })
	return s.s, s.n
}

var cube = &primitive{fn: func() (manifold.Polyhedron, func(manifold.Polyhedron) mesh.Normalizer) {
	const v = 0.5
	return shape.Cube(
		-v, +v,
		-v, +v,
		-v, +v,
	), mesh.NormalFromFace
}}

var sphere = &primitive{fn: func() (manifold.Polyhedron, func(manifold.Polyhedron) mesh.Normalizer) {
	return shape.Sphere(1, tensor.Vec3{}), mesh.NormalFromCenter
}}

var cylinder = &primitive{fn: func() (manifold.Polyhedron, func(manifold.Polyhedron) mesh.Normalizer) {
	return shape.Cylinder(tensor.Vec3{}, tensor.Vec3{0, 0, 1}), nil
}}

var octahedron = &primitive{fn: func() (manifold.Polyhedron, func(manifold.Polyhedron) mesh.Normalizer) {
	const v = 0.5
	return shape.Octahedron(
		-v, -v, -v,
		+v, +v, +v,
	), mesh.NormalFromFace
}}

type Context struct {
	mu     sync.Mutex
	models []model.Model
}

func (c *Context) add(m model.Model) {
	c.mu.Lock()
	c.models = append(c.models, m)
	c.mu.Unlock()
}

func (c *Context) obj(desc string, shape manifold.Polyhedron, norm func(manifold.Polyhedron) mesh.Normalizer, color func(manifold.Polyhedron) mesh.Colorizer) *Shape {
	return &Shape{
		context: c,
		desc:    desc,
		shape:   shape,
		norm:    norm,
		color:   color,
	}
}

func (c *Context) sobj(desc string, p *primitive) *Shape {
	s, n := p.build()
	return c.obj(desc, s, n, nil)
}

func (c *Context) Models() []model.Model {
	c.mu.Lock()
	o := make([]model.Model, len(c.models))
	copy(o, c.models)
	c.mu.Unlock()
	return o
}

func (c *Context) Body() *Body { return &Body{c, "", nil} }

func (c *Context) Cube() *Shape       { return c.sobj("Cube", cube) }
func (c *Context) Sphere() *Shape     { return c.sobj("Sphere", sphere) }
func (c *Context) Cylinder() *Shape   { return c.sobj("Cylinder", cylinder) }
func (c *Context) Octahedron() *Shape { return c.sobj("Octahedron", octahedron) }

func (c *Context) Cube2(x0, y0, z0, x1, y1, z1 float64) *Shape {
	return c.sobj("Cube", &primitive{fn: func() (manifold.Polyhedron, func(manifold.Polyhedron) mesh.Normalizer) {
		return shape.Cube(x0, y0, z0, x1, y1, z1), mesh.NormalFromFace
	}})
}

func (c *Context) Sphere2(radius float64, center tensor.Vec3) *Shape {
	return c.sobj("Sphere", &primitive{fn: func() (manifold.Polyhedron, func(manifold.Polyhedron) mesh.Normalizer) {
		return shape.Sphere(radius, center), mesh.NormalFromCenter
	}})
}

func (c *Context) Cylinder2(center, along tensor.Vec3) *Shape {
	return c.sobj("Cylinder", &primitive{fn: func() (manifold.Polyhedron, func(manifold.Polyhedron) mesh.Normalizer) {
		return shape.Cylinder(center, along), nil
	}})
}

func (c *Context) Octahedron2(x0, y0, z0, x1, y1, z1 float64) *Shape {
	return c.sobj("Octahedron", &primitive{fn: func() (manifold.Polyhedron, func(manifold.Polyhedron) mesh.Normalizer) {
		return shape.Octahedron(x0, y0, z0, x1, y1, z1), mesh.NormalFromFace
	}})
}

func (c *Context) Line(v []tensor.Vec3, loop bool) *Object {
	color := make([]tensor.Vec3, len(v))
	for i := range v {
		color[i] = geomath.HSL2RGB(float64(i)/float64(len(v)), 1, 0.5)
	}
	return &Object{c, model.NewLine("Line", geom.Poly(v), color, loop)}
}

func (c *Context) Torus(r1, r2 float64) *Shape {
	return c.obj("Torus", shape.Torus(r1, r2), nil, nil)
}

func (c *Context) Extrude(profile []tensor.Vec3, path tensor.Vec3) *Shape {
	return c.obj("Extrusion", shape.Extrude(geom.Poly(profile), path), nil, nil)
}

func (c *Context) Revolve(profile []tensor.Vec3, axis tensor.Vec3) *Shape {
	return c.obj("Revolution", shape.Revolve(geom.Poly(profile), axis, 50), nil, nil)
}

type Shape struct {
	context *Context
	desc    string

	shape manifold.Polyhedron
	xform []shape.Transform
	norm  func(manifold.Polyhedron) mesh.Normalizer
	color func(manifold.Polyhedron) mesh.Colorizer

	obj *Object
}

func (s *Shape) transformed() manifold.Polyhedron {
	if len(s.xform) == 0 {
		return s.shape
	}

	xform := s.xform
	s.xform = s.xform[:0]
	s.norm = nil

	var x = shape.Transform(tensor.Ident4())
	for _, y := range xform {
		x = y.Compose(x)
	}

	s.shape = shape.TransformShape(s.shape, x)
	return s.shape
}

func (s *Shape) Object() *Object {
	if s.obj != nil {
		return s.obj
	}

	shape := s.transformed()
	norm := s.norm
	if norm == nil {
		norm = mesh.NormalFromAdjacent
	}

	m := model.NewShape(s.desc, shape, norm, s.color)
	s.obj = &Object{s.context, m}
	return s.obj
}

func (s *Shape) AddTo(b *Body) { b.Add(s) }

func (s *Shape) Describe(desc string) *Shape {
	n := *s
	n.desc = desc
	return &n
}

func (s *Shape) ColorUniform(c tensor.Vec3) *Shape {
	n := *s
	n.color = mesh.UniformColor(c)
	return &n
}

func (s *Shape) ColorUniformA(c tensor.Vec4) *Shape {
	n := *s
	n.color = mesh.UniformColorA(c)
	return &n
}

func (s *Shape) ColorRandom() *Shape {
	n := *s
	n.color = mesh.RandomColor
	return &n
}

func (s *Shape) Union(r *Shape) *Shape {
	m := manifold.Union(s.transformed(), r.transformed())
	return s.context.obj(s.desc, m, nil, s.color)
}

func (s *Shape) Intersect(r *Shape) *Shape {
	m := manifold.Intersect(s.transformed(), r.transformed())
	return s.context.obj(s.desc, m, nil, s.color)
}

func (s *Shape) Subtract(r *Shape) *Shape {
	m := manifold.Subtract(s.transformed(), r.transformed())
	return s.context.obj(s.desc, m, nil, s.color)
}

func (s *Shape) Cleave(point, normal tensor.Vec3) (front, back *Shape) {
	f, b := manifold.Cleave(s.transformed(), point, normal)
	return s.context.obj(s.desc, f, nil, s.color), s.context.obj(s.desc, b, nil, s.color)
}

func (s *Shape) Apply(t shape.Transform) *Shape {
	n := *s
	n.xform = make([]shape.Transform, len(s.xform)+1)
	copy(n.xform, s.xform)
	n.xform[len(s.xform)] = t
	return &n
}

func (s *Shape) ScaleUniform(v float64) *Shape {
	return s.Apply(shape.ScaleUniform(v))
}

func (s *Shape) Scale(v tensor.Vec3) *Shape {
	return s.Apply(shape.ScaleV(v))
}

func (s *Shape) Translate(v tensor.Vec3) *Shape {
	return s.Apply(shape.TranslateV(v))
}

func (s *Shape) Rotate(angle float64, axis tensor.Vec3) *Shape {
	return s.Apply(shape.Rotate(angle, axis))
}

type Body struct {
	context     *Context
	description string
	shapes      []*Shape
}

func (b *Body) Describe(desc string) *Body {
	b.description = desc
	return b
}

func (b *Body) Add(s ...*Shape) *Body {
	// Copy all the shapes
	for _, s := range s {
		s := *s
		b.shapes = append(b.shapes, &s)
	}
	return b
}

func (b *Body) ColorUniform(c tensor.Vec3) *Body {
	a := *b
	for _, s := range b.shapes {
		s.color = mesh.UniformColor(c)
	}
	return &a
}

func (b *Body) ColorUniformA(c tensor.Vec4) *Body {
	a := *b
	for _, s := range b.shapes {
		s.color = mesh.UniformColorA(c)
	}
	return &a
}

func (b *Body) ColorRandom() *Body {
	a := *b
	for _, s := range b.shapes {
		s.color = mesh.RandomColor
	}
	return &a
}

func (b *Body) Object() *Object {
	m := make(model.Body, len(b.shapes))
	for i, s := range b.shapes {
		m[i] = s.Object().model
	}

	if b.description == "" {
		return &Object{b.context, m}
	}

	n := &describedModel{m, b.description}
	return &Object{b.context, n}
}

type Object struct {
	context *Context
	model   model.Model
}

func (o *Object) Add() {
	o.context.add(o.model)
}

func (o *Object) Apply(t model.Transform) *Object {
	return &Object{o.context, o.model.With(t)}
}

func (o *Object) ScaleUniform(v float64) *Object {
	return o.Apply(model.Static(shape.ScaleUniform(v)))
}

func (o *Object) Scale(v tensor.Vec3) *Object {
	return o.Apply(model.Static(shape.ScaleV(v)))
}

func (o *Object) Translate(v tensor.Vec3) *Object {
	return o.Apply(model.Static(shape.TranslateV(v)))
}

func (o *Object) Rotate(angle float64, axis tensor.Vec3) *Object {
	return o.Apply(model.Static(shape.Rotate(angle, axis)))
}

func (o *Object) Spin(rate float64, axis tensor.Vec3) *Object {
	return o.Apply(model.Dynamic(shape.Spin(rate, axis)))
}

func (o *Object) Wireframe(cull bool) *Object {
	return &Object{o.context, &model.Wireframe{Model: o.model, Cull: cull}}
}

type describedModel struct {
	model.Model
	description string
}

func (m *describedModel) With(x ...model.Transform) model.Model {
	return &describedModel{m.Model.With(x...), m.description}
}

func (m *describedModel) Inspect() model.Summary {
	s := m.Model.Inspect()
	s.Description = m.description
	return s
}
