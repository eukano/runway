# Model Script API

This document describes the API of the model script interpreter.

## Local

The stdlib packages `fmt`, `log`, and `math` are automatically imported. `mgl32`
is aliased to [github.com/go-gl/mathgl/mgl32][mgl32] and imported. `object` is
aliased to a modified and limited version of
[gitlab.com/eukano/runway/script/object][script-object] and imported.

[mgl32]: https://pkg.go.dev/github.com/go-gl/mathgl/mgl32
[script-object]: https://pkg.go.dev/gitlab.com/eukano/runway/script/object

The following definitions are provided (unqualified):

```go
type (
    Vec2 = mgl32.Vec2
    Vec3 = mgl32.Vec3
    Vec4 = mgl32.Vec4

    Mat2 = mgl32.Mat2
    Mat3 = mgl32.Mat3
    Mat4 = mgl32.Mat4
)

const (
    Pi float32 = math.Pi
)

var (
    AxisX = Vec3{1, 0, 0}
    AxisY = Vec3{0, 1, 0}
    AxisZ = Vec3{0, 0, 1}
)
```

## Object

Methods of `Object` return a copy of the original with modifications applied and
do not modify the receiver.

```go
package object

type (
    Object struct{}
    Body struct{}
)

// Cube of side length 1, centered about the origin.
func Cube() Object

// Sphere of radius 1, centered about the origin.
func Sphere() Object

// Cylinder of diameter 1 and height 1, centered about the origin.
func Cylinder() Object

// Torus with cross-sectional radius of r1 and rotational radius of r2, centered about the origin.
func Torus(r1, r2 float32) Object

// Extrude *profile* along *path*, centered on the original profile.
func Extrude(profile []Vec3, path Vec3) Object

// Revolve *profile* about *axis*. Passing a non-normal axis may have unintended results.
func Revolve(profile []Vec3, axis Vec3) Object

// Body is a container that objects can be added to. Transforms to a body apply to all contained objects.
func Body() body


// Describe the body. This has no effect on rendering and is purely informational.
func (Body) Describe(desc string) Body

// Add an object to the body. Modifies the receiver.
func (Body) Add(Object) Body

// Create an object from the body. Subsequent changes to the receiver will not affect the returned object.
func (Body) Object() Object


// Add the object to the rendering context.
func (Object) Add()

// Add the object to a body.
func (Object) AddTo(Body)

// Apply a mesh transform to the object.
func (Object) Apply(mesh.Transform) Object

// Scale the object.
func (Object) Scale(x, y, z float32) Object

// Uniformly scale the object.
func (Object) ScaleUniform(v float32) Object

// Scale the object with a vector.
func (Object) ScaleV(v Vector) Object

// Translate the object.
func (Object) Translate(x, y, z float32) Object

// Translate the object with a vector.
func (Object) TranslateV(v Vec3) Object

// Rotate the object about *axis* by *angle* radians. Passing a non-normal axis may have unintended results.
func (Object) Rotate(angle float32, axis Vec3) Object

// Spin the object about *axis* by *rate* radians per second. Passing a non-normal axis may have unintended results.
func (Object) Spin(rate float32, axis Vec3) Object

// Describe the object. This has no effect on rendering and is purely informational.
func (Object) Describe(desc string) Object
```
