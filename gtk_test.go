//go:build linux || gtk

package runway

import (
	"fmt"
	"log"
	"math"

	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/eukano/runway/internal/model"
	"gitlab.com/eukano/runway/internal/ui"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// Example is supposed to show the default model but it's not quite working
func Example() {
	const W, H = 800, 600
	const FOV = 45

	p := model.Position{Value: tensor.Vec3{0, 3, 10}}
	o := new(model.Heading).SetDeg(tensor.Vec2{0, -20}).Orientation()
	p.Update(0, 0, 0, 0, o)

	fov := FOV / 180 * math.Pi
	proj := tensor.Perspective(fov, float64(W)/float64(H), 0.1, 1000)
	ortho := tensor.Ortho(0, float64(W), 0, float64(H), -1, 1)
	G := model.Globals{
		Flatland: ortho,
		Lights:   ui.DefaultLights,
		World: model.World{
			Projection: proj,
			View:       p.View,
			Camera:     p.Value,
		},
	}

	// Initialize GTK without parsing any command line arguments.
	gtk.Init(nil)

	// Create a new toplevel window, set its title, and connect it to the
	// "destroy" signal to exit the GTK main loop when it is destroyed.
	win, err := gtk.WindowNew(gtk.WINDOW_TOPLEVEL)
	if err != nil {
		log.Fatal(err)
	}

	win.SetTitle("Runway")
	win.SetDefaultSize(W, H)
	win.Connect("destroy", func() { gtk.MainQuit() })

	// Set up the GL widget
	gla, err := gtk.GLAreaNew()
	if err != nil {
		log.Fatal(err)
	}
	win.Add(gla)
	gla.SetRequiredVersion(4, 6)

	r := new(ui.Renderer)
	r.Add(ui.DefaultModel)

	glfn := func(fn func() error) func() {
		return func() {
			gla.MakeCurrent()
			err := fn()
			if err != nil {
				log.Fatal(err)
			}
		}
	}

	gla.Connect("realize", glfn(func() error { return r.Prepare(true) }))
	gla.Connect("render", glfn(func() error { r.Render(W, H, G); return nil }))
	gla.Connect("unrealize", glfn(func() error { r.Cleanup(); return nil }))

	// Recursively show all widgets contained in this window.
	win.ShowAll()

	// Begin executing the GTK main loop.  This blocks until
	// gtk.MainQuit() is run.
	gtk.Main()

	fmt.Println("Example")
	// Output: Example
}
