# Project Runway

Project Runway is a Go modeling tool - because models **go** down the **runway**
*\*ba dum tss\**

Models are built as specialized Go scripts that use the `runway` API and are
executed with [yaegi](https://github.com/traefik/yaegi).

![fan](fan.png)