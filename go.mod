module gitlab.com/eukano/runway

go 1.23

require (
	github.com/adrg/sysfont v0.1.2
	github.com/go-gl/gl v0.0.0-20210501111010-69f74958bac0
	github.com/go-gl/mathgl v1.1.0
	github.com/gotk3/gotk3 v0.6.3
	github.com/icza/mjpeg v0.0.0-20201020132628-7c1e1838a393
	github.com/lxn/walk v0.0.0-20210112085537-c389da54e794
	github.com/lxn/win v0.0.0-20210218163916-a377121e959e
	github.com/progrium/macdriver v0.1.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/pflag v1.0.5
	github.com/stretchr/testify v1.7.0
	github.com/traefik/yaegi v0.9.19-0.20210621092410-36594014c96b
	golang.org/x/exp v0.0.0-20231110203233-9a3e6036ecaa
	golang.org/x/image v0.14.0
	golang.org/x/mod v0.14.0
	golang.org/x/sys v0.14.0
	gonum.org/v1/gonum v0.14.0
)

require (
	github.com/adrg/strutil v0.2.2 // indirect
	github.com/adrg/xdg v0.3.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	gopkg.in/Knetic/govaluate.v3 v3.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)

replace (
	github.com/lxn/walk => github.com/firelizzard18/walk v0.0.0-20210707213546-b6cf53106f71
	github.com/lxn/win => github.com/firelizzard18/win v0.0.0-20210707212010-0a296145e3e2
	gonum.org/v1/gonum v0.14.0 => github.com/firelizzard18/gonum v0.9.1-0.20240303053246-38579f0fa0a6
)
