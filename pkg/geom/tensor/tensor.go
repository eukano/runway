// Package tensor implements basic tensor calculations, such as vector
// projection and matrix transformations. Higher-order operations such as mesh
// manipulations use functions in this package but are implemented in their own
// package(s).
//
// Tensors are a generalization of scalars, vectors, matrices, and analogous
// higher-order structures, though this package doesn't implement anything
// higher-order than matrices.
package tensor

import (
	mgl "github.com/go-gl/mathgl/mgl64"
)

// AdjacentEps is the euclidian distance threshold for considering two points
// equal. This is necessary due to floating point errors.
var AdjacentEps = mgl.Epsilon

var EpsSqr = AdjacentEps * AdjacentEps

// Mat3 is a 3✕3 matrix.
type Mat3 = mgl.Mat3

// Mat4 is a 4✕4 matrix.
type Mat4 = mgl.Mat4

// Vec2 is a 2 element vector.
type Vec2 = mgl.Vec2

// Vec3 is a 3 element vector.
type Vec3 = mgl.Vec3

// Vec4 is a 4 element vector.
type Vec4 = mgl.Vec4

// LessThanEps calculates abs(v) < [AdjacentEps] without requiring an intermediate
// value.
func LessThanEps(v float64) bool {
	return -AdjacentEps < v && v < AdjacentEps
}
