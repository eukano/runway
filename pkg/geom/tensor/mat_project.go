package tensor

import mgl "github.com/go-gl/mathgl/mgl64"

// Perspective returns a perspective projection matrix.
func Perspective(fovy, aspect, near, far float64) Mat4 {
	return mgl.Perspective(fovy, aspect, near, far)
}

// Ortho returns a orthographic projection matrix.
func Ortho(left, right, bottom, top, near, far float64) Mat4 {
	return mgl.Ortho(left, right, bottom, top, near, far)
}

// LookAt returns a view matrix looking at center from eye.
func LookAt(eye, center, up Vec3) Mat4 {
	return mgl.LookAtV(eye, center, up)
}
