package tensor

import "math"

func Round3(v Vec3) Vec3 {
	for i, vv := range v {
		v[i] = math.Round(vv/AdjacentEps) * AdjacentEps
	}
	return v
}

func Invert3(v Vec3) Vec3 {
	return Vec3{-v[0], -v[1], -v[2]}

}

func Cmp3(v, u Vec3) int {
	switch {
	case v[0] > u[0]:
		return +1
	case v[0] < u[0]:
		return -1
	case v[1] > u[1]:
		return +1
	case v[1] < u[1]:
		return -1
	case v[2] > u[2]:
		return +1
	case v[2] < u[2]:
		return -1
	}
	return 0
}
