package tensor

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestClipLine(t *testing.T) {
	type V = Vec2

	t.Run("Pass", func(t *testing.T) {
		cases := []struct {
			min, max, a, b, ap, bp V
		}{
			{min: V{0, 0}, max: V{2, 2}, a: V{-1, 0}, b: V{2, 3}, ap: V{0, 1}, bp: V{1, 2}},
			{min: V{0, 0}, max: V{2, 2}, a: V{-1, 1}, b: V{1, 1}, ap: V{0, 1}, bp: V{1, 1}},
			{min: V{0, 0}, max: V{2, 2}, b: V{-1, 0}, a: V{2, 3}, bp: V{0, 1}, ap: V{1, 2}},
			{min: V{0, 0}, max: V{2, 2}, b: V{-1, 1}, a: V{1, 1}, bp: V{0, 1}, ap: V{1, 1}},
		}

		for _, c := range cases {
			t.Run("", func(t *testing.T) {
				ap, bp, ok := ClipLine(c.a, c.b, c.min, c.max)
				require.True(t, ok)
				assert.Equal(t, c.ap, ap)
				assert.Equal(t, c.bp, bp)
			})
		}
	})

	t.Run("Fail", func(t *testing.T) {
		cases := []struct {
			min, max, a, b V
		}{
			{min: V{0, 0}, max: V{2, 2}, a: V{-3, -3}, b: V{-1, -1}},
			{min: V{0, 0}, max: V{2, 2}, a: V{+3, +3}, b: V{+5, +5}},
			{min: V{0, 0}, max: V{2, 2}, b: V{-3, -3}, a: V{-1, -1}},
			{min: V{0, 0}, max: V{2, 2}, b: V{+3, +3}, a: V{+5, +5}},
			{min: V{0, 0}, max: V{2, 2}, a: V{-3, +5}, b: V{-1, +3}},
			{min: V{0, 0}, max: V{2, 2}, a: V{+3, -1}, b: V{+5, -3}},
		}

		for _, c := range cases {
			t.Run("", func(t *testing.T) {
				a, b, ok := ClipLine(c.a, c.b, c.min, c.max)
				require.False(t, ok, "Got %v - %v", a, b)
			})
		}
	})
}
