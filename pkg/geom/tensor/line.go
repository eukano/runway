package tensor

// ClipLine clips the line AB to the rectangle described by min and max.
//
// Based on https://en.wikipedia.org/wiki/Liang%E2%80%93Barsky_algorithm
func ClipLine(a, b, min, max Vec2) (ap, bp Vec2, ok bool) {
	p2, p4 := b[0]-a[0], b[1]-a[1]
	p1, p3 := -p2, -p4
	q1, q2 := a[0]-min[0], max[0]-a[0]
	q3, q4 := a[1]-min[1], max[1]-a[1]

	if p1 == 0 && q1 < 0 || p2 == 0 && q2 < 0 || p3 == 0 && q3 < 0 || p4 == 0 && q4 < 0 {
		// Line is outside of the rectangle
		return Vec2{}, Vec2{}, false
	}

	var pos, neg []float64

	if p1 != 0 {
		r1, r2 := q1/p1, q2/p2
		if p1 < 0 {
			neg = append(neg, r1)
			pos = append(pos, r2)
		} else {
			pos = append(pos, r1)
			neg = append(neg, r2)
		}
	}

	if p3 != 0 {
		r3, r4 := q3/p3, q4/p4
		if p3 < 0 {
			neg = append(neg, r3)
			pos = append(pos, r4)
		} else {
			pos = append(pos, r3)
			neg = append(neg, r4)
		}
	}

	rn1, rn2 := maxr(neg), minr(pos)
	if rn1 > rn2 || rn1 > 1 || rn2 < 0 {
		// Line is outside of the rectangle
		return Vec2{}, Vec2{}, false
	}

	if rn1 < 0 {
		ap = a
	} else {
		ap = Vec2{a[0] + p2*rn1, a[1] + p4*rn1}
	}

	if rn2 > 1 {
		bp = b
	} else {
		bp = Vec2{a[0] + p2*rn2, a[1] + p4*rn2}
	}

	return ap, bp, true
}

func minr(r []float64) float64 {
	v := r[0]
	for _, u := range r[1:] {
		if u < v {
			v = u
		}
	}
	return v
}

func maxr(r []float64) float64 {
	v := r[0]
	for _, u := range r[1:] {
		if u > v {
			v = u
		}
	}
	return v
}
