package tensor

// Variance calculates the variance of values. Standard deviation is the square
// root of variance.
func Variance(v ...float64) float64 {
	var mean float64
	for _, v := range v {
		mean += v
	}
	mean /= float64(len(v))

	var variance float64
	for _, v := range v {
		u := v - mean
		variance += u * u
	}
	variance /= float64(len(v))

	return variance
}

// VarianceTri calculates the variance of the internal angles of the triangle
// V₀V₁V₂. A variance of 0 means the triangle is equilateral. This is intended
// for relative comparison, not for absolute calculations, as it technically
// calculates the variance of the cosine of the angles.
func VarianceTri(v0, v1, v2 Vec3) float64 {
	// Calculate vectors V₀V₁, V₀V₂, V₁V₂, and their length
	ab, ac, bc := v1.Sub(v0), v2.Sub(v0), v2.Sub(v1)
	lab, lac, lbc := ab.Len(), ac.Len(), bc.Len()

	// cos(∠V₀) = V₀V₁∙V₀V₂
	va := ab.Dot(ac) / (lab * lac)
	vb := ab.Dot(bc) / (lab * lbc) * -1
	vc := ac.Dot(bc) / (lac * lbc)
	return Variance(va, vb, vc)
}
