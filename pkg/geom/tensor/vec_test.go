package tensor

import (
	"fmt"
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestFaceTriContains(t *testing.T) {
	cases := []struct {
		base, leg1, leg2, point Vec3
		expect                  bool
	}{
		{Vec3{0, 0}, Vec3{0, 3}, Vec3{4, 0}, Vec3{1, 1}, true},
		{Vec3{0, 0}, Vec3{0, 3}, Vec3{4, 0}, Vec3{3, 3}, false},
		{Vec3{0, 0}, Vec3{0, 3}, Vec3{4, 0}, Vec3{-1, 1}, false},
		{Vec3{0, 0}, Vec3{0, 3}, Vec3{4, 0}, Vec3{1, -1}, false},
		{Vec3{0, 0}, Vec3{0, 3}, Vec3{4, 0}, Vec3{-1, -1}, false},
		{Vec3{10, 10}, Vec3{0, 3}, Vec3{4, 0}, Vec3{11, 11}, true},
		{Vec3{10, 10}, Vec3{0, 3}, Vec3{4, 0}, Vec3{13, 13}, false},
	}

	test := func(p, q0, q1, q2 Vec3, x bool) func(t *testing.T) {
		return func(t *testing.T) {
			y := Contains(p, q0, q1, q2)
			if y != x {
				t.Fatalf("expected %v, got %v", x, y)
			}
		}
	}

	for _, c := range cases {
		v0, v1, v2 := c.base, c.base.Add(c.leg1), c.base.Add(c.leg2)
		t.Run("", test(c.point, v0, v1, v2, c.expect))
		t.Run("", test(c.point, v0, v2, v1, c.expect))
		t.Run("", test(c.point, v1, v0, v2, c.expect))
		t.Run("", test(c.point, v1, v2, v0, c.expect))
		t.Run("", test(c.point, v2, v0, v1, c.expect))
		t.Run("", test(c.point, v2, v1, v0, c.expect))
	}
}

func TestProjectPlane(t *testing.T) {
	cases := []struct {
		origin, normal, point, expected Vec3
	}{
		{Vec3{}, Vec3{0, 1, 0}, Vec3{1, 2, 3}, Vec3{1, 0, 3}},
		{Vec3{}, Vec3{1, 1, 0}, Vec3{0, 2, 0}, Vec3{-1, 1, 0}},
		{Vec3{0, -2, 0}, Vec3{0, 1, 0}, Vec3{1, 2, 3}, Vec3{1, -2, 3}},
	}

	for _, c := range cases {
		t.Run("", func(t *testing.T) {
			j := ProjectPlane(c.point, c.origin, c.normal.Normalize())
			if j.Sub(c.expected).LenSqr() > 1e-12 {
				t.Logf("error factor = %v", j.Sub(c.expected).LenSqr())
				t.Fatalf("expected %v, got %v", c.expected, j)
			}
		})
	}
}

func TestDistancePlane(t *testing.T) {
	cases := []struct {
		origin, normal, point Vec3
		expected              float64
	}{
		{Vec3{}, Vec3{0, 1, 0}, Vec3{1, 2, 3}, 2},
		{Vec3{}, Vec3{1, 1, 0}, Vec3{0, 2, 0}, math.Sqrt2},
		{Vec3{}, Vec3{0, 1, 0}, Vec3{3, -2, 1}, -2},
		{Vec3{}, Vec3{1, 1, 0}, Vec3{0, -2, 0}, -math.Sqrt2},
	}

	for _, c := range cases {
		t.Run("", func(t *testing.T) {
			d := DistancePlane(c.point, c.origin, c.normal.Normalize())
			if math.Abs(d-c.expected) > AdjacentEps {
				t.Fatalf("expected %v, got %v", c.expected, d)
			}
		})
	}
}

func TestIntersectPlanes(t *testing.T) {
	type Line struct{ V, Q Vec3 }
	type Plane struct{ N, Q Vec3 }
	cases := []struct {
		L    Line
		A, B Plane
	}{
		{Line{Vec3{0, 0, 1}, Vec3{0}},
			Plane{Vec3{1, 0, 0}, Vec3{0}},
			Plane{Vec3{0, 1, 0}, Vec3{0}}},

		{Line{Vec3{0, 0, 1}, Vec3{1, 1, 0}},
			Plane{Vec3{1, 0, 0}, Vec3{1, 0, 0}},
			Plane{Vec3{0, 1, 0}, Vec3{0, 1, 0}}},

		{Line{Vec3{-2, 2, 0}, Vec3{0.25, 0.25, -0.5}},
			Plane{Vec3{1, 1, 1}, Vec3{0}},
			Plane{Vec3{1, 1, -1}, Vec3{0, 0, -1}}},
	}

	for i, c := range cases {
		t.Run(fmt.Sprintf("Case %d", i), func(t *testing.T) {
			v, q, ok := PlaneIntersectPlane(c.A.Q, c.A.N, c.B.Q, c.B.N)
			require.True(t, ok)
			assert.Equal(t, c.L.V, v)
			assert.Equal(t, c.L.Q, q)

			// Sanity check
			an := c.A.N.Normalize()
			bn := c.B.N.Normalize()
			require.Equal(t, c.A.Q.Dot(an), q.Dot(an))
			require.Equal(t, c.B.Q.Dot(bn), q.Dot(bn))
		})
	}
}
