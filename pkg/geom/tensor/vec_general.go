package tensor

// ProjectVec projects Q₁P onto the vector space described by Q₁Q₀, Q₁Q₂, and
// Q₁Q₀ ✕ Q₁Q₂
func ProjectVec(p, q0, q1, q2 Vec3) Vec3 {
	// Solve U = a*A + b*B + c*C
	//   A = vector from v0 to v1
	//   B = vector from v0 to v2
	//   C = A x B
	//   U = vector from v0 to p
	//
	// C is necessary in order to do this with linear algebra. If the
	// coefficient of C is non-zero, P is out of the plane.

	A := q0.Sub(q1)
	B := q2.Sub(q1)
	C := A.Cross(B)
	U := p.Sub(q1)
	X := Mat3FromCols(A, B, C).Inv()
	return X.Mul3x1(U)
}

// Contains returns whether P is contained within the triangle Q₀Q₁Q₂.
func Contains(p, q0, q1, q2 Vec3) bool {
	a, b, c := ProjectVec(p, q0, q1, q2).Elem()
	return LessThanEps(c) &&
		a > AdjacentEps &&
		b > AdjacentEps &&
		a+b <= 1
}
