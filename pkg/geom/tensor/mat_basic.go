package tensor

import (
	mgl "github.com/go-gl/mathgl/mgl64"
)

// construct a 3✕3 matrix from three 3-element column vectors.
func Mat3FromCols(a, b, c Vec3) Mat3 {
	return mgl.Mat3FromCols(a, b, c)
}

// Ident4 returns the 4✕4 identity matrix
func Ident4() Mat4 { return mgl.Ident4() }
