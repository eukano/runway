package tensor

import mgl "github.com/go-gl/mathgl/mgl64"

// Scale3D returns a homogenous 3D scaling matrix.
func Scale3D(x, y, z float64) Mat4 {
	return mgl.Scale3D(x, y, z)
}

// Translate3D returns a homogenous 3D translation matrix.
func Translate3D(x, y, z float64) Mat4 {
	return mgl.Translate3D(x, y, z)
}

// Rotate3D returns a homogenous 3D rotation matrix.
func Rotate3D(angle float64, axis Vec3) Mat4 {
	return mgl.HomogRotate3D(angle, axis)
}
