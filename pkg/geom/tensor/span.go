package tensor

// Span3 is an axis-aligned 3-space span, i.e. a rectangular cuboid.
type Span3 struct{ Min, Max Vec3 }

// Volume returns the contained volume of the span.
func (s Span3) Volume() float64 {
	var v float64 = 1
	for i := 0; i < 3; i++ {
		v *= s.Max[i] - s.Min[i]
	}
	return v
}

// Overlaps returns true if A and B overlap each other.
func (s Span3) Overlaps(r Span3) bool {
	// A overlaps B if and only if a_min_d is less than b_max_d and a_max_d is
	// greater than b_min_d for each dimension d
	for i := 0; i < 3; i++ {
		if s.Min[i] > r.Max[i] || s.Max[i] < r.Min[i] {
			return false
		}
	}

	return true
}

// Contains returns true if A completely contains B.
func (s Span3) Contains(r Span3) bool {
	// A contains B if and only if a_min_d is less than b_min_d and a_max_d is
	// greater than b_max_d for each dimension d
	for i := 0; i < 3; i++ {
		if s.Min[i] > r.Min[i] || s.Max[i] < r.Max[i] {
			return false
		}
	}
	return true
}

func (s Span3) ContainsPoint(p Vec3) bool {
	for i := 0; i < 3; i++ {
		if s.Min[i] > p[i] || p[i] > s.Max[i] {
			return false
		}
	}
	return true
}

// Intersection returns the intersection volume of S and R, or false if they do
// not intersect.
func (s Span3) Intersection(r Span3) (Span3, bool) {
	var x Span3
	for i := 0; i < 3; i++ {
		if s.Max[i] < r.Min[i] || r.Max[i] < s.Min[i] {
			return x, false
		}
		x.Min[i] = max(s.Min[i], r.Min[i])
		x.Max[i] = min(s.Max[i], r.Max[i])
	}
	return x, true
}
