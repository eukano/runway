package tensor

import "math"

// Area calculates the area of the parallelogram with sides Q₁Q₀ and Q₁Q₂.
func Area(q0, q1, q2 Vec3) Vec3 {
	a, b := q0.Sub(q1), q2.Sub(q1)
	return a.Cross(b)
}

// DistancePlane returns the distance from point P to the plane described by
// point Q and normal N. The result is positive if P is on the same side as the
// normal.
func DistancePlane(p, q, n Vec3) float64 {
	d := p.Sub(q).Dot(n)
	if LessThanEps(d) {
		return 0
	}
	return d
}

// ProjectPlane projects the vector V onto the plane described by point Q and
// normal N.
func ProjectPlane(v, q, n Vec3) Vec3 {
	return v.Sub(n.Mul(DistancePlane(v, q, n)))
}

// IntersectPlane returns the point of intersection between V₀V₁ and the plane
// described by point Q and normal N, if any.
//
// https://en.wikipedia.org/wiki/Line%E2%80%93plane_intersection
func IntersectPlane(v0, v1, q, n Vec3) (Vec3, float64, bool) {
	v := v1.Sub(v0)
	denom := n.Dot(v)
	if LessThanEps(denom) {
		return Vec3{}, 0, false
	}

	fac := q.Sub(v0).Dot(n) / denom
	return v.Mul(fac).Add(v0), fac, true
}

// PlaneIntersectPlane determines the direction vector L and a point P of the
// intersection between planes A and B each described by a normal vector and a
// point on the plane.
func PlaneIntersectPlane(q1, n1, q2, n2 Vec3) (l, p Vec3, ok bool) {
	// The direction vector L = N₁ ⨯ N₂
	l = n1.Cross(n2)

	// V (= N₁ ⨯ L) and Q₁ describe a line within A normal to L
	v := n1.Cross(l)

	// Determine the intersection of B and the line described by V and Q₁
	d, n := n2.Dot(v), q2.Sub(q1).Dot(n2)
	if LessThanEps(d) {
		return l, p, false
	}

	p = v.Mul(n / d).Add(q1)
	return Round3(l), Round3(p), true
}

func AreColinear(a, b, c Vec3) bool {
	d := a.Sub(c).Cross(b.Sub(c)).LenSqr()
	return math.Abs(d) < EpsSqr
}
