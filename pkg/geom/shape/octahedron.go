package shape

import (
	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// Octahedron creates an octahedral Shape with the specified min-max XYZ. The
// resulting shape is the inner dual of the corresponding Cube. For any
// dimension D, if D₀ > D₁, D₀ and D₁ are swapped.
func Octahedron(x0, y0, z0, x1, y1, z1 float64) manifold.Polyhedron {
	ascending(&x0, &x1)
	ascending(&y0, &y1)
	ascending(&z0, &z1)

	xm, ym, zm := (x0+x1)/2, (y0+y1)/2, (z0+z1)/2

	right := tensor.Vec3{x1, ym, zm}
	left := tensor.Vec3{x0, ym, zm}
	top := tensor.Vec3{xm, y1, zm}
	bottom := tensor.Vec3{xm, y0, zm}
	front := tensor.Vec3{xm, ym, z1}
	back := tensor.Vec3{xm, ym, z0}

	return manifold.New([]manifold.Polygon{
		manifold.SimplePoly{top, right, front},
		manifold.SimplePoly{top, back, right},
		manifold.SimplePoly{top, left, back},
		manifold.SimplePoly{top, front, left},
		manifold.SimplePoly{bottom, front, right},
		manifold.SimplePoly{bottom, right, back},
		manifold.SimplePoly{bottom, back, left},
		manifold.SimplePoly{bottom, left, front},
	})
}
