package shape

import (
	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// Extrude creates a Shape by creating two copies of the profile, one translated
// by ½∙path and the other translated by -½∙path and flipped, and sweeping from
// one face to the other.
func Extrude(profile manifold.Polygon, path tensor.Vec3) manifold.Polyhedron {
	front := TransformFace(profile, TranslateV(path.Mul(-0.5)))
	back := TransformFace(profile, TranslateV(path.Mul(+0.5)))
	shape := make([]manifold.Polygon, profile.Len()+2)
	shape[0] = manifold.Flip(front)
	shape[1] = back
	copy(shape[2:], manifold.Faces(Sweep(front, back)))
	return manifold.New(shape)
}
