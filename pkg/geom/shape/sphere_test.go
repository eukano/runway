package shape

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

func TestSphereWinding(t *testing.T) {
	ac := tensor.Vec3{1, 0.1, 0.1}
	bc := tensor.Vec3{0}
	a := sphere(1, ac, 8, 16).(*manifold.Manifold)
	b := sphere(1, bc, 8, 16).(*manifold.Manifold)

	for _, v := range a.Vertex {
		// Since the 'sphere' is not a true sphere, some points lie inside
		// the true sphere but outside the manifold. So if the distance from
		// the center of B is between ~0.95 and 1, skip the vertex.
		l := v.Sub(bc).LenSqr()
		if 0.9 < l && l < 1 {
			continue
		}

		var s [3]string
		for i, v := range v {
			s[i] = fmt.Sprintf("%.2f", v)
		}
		t.Run(strings.Join(s[:], ","), func(t *testing.T) {
			w := b.Winding(v)
			e := l < 1+tensor.AdjacentEps
			assert.Equal(t, e, w != manifold.OutsidePoint, "%v", v)
		})
	}
}

func TestSphereWinding2(t *testing.T) {
	ac := tensor.Vec3{1, 0.1, 0.1}
	bc := tensor.Vec3{0}
	a := sphere(1, ac, 4, 8).(*manifold.Manifold)
	b := sphere(1, bc, 4, 8).(*manifold.Manifold)

	for _, v := range b.Vertex {
		a.Winding(v)
	}
}

func TestSphereIntersect(t *testing.T) {
	ac := tensor.Vec3{1, 0.1, 0.1}
	bc := tensor.Vec3{0}
	a := sphere(1, ac, 4, 8).(*manifold.Manifold)
	b := sphere(1, bc, 4, 8).(*manifold.Manifold)

	manifold.Subtract(a, b)
}

func TestCubeOctIntersect(t *testing.T) {
	a := Cube(-2, -2, -2, -0.1, +2, +2)
	b := Octahedron(-1, -1, -1, +1, +1, +1)

	manifold.Subtract(b, a)
}
