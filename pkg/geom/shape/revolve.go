package shape

import (
	"math"

	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// Revolve creates a Shape by revolving a profile about an axis.
func Revolve(profile manifold.Polygon, axis tensor.Vec3, resolution int) manifold.Polyhedron {
	N, M := profile.Len(), resolution
	faces := make([]manifold.Polygon, N*M)

	a := profile
	for i := 0; i < M; i++ {
		b := TransformFace(profile, Rotate(-2*math.Pi*float64(i+1)/float64(M), axis))
		copy(faces[i*N:], manifold.Faces(Sweep(a, b)))
		a = b
	}
	return manifold.New(faces)
}
