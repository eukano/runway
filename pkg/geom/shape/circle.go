package shape

import (
	"math"

	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// Circle creates a circle with the specified number of verticies, radius,
// center, and normal vector.
func Circle(resolution int, radius float64, center, normal tensor.Vec3) manifold.Polygon {
	if normal == (tensor.Vec3{}) {
		normal = tensor.Vec3{0, 0, 1}
	}

	f := make(manifold.SimplePoly, resolution)
	for i := range f {
		th := 2 * math.Pi * float64(i) / float64(resolution)
		thSin := math.Sin(th)
		thCos := math.Cos(th)
		f[i] = tensor.Vec3{radius * thSin, radius * thCos, 0}.Add(center)
	}

	return shiftPlane(tensor.Vec3{0, 0, 1}, normal, f)
}
