package shape

import (
	"time"

	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// TransformFace constructs a new face by applying the transform to each vertex
// of the original face.
func TransformFace(f manifold.Polygon, x Transform) manifold.Polygon {
	g := make(manifold.SimplePoly, f.Len())
	for i, v := range manifold.Vertices(f) {
		g[i] = x.Apply(v)
	}
	return g
}

// TransformFace constructs a new shape by applying the transform to each face
// of the original shape.
func TransformShape(s manifold.Polyhedron, x Transform) manifold.Polyhedron {
	// If the shape is a Manifold, transform the vertices directly
	if m, ok := s.(*manifold.Manifold); ok {
		n := new(manifold.Manifold)
		n.Vertex = make([]tensor.Vec3, len(m.Vertex))
		n.Face = make([][]int, len(m.Face))
		copy(n.Face, m.Face)
		for i, v := range m.Vertex {
			n.Vertex[i] = x.Apply(v)
		}
		return n
	}

	n := s.Len()
	t := make([]manifold.Polygon, n)
	for i := 0; i < n; i++ {
		t[i] = TransformFace(s.Get(i), x)
	}
	return manifold.New(t)
}

// Scale returns a 3D scaling transform.
func Scale(x, y, z float64) Transform {
	return Transform(tensor.Scale3D(x, y, z))
}

// ScaleV returns a 3D scaling transform.
func ScaleV(v tensor.Vec3) Transform {
	return Scale(v[0], v[1], v[2])
}

// ScaleUniform returns a 3D scaling transform that is uniform in each axis.
func ScaleUniform(v float64) Transform {
	return Scale(v, v, v)
}

// Translate returns a 3D translation transform.
func Translate(x, y, z float64) Transform {
	return Transform(tensor.Translate3D(x, y, z))
}

// TranslateV returns a 3D translation transform.
func TranslateV(v tensor.Vec3) Transform {
	return Translate(v[0], v[1], v[2])
}

// Rotate returns a 3D transform that rotates about an axis by an angle
// (radians).
func Rotate(angle float64, axis tensor.Vec3) Transform {
	return Transform(tensor.Rotate3D(angle, axis))
}

// Spin returns a dynamic 3D transform that rotates about an axis at an angular
// rate (radians/second).
func Spin(rate float64, axis tensor.Vec3) DynamicTransform {
	return func(delta, runtime time.Duration) tensor.Mat4 {
		angle := rate * runtime.Seconds()
		return tensor.Rotate3D(angle, axis)
	}
}

// A Transform is a 4✕4 matrix that represents a static 3D transform.
type Transform tensor.Mat4

// Mat returns the matrix.
func (x Transform) Mat() tensor.Mat4 { return tensor.Mat4(x) }

// Compose composes two transforms, X ✕ Y.
func (x Transform) Compose(y Transform) Transform { return Transform(x.Mat().Mul4(y.Mat())) }

// Apply applies the transform to a vector, X ✕ V.
func (x Transform) Apply(v tensor.Vec3) tensor.Vec3 { return x.Mat().Mul4x1(v.Vec4(1)).Vec3() }

// A DynamicTransform is a time-varying 4✕4 matrix that represents a dynamic 3D
// transform.
type DynamicTransform func(delta, runtime time.Duration) tensor.Mat4

// Evolve returns the value of the transform at time T.
func (x DynamicTransform) Evolve(delta, runtime time.Duration) Transform {
	return Transform(x(delta, runtime))
}
