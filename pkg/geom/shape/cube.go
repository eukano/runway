package shape

import (
	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// Cube creates a cube Shape with the specified min-max XYZ. For any dimension
// D, if D₀ > D₁, D₀ and D₁ are swapped.
func Cube(x0, y0, z0, x1, y1, z1 float64) manifold.Polyhedron {
	ascending(&x0, &x1)
	ascending(&y0, &y1)
	ascending(&z0, &z1)

	v000 := tensor.Vec3{x0, y0, z0}
	v001 := tensor.Vec3{x0, y0, z1}
	v010 := tensor.Vec3{x0, y1, z0}
	v011 := tensor.Vec3{x0, y1, z1}
	v100 := tensor.Vec3{x1, y0, z0}
	v101 := tensor.Vec3{x1, y0, z1}
	v110 := tensor.Vec3{x1, y1, z0}
	v111 := tensor.Vec3{x1, y1, z1}

	return manifold.New([]manifold.Polygon{
		manifold.SimplePoly{v000, v010, v011, v001}, // left
		manifold.SimplePoly{v100, v101, v111, v110}, // right
		manifold.SimplePoly{v000, v001, v101, v100}, // bottom
		manifold.SimplePoly{v010, v110, v111, v011}, // top
		manifold.SimplePoly{v000, v100, v110, v010}, // back
		manifold.SimplePoly{v001, v011, v111, v101}, // front
	})
}
