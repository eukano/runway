package shape

import "gitlab.com/eukano/runway/pkg/geom/manifold"

// Sweep constructs a set of quadrangles that sweep between two faces. The faces
// must have the same number of verticies.Behavior is undefined if the verticies
// are not in the same order.
func Sweep(from, to manifold.Polygon) manifold.Polyhedron {
	v, u := manifold.Vertices(from), manifold.Vertices(to)
	if len(v) != len(u) {
		panic("cannot sweep between faces with different arity")
	}

	n := len(v)
	faces := make([]manifold.Polygon, n)
	for i := range faces {
		j := (i + 1) % n
		faces[i] = manifold.SimplePoly{v[i], v[j], u[j], u[i]}
	}
	return manifold.New(faces)
}
