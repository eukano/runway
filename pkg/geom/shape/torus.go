package shape

import (
	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// Torus creates a torus (r=R₂) by revolving a circle (r=R₁) about the Y axis,
// centered on the origin.
func Torus(r1, r2 float64) manifold.Polyhedron {
	// TODO take axis as parameter
	const N, M = 50, 50
	profile := Circle(N, r1, tensor.Vec3{r2, 0, 0}, tensor.Vec3{0, 0, 1})
	return Revolve(profile, tensor.Vec3{0, 1, 0}, M)
}
