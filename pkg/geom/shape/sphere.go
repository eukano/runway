package shape

import (
	"math"

	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// Sphere constructs a spherical shape with 50 longitudinal and 50 latitudinal
// bands.
func Sphere(radius float64, center tensor.Vec3) manifold.Polyhedron {
	const Lats, Longs = 50, 50
	// const Lats, Longs = 8, 16
	// const Lats, Longs = 4, 8
	return sphere(radius, center, Lats, Longs)
}

//go:inline
func sphere(radius float64, center tensor.Vec3, Lats, Longs int) manifold.Polyhedron {
	vec := func(thSin, thCos, phiSin, phiCos float64) tensor.Vec3 {
		return tensor.Vec3{
			center[0] + radius*thSin*phiCos,
			center[1] + radius*thCos,
			center[2] + radius*thSin*phiSin,
		}
	}

	shape := make([]manifold.Polygon, Lats*Longs)

	var th0sin, th0cos float64 = 0, 1
	for lat := 0; lat < Lats; lat++ {
		th1 := math.Pi * float64(lat+1) / float64(Lats)
		th1sin := math.Sin(th1)
		th1cos := math.Cos(th1)

		var phi0sin, phi0cos float64 = 0, 1
		for long := 0; long < Longs; long++ {
			phi1 := 2 * math.Pi * float64(long+1) / float64(Longs)
			phi1sin := math.Sin(phi1)
			phi1cos := math.Cos(phi1)

			shape[lat*Longs+long] = simplify(manifold.SimplePoly{
				vec(th0sin, th0cos, phi0sin, phi0cos),
				vec(th1sin, th1cos, phi0sin, phi0cos),
				vec(th1sin, th1cos, phi1sin, phi1cos),
				vec(th0sin, th0cos, phi1sin, phi1cos),
			})

			phi0sin, phi0cos = phi1sin, phi1cos
		}
		th0sin, th0cos = th1sin, th1cos
	}

	return manifold.New(shape)
}
