package shape

import (
	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// Cylinder creates a Shape by extruding a circle (r=½) along a vector.
func Cylinder(center, along tensor.Vec3) manifold.Polyhedron {
	const N = 50
	profile := Circle(N, 0.5, center, along)
	return Extrude(profile, along)
}
