// Package shape implements common shapes (e.g. cubes and spheres), shape
// generating procedures (e.g. extrusion and revolution), and matrix
// transformations of shapes.
package shape
