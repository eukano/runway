package shape

import (
	"math"

	. "gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// orders A and B in ascending order
func ascending(a, b *float64) {
	if *a > *b {
		*b, *a = *a, *b
	}
}

// shifts F from one plane to another
func shiftPlane(from, to tensor.Vec3, f Polygon) Polygon {
	axis := from.Cross(to)
	if axis == (tensor.Vec3{}) {
		return f
	}

	cos := from.Dot(to) / from.Len() / to.Len()
	angle := math.Acos(cos)
	return TransformFace(f, Rotate(angle, axis.Normalize()))
}

// simplifies F by removing identical adjacent vertices
func simplify(f Polygon) Polygon {
	n := f.Len()
	if n < 2 {
		return f
	}

	v := Vertices(f)
	for v[0].Sub(v[len(v)-1]).LenSqr() < 1e-9 {
		v = v[:len(v)-1]
	}

	i := 0
	for i+1 < len(v) {
		if v[i].Sub(v[i+1]).LenSqr() < 1e-9 {
			v = append(v[:i+1], v[i+2:]...)
		} else {
			i++
		}
	}

	if n == len(v) {
		return f
	}

	g := make(SimplePoly, len(v))
	copy(g, v)
	return g
}

// checks if A and B contain the same set of vertices
func equal(a, b Polygon) bool {
	if a.Len() != b.Len() {
		return false
	}

	for i := 0; i < a.Len(); i++ {
		if a.Get(i) != b.Get(i) {
			return false
		}
	}

	return true
}
