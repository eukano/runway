package manifold

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

func TestVolume_cutFace(t *testing.T) {
	v := Volume{
		Min: tensor.Vec3{-1, -1, -1},
		Max: tensor.Vec3{+1, +1, +1},
	}
	vc := volumeCut{
		Volume: v,
	}

	var f Polygon = SimplePoly{
		{0, 0, 2},
		{0, 2, 0},
		{2, 0, 0},
	}

	f = vc.cutFace(f, false, 0)
	f = vc.cutFace(f, false, 1)
	f = vc.cutFace(f, false, 2)
	f = vc.cutFace(f, true, 0)
	f = vc.cutFace(f, true, 1)
	f = vc.cutFace(f, true, 2)
	require.Equal(t, SimplePoly{
		{0, 1, 1},
		{1, 1, 0},
		{1, 0, 1},
	}, f)
	require.Equal(t, [6][]Line{
		nil,
		{{{1, 0, 1}, {1, 1, 0}}},
		nil,
		{{{1, 1, 0}, {0, 1, 1}}},
		nil,
		{{{0, 1, 1}, {1, 0, 1}}},
	}, vc.perimeter)
	require.Equal(t, [6][4][]edgeCrossing{
		{nil, nil, nil, nil},
		{{{point: 0.5, into: true}}, nil, nil, {{point: 0.5, into: false}}},
		{nil, nil, nil, nil},
		{{{point: 0.5, into: true}}, nil, nil, {{point: 0.5, into: false}}},
		{nil, nil, nil, nil},
		{{{point: 0.5, into: true}}, nil, nil, {{point: 0.5, into: false}}},
	}, vc.crossings)
}
