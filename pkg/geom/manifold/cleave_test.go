package manifold_test

import (
	"testing"

	. "gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/shape"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

func TestCleave(t *testing.T) {
	o := shape.Revolve(SimplePoly{
		{2, 0},
		{2, 2},
		{3, 2},
		{3, 1},
		{4, 1},
		{4, 2},
		{5, 2},
		{5, 0},
	}, tensor.Vec3{0, 1}, 10)

	knife := tensor.Vec3{1, 6}.Normalize()
	a, b := Cleave(o, tensor.Vec3{0, 1.5}, knife)

	for i, s := range []Polyhedron{a, b} {
		for j, f := range Faces(s) {
			if f.Len() < 10 {
				continue
			}
			t.Logf("shape %d face %d: %d vertices, normal %v", i, j, f.Len(), NormalSum(f).Normalize().Dot(knife))
		}
	}
}
