package manifold

import (
	"fmt"
	"strings"

	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

type nesting struct {
	data []int
	size int
}

func (n *nesting) get(i, j int) *int {
	if n.data == nil {
		n.data = make([]int, n.size*n.size)
	}
	return &n.data[n.size*i+j]
}

func (n *nesting) contains(i int) []int {
	if n.data == nil {
		return make([]int, n.size)
	}
	return n.data[n.size*i : n.size*(i+1)]
}

func (n *nesting) isContained(j int) bool {
	if n.data == nil {
		return false
	}
	for i := 0; i < n.size; i++ {
		if *n.get(i, j) > 0 {
			return true
		}
	}
	return false
}

func (n *nesting) String() string {
	var sb strings.Builder
	for i := 0; i < n.size; i++ {
		if i > 0 {
			sb.WriteRune('\n')
		}
		for j := 0; j < n.size; j++ {
			v := *n.get(i, j)
			if v == 0 {
				sb.WriteString(" 0 ")
			} else {
				sb.WriteString(fmt.Sprintf("%+d ", v))
			}
		}
	}
	return sb.String()
}

func determineNesting(normal tensor.Vec3, faces []Polygon) *nesting {
	N := len(faces)
	within := &nesting{size: N}
	nested := false

	d0, d1, _ := Pointing(normal)
	for i, p := range faces {
		for j, q := range faces {
			if i == j {
				continue
			}

			w := Winding(p, q.Get(0), d0, d1)
			*within.get(i, j) = w
			if w != 0 {
				nested = true
			}
		}
	}

	if !nested {
		return within
	}

	for x, w := range within.data {
		i, j := x/N, x%N

		// Q -> perimeter I
		// R -> perimeter J
		// S -> perimeter K

		// Q anticontains R?
		if w >= 0 {
			continue
		}

		for k := 0; k < N; k++ {
			// S contains R?
			if *within.get(k, j) <= 0 {
				continue
			}

			// S contains Q?
			if *within.get(k, i) <= 0 {
				continue
			}

			// S contains Q and Q anticontains R, so reset S contains Q
			*within.get(k, j) = 0
		}
	}

	return within
}
