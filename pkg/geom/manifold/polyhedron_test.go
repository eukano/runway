package manifold_test

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	. "gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/shape"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

func TestManifold_Contains(t *testing.T) {
	t.Run("Sphere", func(t *testing.T) {
		p := tensor.Vec3{-0.4, 1, 0}
		h := shape.Sphere(1, tensor.Vec3{+0.4}).(*Manifold)
		require.Equal(t, OutsidePoint.String(), h.Winding(p).String())
	})

	t.Run("Octahedron", func(t *testing.T) {
		a := New([]Polygon{
			SimplePoly{{+0.0, +1.0, +0.0}, {+1.0, +0.0, +0.0}, {+0.0, +0.0, +1.0}},
			SimplePoly{{+0.0, +1.0, +0.0}, {+0.0, +0.0, -1.0}, {+1.0, +0.0, +0.0}},
			SimplePoly{{+0.0, +1.0, +0.0}, {-1.0, +0.0, +0.0}, {+0.0, +0.0, -1.0}},
			SimplePoly{{+0.0, +1.0, +0.0}, {+0.0, +0.0, +1.0}, {-1.0, +0.0, +0.0}},
			SimplePoly{{+0.0, -1.0, +0.0}, {+0.0, +0.0, +1.0}, {+1.0, +0.0, +0.0}},
			SimplePoly{{+0.0, -1.0, +0.0}, {+1.0, +0.0, +0.0}, {+0.0, +0.0, -1.0}},
			SimplePoly{{+0.0, -1.0, +0.0}, {+0.0, +0.0, -1.0}, {-1.0, +0.0, +0.0}},
			SimplePoly{{+0.0, -1.0, +0.0}, {-1.0, +0.0, +0.0}, {+0.0, +0.0, +1.0}},
		})

		const R2 = 1.0 / 2
		const R3 = 1.0 / 3
		cases := []struct {
			P tensor.Vec3
			W PointLocation
		}{
			// In the center
			{P: tensor.Vec3{+0, +0, +0}, W: InsidePoint},

			// Through a vertex
			{P: tensor.Vec3{+2, +0, +0}, W: OutsidePoint},
			{P: tensor.Vec3{-2, +0, +0}, W: OutsidePoint},

			// Through an edge
			{P: tensor.Vec3{+2, R2, 0}, W: OutsidePoint},
			{P: tensor.Vec3{-2, R2, 0}, W: OutsidePoint},

			// Through a face
			{P: tensor.Vec3{+2, R3, R3}, W: OutsidePoint},
			{P: tensor.Vec3{-2, R3, R3}, W: OutsidePoint},

			// Vertices
			{P: a.Vertex[0], W: BoundaryPoint},
			{P: a.Vertex[1], W: BoundaryPoint},
			{P: a.Vertex[2], W: BoundaryPoint},
			{P: a.Vertex[3], W: BoundaryPoint},
			{P: a.Vertex[4], W: BoundaryPoint},
			{P: a.Vertex[5], W: BoundaryPoint},

			// Edges
			{P: a.Vertex[0].Add(a.Vertex[1]).Mul(R2), W: BoundaryPoint},
			{P: a.Vertex[1].Add(a.Vertex[2]).Mul(R2), W: BoundaryPoint},
			{P: a.Vertex[0].Add(a.Vertex[2]).Mul(R2), W: BoundaryPoint},
			{P: a.Vertex[0].Add(a.Vertex[3]).Mul(R2), W: BoundaryPoint},
			{P: a.Vertex[1].Add(a.Vertex[3]).Mul(R2), W: BoundaryPoint},
			{P: a.Vertex[0].Add(a.Vertex[4]).Mul(R2), W: BoundaryPoint},
			{P: a.Vertex[3].Add(a.Vertex[4]).Mul(R2), W: BoundaryPoint},
			{P: a.Vertex[2].Add(a.Vertex[4]).Mul(R2), W: BoundaryPoint},
			{P: a.Vertex[2].Add(a.Vertex[5]).Mul(R2), W: BoundaryPoint},
			{P: a.Vertex[1].Add(a.Vertex[5]).Mul(R2), W: BoundaryPoint},
			{P: a.Vertex[3].Add(a.Vertex[5]).Mul(R2), W: BoundaryPoint},
			{P: a.Vertex[4].Add(a.Vertex[5]).Mul(R2), W: BoundaryPoint},

			// Faces
			{P: a.Vertex[0].Add(a.Vertex[1]).Add(a.Vertex[2]).Mul(R3), W: BoundaryPoint},
			{P: a.Vertex[0].Add(a.Vertex[3]).Add(a.Vertex[1]).Mul(R3), W: BoundaryPoint},
			{P: a.Vertex[0].Add(a.Vertex[4]).Add(a.Vertex[3]).Mul(R3), W: BoundaryPoint},
			{P: a.Vertex[0].Add(a.Vertex[2]).Add(a.Vertex[4]).Mul(R3), W: BoundaryPoint},
			{P: a.Vertex[5].Add(a.Vertex[2]).Add(a.Vertex[1]).Mul(R3), W: BoundaryPoint},
			{P: a.Vertex[5].Add(a.Vertex[1]).Add(a.Vertex[3]).Mul(R3), W: BoundaryPoint},
			{P: a.Vertex[5].Add(a.Vertex[3]).Add(a.Vertex[4]).Mul(R3), W: BoundaryPoint},
			{P: a.Vertex[5].Add(a.Vertex[4]).Add(a.Vertex[2]).Mul(R3), W: BoundaryPoint},
		}

		for _, c := range cases {
			var s []string
			for _, v := range c.P {
				s = append(s, fmt.Sprintf("%.2f", v))
			}
			t.Run(fmt.Sprintf("[%s]", strings.Join(s, ",")), func(t *testing.T) {
				require.Equal(t, c.W.String(), a.Winding(c.P).String())
			})
		}
	})
}
