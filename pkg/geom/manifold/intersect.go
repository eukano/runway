package manifold

import (
	"iter"
	"math"

	"gitlab.com/eukano/runway/internal/tree"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

func Union(a, b Polyhedron) Polyhedron {
	return intersect(
		AsManifold(a),
		AsManifold(b),
		OutsidePoint,
		OutsidePoint,
		InsidePoint)
}

func Intersect(a, b Polyhedron) Polyhedron {
	return intersect(
		AsManifold(a),
		AsManifold(b),
		InsidePoint,
		InsidePoint,
		OutsidePoint)
}

func Subtract(a, b Polyhedron) Polyhedron {
	return intersect(
		AsManifold(a),
		AsManifold(b),
		OutsidePoint,
		InsidePoint,
		InsidePoint)
}

func intersect(a, b *Manifold, keepA, keepB, flip PointLocation) Polyhedron {
	// TODO: Make all the faces of A and B convex (e.g. ear clipping). Can we do
	// all the ear clipping in AsManifold?

	// Calculate the intersection volume
	xv, ok := a.Bounds().Intersection(b.Bounds())
	if !ok {
		panic("A and B have no overlap")
	}

	// Cut intersecting faces
	x := new(intersector)
	keep := map[Polygon][2]bool{}
	for face, typ := range x.cutManifold(a, b, xv) {
		keep[face] = [2]bool{typ == keepA, typ == flip}
	}
	for face, typ := range x.cutManifold(b, a, xv) {
		keep[face] = [2]bool{typ == keepB, typ == flip}
	}

	var result []Polygon
	add := func(face Polygon, flip bool) {
		if flip {
			result = append(result, Flip(face))
		} else {
			result = append(result, face)
		}
	}

	for face, keep := range keep {
		if keep[0] {
			add(face, keep[1])
		}
	}

	abWithin := vertWithin(a, b)
	for i, f := range a.Faces() {
		// If F has been cut, skip it
		if _, ok := keep[f]; ok {
			continue
		}

		// If F does not have points on both sides and has at least one point on
		// the desired side, keep it
		w := distancesForFace(a, i, abWithin)
		if !w.HasPointsOnBothSides() && w.Count(keepA) > 0 {
			add(f, keepA == flip)
		}
	}

	baWithin := vertWithin(b, a)
	for i, g := range b.Faces() {
		if _, ok := keep[g]; ok {
			continue
		}
		w := distancesForFace(b, i, baWithin)
		if !w.HasPointsOnBothSides() && w.Count(keepB) > 0 {
			add(g, keepB == flip)
		}
	}

	return New(result)
}

type cacheKeyDistance struct {
	Q, N, P tensor.Vec3
}

type intersector struct {
	normals     map[Polygon]tensor.Vec3
	distance    map[cacheKeyDistance]float64
	edgeVectors map[Polygon][]tensor.Vec3
}

func (x *intersector) cutManifold(a, b *Manifold, xv tensor.Span3) iter.Seq2[Polygon, PointLocation] {
	return func(yield func(Polygon, PointLocation) bool) {
		// For each face F of A overlapping the intersection area X
		for f := range a.OverlappingFaces(tree.SimpleBounds(xv)) {
			for face, typ := range x.cutWithFaces(f, b) {
				if !yield(face, typ) {
					return
				}
			}
		}
	}
}

func (x *intersector) cutWithFaces(f Polygon, b *Manifold) iter.Seq2[Polygon, PointLocation] {
	return func(yield func(Polygon, PointLocation) bool) {
		// Until there's nothing left to cut
		faces, next := []Polygon{f}, []Polygon{}
		for ; len(faces) > 0; faces, next = next, faces[:0] {
			for _, f := range faces {
				if f == nil {
					continue
				}

				// For each face G of B overlapping F
				for g := range b.OverlappingFaces(polyAsBounded(f)) {
					// Cut F with G
					x, y, ok := x.cutFace(f, g)
					if !ok {
						// F was not cut
						continue
					}

					// Record the inner and outer fragments
					if !yield(f, BoundaryPoint) ||
						!yield(x, OutsidePoint) ||
						!yield(y, InsidePoint) {
						return
					}

					// Check the fragments for intersections
					if x != nil && y != nil {
						next = append(next, x, y)
						break
					}
				}
			}
		}
	}
}

func (x *intersector) cutFace(f, g Polygon) (Polygon, Polygon, bool) {
	// Calculate the distance from each vertex of F to the plane of G and check
	// if F should be cut by G.
	//
	// F should only be cut by G if F has points on both sides of G.
	gq, gn := g.Get(0), x.Normal(g)
	distFG := x.VertexDistanceToPlane(gq, gn, f)
	if !distFG.HasPointsOnBothSides() {
		return nil, nil, false
	}

	// Should G cut F?
	if !x.faceMayCut(f, g) {
		return nil, nil, false
	}

	var top, bottom SimplePoly
	for i, d := range distFG {
		v := f.Get(i)

		// If V is on the plane of G
		if d == 0 {
			c, e := nextNonZeroDist(f, i, -1, distFG), nextNonZeroDist(f, i, +1, distFG)

			// Add V to both sides if its neighbors are on opposite sides
			if c*e < 0 {
				top = append(top, v)
				bottom = append(bottom, v)
				continue
			}

			// Otherwise add V to the same side as its neighbors
			if c > 0 {
				top = append(top, v)
			} else {
				bottom = append(bottom, v)
			}
			continue
		}

		// V is off the line so add it to the appropriate side
		if d > 0 {
			top = append(top, v)
		} else {
			bottom = append(bottom, v)
		}

		// Move on if U is on the same side
		j := (i + 1) % f.Len()
		if d*distFG[j] >= 0 {
			continue
		}

		// Find the point X where VU intersects P
		u := f.Get(j)
		x, d, ok := tensor.IntersectPlane(v, u, gq, gn)
		if !ok || d < 0 || d > 1 {
			// X must necessarily exist and be within VU if V and U are on
			// opposite sides of P
			panic("internal error")
		}

		// Round and add X to both sides
		x = tensor.Round3(x)
		top = append(top, x)
		bottom = append(bottom, x)
	}

	if len(top) == 0 || len(bottom) == 0 {
		// There are vertices of F on both sides of G yet somehow they're all on
		// the same side...
		panic("internal error")
	}
	return &top, &bottom, true
}

// faceMayCut returns true if G may cut F. faceMayCut returns false if G
// definitely cannot cut F.
func (x *intersector) faceMayCut(f, g Polygon) bool {
	// G should cut F if G has points on both sides of G **or** G has at least
	// one edge coplanar with F and at least one point of that edge is strictly
	// within F **unless** G is entirely coplanar with F.

	// Calculate the distance from each vertex of G to the plane of F.
	fq, fn := f.Get(0), x.Normal(f)
	distGF := x.VertexDistanceToPlane(fq, fn, g)

	// If G has points on both sides of F, F and G intersects if there is not a
	// plane that separates them (non-exclusive: F and G may have points on the
	// plane as long as they don't have points on both sides).
	if distGF.HasPointsOnBothSides() {
		return !x.SeparatingPlaneExists(f, g, false)
	}

	copEdges := distGF.CoplanarEdges()
	if distGF.AllOnSameSide() || len(copEdges) == 0 {
		// All points are coplanar, inside, or outside; or some points are
		// coplanar and some are inside or outside (but not inside *and*
		// outside) but none of the coplanar points are adjacent (non of the
		// edges of G are embedded in F).
		return false
	}

	// If G is not a polygon of a manifold we don't have enough info to check if
	// the embedded edge should cut F.
	gm, ok := g.(*mpoly)
	if !ok {
		return false
	}

	// If all of the coplanar edges of G are on the perimeter or outside of F, G
	// should not cut F.
	wd0, wd1, _ := Pointing(fn)
	for _, i := range copEdges {
		// Should we consider cutting with this edge?
		if !x.edgeMayCut(f, g, i, wd0, wd1) {
			continue
		}

		// Get the edge H that shares this edge with G.
		h := gm.neighbor(i)
		if h == nil {
			// We may want to be forgiving and send an error message and do our
			// best instead of dying
			panic("discontinuous manifold")
			// continue
		}

		// If H has points on the opposite side of F from G, then G should cut
		// F.
		distHF := x.VertexDistanceToPlane(fq, fn, h)
		if append(distGF, distHF...).HasPointsOnBothSides() {
			return true
		}
	}

	// None of the edges are inside
	return false
}

// edgeMayCut returns true if the given edge of G may cut F. edgeMayCut returns
// false if the edge definitely cannot cut F.
func (x *intersector) edgeMayCut(f, g Polygon, i, wd0, wd1 int) bool {
	v1, v2 := g.Get(i), g.Get((i+1)%g.Len())

	// If both points are outside F, don't cut.
	w1, w2 := Winding(f, v1, wd0, wd1), Winding(f, v2, wd0, wd1)
	if w1 == 0 && w2 == 0 {
		return false
	}

	// What if both points are outside but the line between them crosses through
	// F? If at least one point is outside, every other check could be replaced
	// with: "does G's edge intersect any of F's edges?"

	// If both points are colinear with the same edge of F, don't cut. If both
	// points are colinear with different edges, cut.
	e1, p1 := LiesOnPerimeter(f, v1, wd0, wd1)
	e2, p2 := LiesOnPerimeter(f, v2, wd0, wd1)
	if p1 && p2 {
		return e1 != e2
	}

	// If both points are inside, cut.
	if w1 != 0 && w2 != 0 {
		return true
	}

	// We've excluded both points outside, both points inside, and both points
	// on the perimeter. Thus one must be outside and the other must be inside,
	// possibly on the perimeter. If the inside point is on the perimeter, don't
	// cut.
	if p1 || p2 {
		return false
	}

	// The edge must cross the perimeter.
	return true
}

// nextNonZeroDist find the next vertex of F that has a non-zero distance from P
// starting from I.
func nextNonZeroDist(f Polygon, i, dir int, dist distanceSet) PointLocation {
	for range dist {
		i += dir
		for i < 0 {
			i += f.Len()
		}
		d := dist[i%f.Len()]
		if d != 0 {
			return d
		}
	}

	// If F and G are not coplanar, some vertex of F must necessarily have a
	// non-zero distance to P
	panic("invalid face")
}

func distancesForFace(m *Manifold, i int, s distanceSet) distanceSet {
	f := m.Face[i]
	r := make(distanceSet, len(f))
	for j, i := range f {
		r[j] = s[i]
	}
	return r
}

type polyWithVolume struct {
	Polygon
	bounds Volume
}

func (p polyWithVolume) Bounds() tensor.Span3 { return p.bounds.Bounds() }

func polyAsBounded(p Polygon) BoundedPolygon {
	if b, ok := p.(BoundedPolygon); ok {
		return b
	}

	v := NanVolume()
	v.EncompassFace(p)
	return polyWithVolume{p, v}
}

// vertWithin builds a lookup of vertices of A that are contained within B.
func vertWithin(a, b *Manifold) distanceSet {
	x := make(distanceSet, len(a.Vertex))
	for i, v := range a.Vertex {
		x[i] = b.Winding(v)
	}
	return x
}

func (x *intersector) Normal(y Polygon) (n tensor.Vec3) {
	if m, ok := y.(*mpoly); ok {
		return m.normal()
	}

	if n, ok := x.normals[y]; ok {
		return n
	}

	defer func() { x.normals[y] = n }()
	if x.normals == nil {
		x.normals = map[Polygon]tensor.Vec3{}
	}

	return normal(y)
}

func normal(y Polygon) tensor.Vec3 {
	if m, ok := y.(*mpoly); ok {
		return m.normal()
	}

	if IsConvex(y) {
		return Normal(y)
	} else {
		return NormalSum(y).Normalize()
	}
}

// DistanceToPlane calculates the distance of point P from the line described by point
// Q and normal N.
func (x *intersector) DistanceToPlane(q, n, p tensor.Vec3) (d float64) {
	if x.distance == nil {
		x.distance = map[cacheKeyDistance]float64{}
	}

	// This may be redundant - normalizing N and minimizing Q would reduce the
	// cache size - but that would increase the computational cost.
	key := cacheKeyDistance{q, n, p}
	if d, ok := x.distance[key]; ok {
		return d
	}
	defer func() { x.distance[key] = d }()

	return tensor.DistancePlane(p, q, n)
}

func (x *intersector) DistanceToFace(y Polygon, p tensor.Vec3) float64 {
	return x.DistanceToPlane(y.Get(0), x.Normal(y), p)
}

type distanceSet []PointLocation

func (x *intersector) VertexDistanceToPlane(q, n tensor.Vec3, y Polygon) distanceSet {
	dist := make(distanceSet, y.Len())
	for i := range dist {
		d := x.DistanceToPlane(q, n, y.Get(i))
		switch {
		case d > 0:
			dist[i] = OutsidePoint
		case d < 0:
			dist[i] = InsidePoint
		default:
			dist[i] = BoundaryPoint
		}
	}
	return dist
}

// Count returns the number of distances with the given sign.
func (d distanceSet) Count(sign PointLocation) int {
	var c int
	for _, d := range d {
		if d == sign {
			c++
		}
	}
	return c
}

func (d distanceSet) AllOnSameSide() bool {
	switch len(d) {
	case d.Count(OutsidePoint):
		return true
	case d.Count(BoundaryPoint):
		return true
	case d.Count(InsidePoint):
		return true
	}
	return false
}

func (d distanceSet) HasPointsOnBothSides() bool {
	in, out := d.Count(InsidePoint), d.Count(OutsidePoint)
	return in > 0 && out > 0
}

func (d distanceSet) CoplanarEdges() []int {
	var e []int
	for i := range d {
		a, b := d[i], d[(i+1)%len(d)]
		if a == 0 && b == 0 {
			e = append(e, i)
		}
	}
	return e
}

// FaceSide returns +1 or -1 if all of the points of Y are outside or inside
// (respectively) plane P described by point Q and normal N. FaceSide returns 0
// if all points are on Y and NaN if there are points on both sides.
func (x *intersector) FaceSide(q, n tensor.Vec3, y Polygon) float64 {
	// If a pair of vertices V and U exist where their distances to P
	// have opposite signs, V and U are on opposite sides of P

	s := 0.0
	for i := range y.Len() {
		d := x.DistanceToPlane(q, n, y.Get(i))
		if s == 0 {
			s = d
		} else if s*d < 0 {
			return math.NaN()
		}
	}
	return s
}

func (x *intersector) EdgeVectors(y Polygon) (e []tensor.Vec3) {
	if e, ok := x.edgeVectors[y]; ok {
		return e
	}

	defer func() { x.edgeVectors[y] = e }()
	if x.edgeVectors == nil {
		x.edgeVectors = map[Polygon][]tensor.Vec3{}
	}

	e = make([]tensor.Vec3, y.Len())
	for i := range e {
		v, u := y.Get(i), y.Get((i+1)%len(e))
		e[i] = u.Sub(v)
	}
	return e
}

// SeparatingPlaneExists checks whether an plane exists that separates polygons
// F and G.
func (x *intersector) SeparatingPlaneExists(f, g Polygon, exclusive bool) bool {
	// For each edge E of F and edge H of G, for each edge pair (E, H), check if
	// plane P described by the first point Q of E and the normal N = E × H
	// separates F and G
	fe, ge := x.EdgeVectors(f), x.EdgeVectors(g)
	for i, e := range fe {
		_ = i
		for j, h := range ge {
			q := g.Get(j)
			n := tensor.Round3(e.Cross(h))

			// Skip if the points of E or H are too close together (if the
			// magnitude of N is ~zero)
			if n.LenSqr() < tensor.EpsSqr {
				continue
			}

			// Given plane P described by point Q of E and normal N, P separates
			// F and G if and only if the vertices of F and G are on opposite
			// sides of P. If either F or G has points on both sides of P then R
			// or S (respectively) will be NaN, the product will be NaN, and the
			// condition will be false.
			s := x.FaceSide(q, n, g)
			r := x.FaceSide(q, n, f)
			if exclusive {
				if r*s < 0 {
					return true
				}
			} else {
				if r*s <= 0 {
					return true
				}
			}
		}
	}

	return false
}
