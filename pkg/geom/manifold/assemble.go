package manifold

import (
	"fmt"
	"log"
	"sort"

	"gitlab.com/eukano/runway/internal/debug"
	"gitlab.com/eukano/runway/internal/tree"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

var debugAssemble struct {
	Segments, Steps, Facets debug.Logger
}

// AssembleLoops builds one or more simple polygons from a set of line segments.
// The represented polygons do not need to be in any particular orientation
// (counther/clockwise), but the vertices for segments of any given loop must be
// ordered consistently head-to-tail. That is, two adjacent line segments V₀V₁
// and V₁V₂ must be ordered {V₀, V₁} and {V₁, V₂}.
//
// Loops are assembled iteratively; if the previous line segment was V₀V₁, the
// next line segment will be V₁V₂. If there is no unclaimed line segment
// beginning with V₁, the current loop is terminated. If V₁ is the same as the
// first vertex of the current loop, the current loop is terminated. If multiple
// unclaimed line segments begin with V₁, the one with the smallest interior
// angle is used.
//
// All points are expected to be co-planar, with the normal vector describing
// the orientation of that plane. Behavior is unspecified if line segments are
// not co-planar or not in the plane described by the normal vector.
//
// Behavior is unspecified if the set contains line segments that are not part
// of a closed loop.
func AssembleLoops(segments []Line, normal tensor.Vec3) []Polygon {
	if len(segments) == 0 {
		return nil
	}
	a := assembler{
		segments:  segments,
		normal:    normal,
		remaining: map[int]bool{},
		facets:    make([]Polygon, 0, 2),
	}
	a.assemble()
	return a.facets
}

// implements assemblePerimeterFacets
type assembler struct {
	segments  []Line      // the line segments
	normal    tensor.Vec3 // the normal vector
	remaining map[int]bool
	facets    []Polygon
}

// implements assemblePerimeterFacets
func (a *assembler) assemble() {
	// Build a k-d tree of the line segments, but only for the first vertex of
	// each segment
	var vds tree.VertexDataSet[int]
	for i, n := 0, len(a.segments); i < n; i++ {
		vds = vds.Append(a.segments[i][0], i)
		a.remaining[i] = true
	}
	lookup := vds.AsTree()

	debugAssemble.Segments.Logfn(func(l *log.Logger) {
		l.Printf("%d segments\n", len(a.segments))
		for _, f := range a.segments {
			v, u := f[0], f[1]
			l.Printf("{{%+.17f,%+.17f,%+.17f}, {%+.17f,%+.17f,%+.17f}},\n", v[0], v[1], v[2], u[0], u[1], u[2])
		}
		l.Println()
	})

	facet := make(SimplePoly, 0, len(a.segments))
	a.facets = make([]Polygon, 0, 2)

	// Start with the first vertex of the first line segment
	s := a.take(0)
	facet = append(facet, s[0])

	// Build the perimeter facets
	for {
		// If the current facet is not a closed loop
		if s[1].Sub(facet[0]).LenSqr() > tensor.AdjacentEps {
			// Find the line segment that starts with the second vertex of S
			k := lookup.NearestSet(s[1], tensor.AdjacentEps)

			var ok bool
			if s, ok = a.choose(s, k); ok {
				facet = append(facet, s[0])
				continue
			}
		}

		// Add the completed facet to the list
		if len(facet) < 3 {
			panic(fmt.Errorf("assembled facet with only %d vertices", len(facet)))
		}
		a.facets = append(a.facets, facet)

		// If no more segments remain, we're done
		if len(a.remaining) == 0 {
			break
		}

		// Reinitialize the facet array
		facet = make(SimplePoly, 0, len(a.segments))

		for i := range a.remaining {
			// Start (over) with one of the remaining line segments - the choice
			// is arbitrary
			s = a.segments[i]
			delete(a.remaining, i)
			facet = append(facet, s[0])
			break
		}
	}

	debugAssemble.Facets.Logfn(func(l *log.Logger) {
		l.Printf("assembled %d facets\n", len(a.facets))
		for i, f := range a.facets {
			l.Printf("facet %d\n---\n", i)
			for _, v := range Vertices(f) {
				l.Printf("{%+.9f,%+.9f,%+.9f},\n", v[0], v[1], v[2])
			}
		}
		l.Println()
	})
}

func (a *assembler) choose(prev Line, k []tree.VertexData[int]) (Line, bool) {
	type candidate struct {
		i     int
		angle float64
	}
	v := prev[0].Sub(prev[1])
	candidates := make([]*candidate, 0, len(k))
	for _, c := range k {
		i := c.Data
		if !a.remaining[i] {
			continue
		}

		s := a.segments[i]
		d := &candidate{i: i}
		candidates = append(candidates, d)

		// 0 -> +2; 90 -> +1; 180 -> 0; 270 -> -1; 360 -> -2
		u := s[1].Sub(s[0])
		dot, cross := v.Dot(u)/v.Len()/u.Len(), v.Cross(u)
		if cross.LenSqr() < tensor.AdjacentEps {
			d.angle = 0
			continue
		}

		if cross.Dot(a.normal) > 0 {
			d.angle = dot + 1
		} else {
			d.angle = -(dot + 1)
		}
	}

	debugAssemble.Steps.Logf(
		"%d segments close to <%.2f, %.2f, %.2f>, %d usable\n",
		len(k), prev[0][0], prev[0][1], prev[0][2], len(candidates),
	)

	if len(candidates) == 0 {
		return Line{}, false
	}

	// Select the candidate with the lowest inner angle
	sort.Slice(candidates, func(i, j int) bool { return candidates[i].angle > candidates[j].angle })
	return a.take(candidates[0].i), true
}

func (a *assembler) take(i int) Line {
	delete(a.remaining, i)
	s := a.segments[i]

	// if any duplicates exist, remove those
	var same []int
	for j := range a.remaining {
		t := a.segments[j]
		if s[0].Sub(t[0]).Len() > tensor.AdjacentEps {
			continue
		}
		if s[1].Sub(t[1]).Len() > tensor.AdjacentEps {
			continue
		}
		same = append(same, j)
	}
	for _, j := range same {
		delete(a.remaining, j)
	}

	return s
}
