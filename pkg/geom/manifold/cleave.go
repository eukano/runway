package manifold

import (
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// Cleave cleaves the shape in two with the plane described by point Q and
// normal N, returning the subshapes that are in front and back of the plane.
//
// The shape must represent a closed surface - it must have a well defined
// inside and outside and no holes.
func Cleave(s Polyhedron, q, n tensor.Vec3) (_, _ Polyhedron) {
	// The only assumptions we can make about the shape are that it represents a
	// closed surface - that is it has an inside and an outside and no holes. We
	// cannot make assumptions about the order or orientation of facets. Thus we
	// cannot make assumptions about the order in which we will encounter
	// vertices on the perimeter of the cut. Given a simple list of points on
	// the perimeter, determining how to arrange them into faces is very
	// difficult, perhaps impossible for complex and/or concave shapes. But
	// because we require the shape to be a closed surface of facets, the
	// perimeter must be one or more closed loops of line segments. If we record
	// a list of those line segments, we can stitch them back together into
	// perimeter loops.

	// The front and back shapes cannot have more facets than the original shape
	N := s.Len()
	front := make([]Polygon, 0, N)
	back := make([]Polygon, 0, N)
	var psegments []Line

	// For each facet of the shape...
	for i := 0; i < N; i++ {
		f := s.Get(i)

		// Does it intersect the knife plane?
		switch IntersectsPlane(f, q, n) {
		case +1:
			// Facet is in front of the knife
			front = append(front, f)
			continue
		case -1:
			// Facet is behind the knife
			back = append(back, f)
			continue
		}

		// The facet intersects the knife, split it
		vfront, vback, perim, ok := cleaveFace(f, q, n)

		// If the facet lies entirely on the plane
		if !ok {
			// Add it to the front and back but not the perimeter
			front = append(front, f)
			back = append(back, f)
			continue
		}

		if vfront != nil {
			front = append(front, vfront)
		}

		if vback != nil {
			back = append(back, vback)
		}

		if perim != nil {
			psegments = append(psegments, *perim)
		}
	}

	for _, facet := range assembleLoopsWithHoles(psegments, n) {
		front = append(front, facet)
		back = append(back, Flip(facet))
	}

	return New(front), New(back)
}

func cleaveFace(f Polygon, q, n tensor.Vec3) (front, back SimplePoly, perim *Line, ok bool) {
	N := f.Len()
	front = make(SimplePoly, 0, N)
	back = make(SimplePoly, 0, N)

	// Calculate distances
	dist := make([]float64, N)
	var zeros int
	for i := 0; i < N; i++ {
		dist[i] = tensor.DistancePlane(f.Get(i), q, n)
		if dist[i] == 0 {
			zeros++
		}
	}

	if zeros == N {
		// Facet is coplanar
		return nil, nil, nil, false
	}

	// Perimeter points:
	// - P₀ must be front-to-back, front-to-plane, or plane-to-back;
	// - P₁ must be back-to-front, plane-to-front, or back-to-plane.
	var p0, p1 tensor.Vec3
	var haveP0, haveP1 bool

	// For each vertex of the facet...
	for i := 0; i < N; i++ {
		// Get vertex I and I+1
		v, u := f.Get(i), f.Get((i+1)%N)
		vd, ud := dist[i], dist[(i+1)%N]

		switch {
		// P₀: front-to-plane
		case vd > 0 && ud == 0:
			p0, haveP0 = u, true

		// P₀: plane-to-back
		case vd == 0 && ud < 0:
			p0, haveP0 = v, true

		// P₁: plane-to-front
		case vd == 0 && ud > 0:
			p1, haveP1 = v, true

		// P₁: back-to-plane
		case vd < 0 && ud == 0:
			p1, haveP1 = u, true
		}

		if vd > 0 {
			// Vertex is in front of the knife
			front = append(front, v)
		} else if vd < 0 {
			// Vertex is behind the knife
			back = append(back, v)
		} else {
			// Vertex is on the knife, so add it to the front and back
			front = append(front, v)
			back = append(back, v)
			continue
		}

		if vd*ud > 0 {
			// V and U are on the same side of the knife
			continue
		} else if ud == 0 {
			// U is on the knife
			continue
		}

		// V and U are on opposite sides of the knife - calculate where the
		// line between V and U intersects the knife
		w, _, _ := tensor.IntersectPlane(v, u, q, n)

		// Add the intersection to the front and back, and the perimeter
		front = append(front, w)
		back = append(back, w)

		if vd > 0 {
			p0, haveP0 = w, true // P₀: front-to-back
		} else {
			p1, haveP1 = w, true // P₁: back-to-front
		}
	}

	// The perimeter is assembled into a face. This face is added to the
	// part of the shape that is in front of the knife, which is to say the
	// part that the knife's normal points at. Thus the knife's normal
	// points into the front part. The assembled perimeter describes the
	// cleaved face of the front part.
	//
	// Perimeter segments are CW with respect to the knife's normal.
	// However, the cleaved face of the front part has a normal exactly
	// opposite that of the knife. Thus, the perimeter segments must be
	// reversed in order to describe the perimeter of the cleaved face of
	// the front part.
	if haveP0 && haveP1 && p0.Sub(p1).Len() > tensor.AdjacentEps {
		perim = &Line{p1, p0}
	}

	if len(front) < 3 {
		front = nil
	}

	if len(back) < 3 {
		back = nil
	}

	return front, back, perim, true
}

func assembleLoopsWithHoles(segments []Line, n tensor.Vec3) []Polygon {
	if len(segments) == 0 {
		return nil
	}

	// Assemble perimeter segments into facets
	facets := AssembleLoops(segments, n.Mul(-1))
	return checkForHoles(facets, n)
}

func checkForHoles(facets []Polygon, n tensor.Vec3) []Polygon {
	within := determineNesting(n, facets)

	var out []Polygon
	for i, N := 0, len(facets); i < N; i++ {
		// Skip any facets that are contained within another
		if within.isContained(i) {
			continue
		}

		// Collect all the holes contained within this facet
		var holes []Polygon
		for j, w := range within.contains(i) {
			if w > 0 {
				holes = append(holes, facets[j])
			}
		}

		// Add all the holes to the facet
		facet := facets[i]
		if len(holes) > 0 {
			facet = AddHoles(facet, holes...)
		}

		// Append
		out = append(out, facet)
	}

	return out
}
