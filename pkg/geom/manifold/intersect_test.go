package manifold

import (
	"fmt"
	"iter"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCutFace(t *testing.T) {
	// X: (R)ight, (L)eft
	// Y: (T)op, (B)ottom
	// Z: (O)ut, I(n)
	// C - origin

	RT_RB_LB_LT := SimplePoly{{+1, +1, 0}, {+1, -1, 0}, {-1, -1, 0}, {-1, +1, 0}}
	RO_LO_LI_RI := SimplePoly{{+1, 0, +1}, {-1, 0, +1}, {-1, 0, -1}, {+1, 0, -1}}
	RT_R_L_LT := SimplePoly{{+1, +1, 0}, {+1, 0, 0}, {-1, 0, 0}, {-1, +1, 0}}
	R_RB_LB_L := SimplePoly{{+1, 0, 0}, {+1, -1, 0}, {-1, -1, 0}, {-1, 0, 0}}

	RLO_RLI_I_O := SimplePoly{{+1, +1, +1}, {+1, +1, -1}, {0, 0, -1}, {0, 0, +1}}
	O_I_LTI_LTO := SimplePoly{{0, 0, +1}, {0, 0, -1}, {-1, +1, -1}, {-1, +1, +1}}

	C_LB_LT := SimplePoly{{0, 0, 0}, {-1, -1, 0}, {-1, +1, 0}}
	RT_RB_LB := SimplePoly{{+1, +1, 0}, {+1, -1, 0}, {-1, -1, 0}}
	C_LT_RT := SimplePoly{{0, 0, 0}, {-1, +1, 0}, {+1, +1, 0}}

	t.Run("Face", func(t *testing.T) {
		cases := []struct {
			F, G, X, Y Polygon
		}{
			// F crosses G
			{F: RT_RB_LB_LT, G: RO_LO_LI_RI, X: RT_R_L_LT, Y: R_RB_LB_L},

			// Flipping G swaps X and Y
			{F: RT_RB_LB_LT, G: Flip(RO_LO_LI_RI), X: R_RB_LB_L, Y: RT_R_L_LT},
		}

		for i, c := range cases {
			t.Run(fmt.Sprint(i), func(t *testing.T) {
				x, y, ok := new(intersector).cutFace(ptr(c.F), ptr(c.G))
				require.True(t, ok)
				AssertSamePolygon(t, c.X, x, "Expected correct X")
				AssertSamePolygon(t, c.Y, y, "Expected correct Y")
			})
		}
	})

	t.Run("Manifold", func(t *testing.T) {
		cases := []struct {
			F       []Polygon
			G, X, Y []Polygon
		}{
			{
				F: []Polygon{RT_RB_LB_LT},
				G: []Polygon{RLO_RLI_I_O, O_I_LTI_LTO},
				X: []Polygon{C_LB_LT, RT_RB_LB},
				Y: []Polygon{C_LT_RT},
			},
		}

		for i, c := range cases {
			t.Run(fmt.Sprint(i), func(t *testing.T) {
				a := New(c.F)
				b := New(c.G)
				v, ok := a.Bounds().Intersection(b.Bounds())
				require.True(t, ok)

				x, y := collectFaces(new(intersector).cutManifold(a, b, v), InsidePoint)
				AssertSamePolygonSet(t, c.X, x, "Expected correct X")
				AssertSamePolygonSet(t, c.Y, y, "Expected correct Y")
			})
		}
	})
}

func Collect2[A, B any](seq iter.Seq2[A, B]) []B {
	var r []B
	for _, b := range seq {
		r = append(r, b)
	}
	return r
}

func collectFaces(seq iter.Seq2[Polygon, PointLocation], keep PointLocation) (x, y []Polygon) {
	faces := map[Polygon]bool{}
	for f, typ := range seq {
		switch typ {
		case BoundaryPoint:
			delete(faces, f)
		default:
			faces[f] = typ == keep
		}
	}
	for face, keep := range faces {
		if keep {
			y = append(y, face)
		} else {
			x = append(x, face)
		}
	}
	return x, y
}

func ptr(y Polygon) Polygon {
	if y, ok := y.(SimplePoly); ok {
		return &y
	}
	return y
}

func AssertSamePolygonSet(t testing.TB, want, got []Polygon, msg string) bool {
	for i, y := range want {
		want[i] = ptr(y)
	}

	wantMap := map[Polygon]struct{}{}
	for _, y := range want {
		wantMap[y] = struct{}{}
	}

	extra := map[Polygon]struct{}{}
got:
	for _, got := range got {
		for want := range wantMap {
			if PolyEqual(want, got) {
				delete(wantMap, want)
				continue got
			}
		}

		extra[ptr(got)] = struct{}{}
	}

	if len(wantMap) == 0 && len(extra) == 0 {
		return true
	}

	t.Error(msg)
	for _, y := range want {
		if _, ok := wantMap[y]; !ok {
			fmt.Printf("   %s\n", PolySprint(y))
		}
	}
	for y := range wantMap {
		fmt.Printf("  -%s\n", PolySprint(y))
	}
	for y := range extra {
		fmt.Printf("  +%s\n", PolySprint(y))
	}
	return false
}

func AssertSamePolygon(t testing.TB, want, got Polygon, msg string) bool {
	if PolyEqual(want, got) {
		return true
	}

	t.Error(msg)
	PolyDiff(t, want, got)
	return false
}

func PolyEqual(f, g Polygon) bool {
	if f == g {
		return true
	}
	if f == nil || g == nil {
		return false
	}
	if f.Len() != g.Len() {
		return false
	}

	// A B C and C A B are the same as long as the points are the same
	var start int
	n := f.Len()
	for i := range n {
		if f.Get(0).ApproxEqual(g.Get(i)) {
			start = i
			goto compare
		}
	}
	return false

compare:
	for i := range n {
		v, u := f.Get(i), g.Get((i+start)%n)
		if !v.ApproxEqual(u) {
			return false
		}
	}
	return true
}

func PolyDiff(t testing.TB, want, got Polygon) {
	t.Logf("  want: %s", PolySprint(want))
	t.Logf("   got: %s", PolySprint(got))
}

func PolySprint(y Polygon) string {
	if y == nil {
		return "nil"
	}
	var s []string
	var t []string
	for i := range y.Len() {
		t = t[:0]
		for _, v := range y.Get(i) {
			t = append(t, fmt.Sprintf("%+.1f", v))
		}
		s = append(s, fmt.Sprintf("{%s}", strings.Join(t, ",")))
	}
	return fmt.Sprintf("(%s)", strings.Join(s, " "))
}
