// Package manifold defines manifolds and methods for constructing and
// manipulating them.
//
// These are not literal manifolds in the topological sense - they are not
// guaranteed to be closed or locally Euclidean and they are discrete not
// continuous. But I liked the word so that's what I used. Bite me.
package manifold

import (
	"fmt"
	"iter"
	"math"

	"gitlab.com/eukano/runway/internal/tree"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
	"golang.org/x/exp/constraints"
)

// New constructs a new [Manifold] from a set of faces. If two faces share a
// vertex (when rounded to [tensor.AdjacentEps]), the vertex will be recorded
// once and both faces will reference it.
func New(faces []Polygon) *Manifold {
	m := new(Manifold)
	m.Face = make([][]int, 0, len(faces))
	pos := map[tensor.Vec3]int{}

	add := func(v tensor.Vec3) int {
		v = tensor.Round3(v)
		i, ok := pos[v]
		if ok {
			return i
		}
		i = len(m.Vertex)
		pos[v] = i
		m.Vertex = append(m.Vertex, v)
		return i
	}

	for _, face := range faces {
		idx := make([]int, face.Len())
		for i := range idx {
			idx[i] = add(face.Get(i))
		}
		m.Face = append(m.Face, idx)
	}

	return m
}

// AsManifold constructs a [Manifold] from a [Polyhedron]. If the provided shape
// is already a Manifold, it will be returned unchanged.
func AsManifold(p Polyhedron) *Manifold {
	if m, ok := p.(*Manifold); ok {
		return m
	}
	return New(Faces(p))
}

// A Manifold is a set of points and faces formed of those points. A valid
// manifold represents the surface of a closed volume. The vertices are stored
// as a flat list and faces as a list of vertex indices.
type Manifold struct {
	Vertex []tensor.Vec3
	Face   [][]int

	// mpoly instances
	faces []*mpoly

	// Edge pairs
	edges map[[2]int][]int

	// Bounds
	bounds *Volume

	// Normal vectors for each face
	normals []tensor.Vec3

	// Winding number for the faces/edges surrounding each vertex, in the YZ
	// plane, X⁺ orientation
	vertexWindingYZ map[int]int

	// Winding number for the faces surrounding each edge, in the YZ plane, X⁺
	// orientation
	edgeWindingYZ map[[2]int]int

	// Bounding tree of the faces
	boundingTree *tree.BoundingTree[BoundedPolygon, int]
}

func (m *Manifold) Copy() *Manifold {
	n := new(Manifold)
	n.Vertex = make([]tensor.Vec3, len(m.Vertex))
	n.Face = make([][]int, len(m.Face))
	copy(n.Vertex, m.Vertex)
	copy(n.Face, m.Face)
	return n
}

type mpoly struct {
	m        *Manifold
	index    *int
	vertex   []int
	bounds   *Volume
	wd0, wd1 int
	myNormal *tensor.Vec3
}

func (m *mpoly) moduloVertexCount(i int) int {
	for i < 0 {
		i += len(m.vertex)
	}
	return i % len(m.vertex)
}

func (m *mpoly) Flip() Polygon {
	n := new(mpoly)
	n.m = m.m
	n.vertex = make([]int, len(m.vertex))
	for i, v := range m.vertex {
		n.vertex[len(n.vertex)-i-1] = v
	}
	return n
}

// neighbor returns the face of the manifold that shares M's I'th edge.
func (m *mpoly) neighbor(i int) *mpoly {
	if m.m.edges != nil {
		goto built
	}

	// Build the lookup
	m.m.edges = map[[2]int][]int{}
	for i, f := range m.m.Face {
		for j := range f {
			v1, v2 := sort2(f[j], f[(j+1)%len(f)])
			key := [2]int{v1, v2}
			m.m.edges[key] = append(m.m.edges[key], i)
		}
	}

built:
	v1, v2 := sort2(m.vertex[i], m.vertex[(i+1)%len(m.vertex)])
	edges := m.m.edges[[2]int{v1, v2}]
	if len(edges) != 2 {
		return nil
	}
	if edges[0] == *m.index {
		return m.m.get(edges[1])
	}
	return m.m.get(edges[0])
}

func sort2[V constraints.Ordered](a, b V) (V, V) {
	if a > b {
		return b, a
	}
	return a, b
}

var _ Polyhedron = (*Manifold)(nil)

func (m *Manifold) Len() int { return len(m.Face) }
func (p *mpoly) Len() int    { return len(p.vertex) }

func (m *Manifold) Get(i int) Polygon {
	return m.get(i)
}

func (m *Manifold) get(i int) *mpoly {
	if m.faces == nil {
		m.faces = make([]*mpoly, len(m.Face))
	}
	if m.faces[i] == nil {
		m.faces[i] = &mpoly{
			m:      m,
			index:  &i,
			vertex: m.Face[i],
		}
	}
	return m.faces[i]
}

func (m *Manifold) Faces() iter.Seq2[int, Polygon] {
	return func(yield func(int, Polygon) bool) {
		for i := range m.Face {
			if !yield(i, m.get(i)) {
				return
			}
		}
	}
}

func (p *mpoly) Get(i int) tensor.Vec3 {
	return p.m.Vertex[p.vertex[i]]
}

func (m *Manifold) Bounds() tensor.Span3 {
	if m.bounds != nil {
		return m.bounds.Bounds()
	}

	v := NanVolume()
	v.Encompass(m)
	m.bounds = &v
	return v.Bounds()
}

func (p *mpoly) Bounds() tensor.Span3 {
	if p.bounds != nil {
		return p.bounds.Bounds()
	}

	v := NanVolume()
	v.EncompassFace(p)
	p.bounds = &v
	return v.Bounds()
}

// OverlappingFaces iterates over faces of the manifold that overlap the given
// bounds.
func (m *Manifold) OverlappingFaces(b tree.Bounded) iter.Seq[Polygon] {
	return func(yield func(Polygon) bool) {
		for _, x := range m.getBoundingTree().Overlaps(b) {
			if !yield(x.Bounded) {
				return
			}
		}
	}
}

func (m *Manifold) getBoundingTree() *tree.BoundingTree[BoundedPolygon, int] {
	if m.boundingTree != nil {
		return m.boundingTree
	}

	t := tree.NewBoundingTree[BoundedPolygon, int](nil)
	for i := range m.Face {
		t.Insert(m.get(i), i)
	}

	m.boundingTree = t
	return t
}

func (m *Manifold) calcNormals() {
	if m.normals != nil {
		return
	}

	// Calculate the normal vector of each face
	m.normals = make([]tensor.Vec3, len(m.Face))
	for i := range m.Face {
		m.normals[i] = fastNormal(m.get(i))
	}
}

func fastNormal(p Polygon) tensor.Vec3 {
	if p.Len() < 3 {
		panic("invalid polygon")
	}

	if p.Len() == 3 {
		return Normal(p)
	}
	return NormalSum(p).Normalize()
}

func (m *Manifold) memoizeWinding() {
	// Ensure normals are calculated
	m.calcNormals()

	if m.vertexWindingYZ != nil {
		return
	}

	// For each vertex V of each face F, determine the neighboring vertex U that
	// comes before V, and the vertex W that comes after V in the winding order
	// of F, then associate V → (U → W)...
	vertexEdges := map[int]map[int]int{}
	// and associate V → (U → F)
	faceEdges := map[int]map[int]int{}
	for f := range m.Face {
		n := len(m.Face[f])
		for i, v := range m.Face[f] {
			u := m.Face[f][(i-1+n)%n]
			w := m.Face[f][(i+1)%n]

			if e, ok := vertexEdges[v]; ok {
				e[u] = w
			} else {
				vertexEdges[v] = map[int]int{u: w}
			}

			if e, ok := faceEdges[v]; ok {
				e[u] = f
			} else {
				faceEdges[v] = map[int]int{u: f}
			}
		}
	}

	// For mapping E (U → W) of each vertex V, follow the associations of E to
	// construct a loop L and determine the X⁺ winding direction of L about V
	m.vertexWindingYZ = map[int]int{}
	for v, e := range vertexEdges {
		// Start the loop with some U of E
		l := make([]int, 0, len(e))
		for a, b := range e {
			l = append(l, a, b)
			break
		}
		delete(e, l[0])

		// Construct L from U → W and verify that L is a complete loop and
		// consumes all associations from E
		for len(e) > 0 {
			a := l[len(l)-1]
			b, ok := e[a]
			if !ok {
				// Incomplete loop
				l = nil
				break
			}

			delete(e, a)
			switch {
			case len(e) > 0:
				l = append(l, b)
			case b != l[0]:
				// Incomplete loop
				l = nil
			}
		}
		if len(e) > 0 {
			l = nil
		}

		// Calculate and cache the X⁺ winding number of L about V
		if l != nil {
			w := Winding(&mpoly{m: m, vertex: l}, m.Vertex[v], 1, 2)
			m.vertexWindingYZ[v] = w
		}
	}

	// For each association V → U → F, load association U → V → G and determine
	// the X⁺ winding direction of the edge VU between F and G
	m.edgeWindingYZ = map[[2]int]int{}
	for v, e := range faceEdges {
		for u, f := range e {
			g := faceEdges[u][v]

			// Ensure V < U
			v := v
			if v > u {
				v, u = u, v
			}

			vv, uu := m.Vertex[v], m.Vertex[u]
			_, _ = vv, uu

			// Only calculate once
			if _, ok := m.edgeWindingYZ[[2]int{v, u}]; ok {
				continue
			}

			fn, gn := m.normals[f], m.normals[g]
			switch {
			case fn[0] > 0 && gn[0] > 0:
				// If the X component of both normals are positive, V → X⁺
				// enters through the edge
				m.edgeWindingYZ[[2]int{v, u}] = -1

			case fn[0] < 0 && gn[0] < 0:
				// If the X component of both normals are negative, V → X⁺
				// exits through the edge
				m.edgeWindingYZ[[2]int{v, u}] = +1

			default:
				// If the X component of the normals are different signs, V → X⁺
				// skims (does not cross) the edge
				m.edgeWindingYZ[[2]int{v, u}] = 0
			}
		}
	}
}

func (m *mpoly) onPerim(p tensor.Vec3) bool {
	_, ok := m.onPerim2(p)
	return ok
}

func (m *mpoly) onPerim2(p tensor.Vec3) (int, bool) {
	m.memoizeDims()
	return LiesOnPerimeter(m, p, m.wd0, m.wd1)
}

func (m *mpoly) winding(p tensor.Vec3) int {
	m.memoizeDims()
	return Winding(m, p, m.wd0, m.wd1)
}

func (m *mpoly) distance(p tensor.Vec3) float64 {
	m.m.calcNormals()

	q, n := m.Get(0), m.normal()
	return tensor.DistancePlane(p, q, n)
}

func (m *mpoly) memoizeDims() {
	if m.wd0 != 0 && m.wd1 != 0 {
		return
	}

	// Calculate the axes to use for winding
	m.wd0, m.wd1, _ = Pointing(m.normal())
}

func (m *mpoly) normal() tensor.Vec3 {
	if m.index != nil {
		m.m.calcNormals()
		return m.m.normals[*m.index]
	}

	if m.myNormal == nil {
		n := fastNormal(m)
		m.myNormal = &n
	}
	return *m.myNormal
}

// PointLocation is the location of a point relative to an object
type PointLocation int

const (
	// BoundaryPoint indicates the point is on the bounds of the object, e.g.
	// the surface of a volume or perimeter of a polygon.
	BoundaryPoint PointLocation = 0

	// InsidePoint indicates the point is strictly inside the object.
	InsidePoint PointLocation = -1

	// Outside point indicates the point is strictly outside the object.
	OutsidePoint PointLocation = +1
)

func (l PointLocation) String() string {
	switch l {
	case BoundaryPoint:
		return "Boundary"
	case InsidePoint:
		return "Inside"
	case OutsidePoint:
		return "Outside"
	}
	return fmt.Sprint(int(l))
}

// Winding calculates the winding number W of M about P. W will be zero or
// positive (will not be negative) if M is closed.
//
// Winding assumes M is a sane, closed manifold; its behavior is undefined
// otherwise. This includes but is not limited to:
//   - Multiple faces that share a vertex but face different directions;
//   - An edge shared by N≠2 faces;
//   - An edge shared by two faces that face in opposite directions relative to the edge;
//   - Other discontinuities.
func (m *Manifold) Winding(p tensor.Vec3) PointLocation {
	// If the point is outside of the manifold's bounding volume, assume the
	// winding number is zero
	if !m.Bounds().ContainsPoint(p) {
		return OutsidePoint
	}

	// Memoize winding info
	m.memoizeWinding()

	vi := map[int]bool{}
	fHasVI := func(f int) bool {
		for _, v := range m.Face[f] {
			if vi[v] {
				return true
			}
		}
		return false
	}

	// Does P → X⁺ intersect a vertex
	p2 := projYZ(p)
	var w int
	for i, v := range m.Vertex {
		// If P is a vertex, then consider it to be inside (w=1)
		if p.ApproxEqual(v) {
			return BoundaryPoint
		}

		switch {
		case p[0] < v[0]:
			continue // Ignore vertices to the left of P
		case projYZ(v).ApproxEqual(p2):
			vi[i] = true
		default:
			continue
		}

		u, ok := m.vertexWindingYZ[i]
		switch {
		case !ok:
			panic("manifold has a discontinuity")
		case u > 0:
			w++
		case u < 0:
			w--
		}
	}

	hitEdge := map[[2]int]int{}
face:
	for f := range m.Face {
		// If P → X⁺ hits a vertex of F (the face), skip
		if fHasVI(f) {
			continue
		}

		// Intersection J of P → X⁺ with F
		j, ok := m.projectOntoFace(f, p)
		if !ok {
			continue
		}

		// If J is P, check if P is within F and thus is on the boundary
		if p.ApproxEqual(j) {
			f := m.get(f)
			d0, d1, _ := Pointing(f.normal())
			if Winding(f, p, d0, d1) != 0 {
				return BoundaryPoint
			}
		}

		// Check edges
		for i, a := range m.Face[f] {
			// B - other vertex of the edge
			b := m.Face[f][(i+1)%len(m.Face[f])]

			// Ensure A < B
			if a > b {
				a, b = b, a
			}

			// Skip the face if the edge has already been hit
			if _, ok := hitEdge[[2]int{a, b}]; ok {
				continue face
			}

			// Is J colinear with AB?
			av, bv := m.Vertex[a], m.Vertex[b]
			if !tensor.AreColinear(av, bv, j) {
				continue
			}

			// If P is J then P is on the boundary
			if p.ApproxEqual(j) {
				return BoundaryPoint
			}

			// Is J within AB?
			ab := bv.Sub(av)
			aj := j.Sub(av)
			if ab.Dot(aj) < 0 ||
				aj.LenSqr()/ab.LenSqr() > 1 {
				continue
			}

			u, ok := m.edgeWindingYZ[[2]int{a, b}]
			hitEdge[[2]int{a, b}] = u
			switch {
			case !ok:
				println("manifold has a discontinuity")
				return OutsidePoint
			case u > 0:
				w++
			case u < 0:
				w--
			}

			// Continue to the next face
			continue face
		}

		// Calculate the winding number of F around J
		u := Winding(m.Get(f), j, 2, 1)

		switch {
		case u > 0:
			w++
		case u < 0:
			w--
		}
	}

	if w > 0 {
		return InsidePoint
	}
	return OutsidePoint
}

func projYZ(v tensor.Vec3) tensor.Vec2 {
	return tensor.Vec2(v[1:])
}

// projectOntoFace projects P along X⁺ onto F.
func (m *Manifold) projectOntoFace(f int, p tensor.Vec3) (tensor.Vec3, bool) {
	q := m.Vertex[m.Face[f][0]] // Q - a point on the face
	n := m.normals[f]           // N - a vector normal to the face

	// Normal is perpendicular to X, so...
	if math.Abs(n[0]) < tensor.AdjacentEps {
		// Save the original P as J
		j := p

		// Project P, Q, and N onto the X = 0 plane
		p := tensor.Vec2{p[1], p[2]}
		q := tensor.Vec2{q[1], q[2]}
		n := tensor.Vec2{n[1], n[2]}

		if math.Abs(p.Sub(q).Dot(n)) < tensor.AdjacentEps {
			// P lies within Y, so no projection is necessary
			return j, true
		}

		// P → X⁺ does not intersect Y
		return tensor.Vec3{}, false
	}

	// Calculate the intersection J of P → X⁺ with the plane of Y. This can
	// be much simpler than the general case of the intersection of a line
	// and a plane since the line is parallel to the X axis.
	fac := -p.Sub(q).Dot(n) / n[0]
	if tensor.LessThanEps(fac) {
		return p, true
	}

	j := p.Add(tensor.Vec3{fac, 0, 0})

	// If J (and thus the face) is to the left of P, skip it.
	if fac > 0 {
		return tensor.Vec3{}, false
	}

	return j, true
}
