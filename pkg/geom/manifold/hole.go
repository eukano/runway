package manifold

import (
	"log"
	"sort"

	"gitlab.com/eukano/runway/internal/debug"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

var debugAddHole struct {
	Faces, Steps debug.Logger
}

// AddHoles constructs a simple polygon of the face with holes cut out of it.
//
// https://www.geometrictools.com/Documentation/TriangulationByEarClipping.pdf
func AddHoles(face Polygon, holes ...Polygon) Polygon {
	// Calculate the normal vector to the face, then figure out which dimension
	// of the normal is largest and ignore that dimension. For example, if the
	// normal is largest in Y, ignore Y - this effectively projects everything
	// onto the XZ plane, which greatly simplifies the algorithm. We will call
	// the two other dimensions - the ones not ignored - D₀ and D₁.
	//
	// For the sake of simplicity, we will act as though D₀ is X and thus
	// 'right' and 'left' mean positive and negative D₀, respectively.
	normal := NormalSum(face)
	d0, d1, _ := Pointing(normal)

	debugAddHole.Faces.Logfn(func(l *log.Logger) {
		l.Printf("face vertices\n---\n")
		for _, v := range Vertices(face) {
			l.Printf("{%+.9f,%+.9f,%+.9f},\n", v[0], v[1], v[2])
		}
		for i, h := range holes {
			l.Printf("\n---\nhole %d vertices\n---\n", i)
			for _, v := range Vertices(h) {
				l.Printf("{%+.9f,%+.9f,%+.9f},\n", v[0], v[1], v[2])
			}
		}
		l.Println()
	})

	// For each hole, record the point furthest to the right - the maximum D₀
	// value of the hole
	max := make([]struct {
		hole  Polygon
		value float64
	}, len(holes))
	for i, f := range holes {
		max[i].hole = f
		for j, v := range Vertices(f) {
			if j == 0 || v[d0] > max[i].value {
				max[i].value = v[d0]
			}
		}
	}

	// Add each hole to the face, starting with the hole that extends furthest
	// to the right, following with the hole that extends second furthest to the
	// right, etc.
	sort.Slice(max, func(i, j int) bool { return max[i].value > max[j].value })
	for _, x := range max {
		face = addHole(face, x.hole, normal, d0, d1)
	}
	return face
}

func addHole(face, hole Polygon, normal tensor.Vec3, d0, d1 int) Polygon {
	// Find the rightmost vertex U of the hole
	var u tensor.Vec3
	var ui int
	for i, x := range Vertices(hole) {
		if i == 0 || x[d0] > u[d0] {
			u, ui = x, i
		}
	}

	// Find the vertex V of the face such that,
	// - Given the line L from V to V+1,
	// - And the line M extending from U to the right,
	// - If L crosses M, and U is to the left of L,
	// - Where T is the intersection of M and L,
	// - T is the leftmost (closest to U) of all such vertices.
	var t, v tensor.Vec3 // intersection, V
	var vi int = -1      // index of V
	var reflex []int
	for j, n := 0, face.Len(); j < n; j++ {
		i, k := (n+j-1)%n, (j+1)%n
		a, b := face.Get(j), face.Get(k)

		// Track reflex verticies for later
		if tensor.Area(face.Get(i), a, b).Dot(normal) < 0 {
			reflex = append(reflex, j)
		}

		// Is V-V+1 to the right of U?
		w := IsRightOf(a, b, u, d0, d1)
		if w == 0 {
			continue
		}

		// Find T, the intersection of M and L, by finding the point on L with a
		// D₁ coordinate equal to that of U
		q := (u[d1] - a[d1]) / (b[d1] - a[d1])
		x := b.Sub(a).Mul(q).Add(a)

		// If we already have something more leftward, next
		if vi >= 0 && x[d0] >= t[d0] {
			continue
		}

		t = x
		if a[d0] > b[d0] {
			v, vi = a, j
		} else {
			v, vi = b, k
		}
	}

	// Remove reflex vertices that are not within UTV
	for i := len(reflex) - 1; i >= 0; i-- {
		// Equivalent to geomath.Contains, but more relaxed
		a, b, _ := tensor.ProjectVec(face.Get(reflex[i]), u, t, v).Elem()
		if a < 0 && b < 0 && a+b >= 1 {
			continue
		}

		reflex = append(reflex[:i], reflex[i+1:]...)
	}

	// If any reflex verticies are within UTV (unless the intersection point is
	// a vertex)
	if t.Sub(v).Len() > tensor.AdjacentEps && len(reflex) > 0 {
		// Order the reflex vertices R by the angle between UT and UR
		ut := t.Sub(u)
		angle := func(i int) float64 {
			ur := face.Get(i).Sub(u)
			return ut.Dot(ur) / ut.Len() / ur.Len()
		}

		sort.Slice(reflex, func(i, j int) bool {
			a, b := angle(i), angle(j)
			return a < b
		})

		// And pick the one with the smallest angle
		vi = reflex[0]
	}

	// Assemble the result
	g := make(SimplePoly, face.Len()+hole.Len()+2)
	fv, hv := Vertices(face), Vertices(hole)
	n := copy(g[0:], fv[:vi+1])
	n += copy(g[n:], hv[ui:])
	n += copy(g[n:], hv[:ui+1])
	n += copy(g[n:], fv[vi:])
	debugAddHole.Steps.Logf("+hole - face[:%d] (%d), hole[%d:] (%d), hole[:%d] (%d), face[%d:] (%d)\n", vi+1, vi+1, ui, len(hv)-ui, ui+1, ui+1, vi, len(fv)-vi)
	return g
}
