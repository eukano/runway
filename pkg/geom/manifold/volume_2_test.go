package manifold_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/shape"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

func TestVolume_Cut(t *testing.T) {
	t.Run("Octahedron", func(t *testing.T) {
		oct := shape.Octahedron(-10, +10, -10, +10, -10, +10)

		cases := map[string]struct {
			min, max tensor.Vec3
			skip     string
		}{
			"Under": {min: tensor.Vec3{-4, -4, -4}, max: tensor.Vec3{+4, +4, +4}},
			"Equal": {min: tensor.Vec3{-5, -5, -5}, max: tensor.Vec3{+5, +5, +5}},
			"Over":  {min: tensor.Vec3{-6, -6, -6}, max: tensor.Vec3{+6, +6, +6}},
		}

		for k, tc := range cases {
			t.Run(k, func(t *testing.T) {
				if tc.skip != "" {
					t.Skip(tc.skip)
				}

				v := manifold.Volume{Min: tc.min, Max: tc.max}
				c := v.Cut(oct).(*manifold.Manifold)
				assert.Len(t, c.Face, 8+6)
			})
		}
	})

	t.Run("Sphere", func(t *testing.T) {
		sphere := shape.Sphere(10, tensor.Vec3{})
		octantFaces := sphere.Len() / 8

		cases := map[string]struct {
			max  tensor.Vec3
			full bool
			skip string
		}{
			"Larger":  {max: tensor.Vec3{12, 12, 12}, full: true},
			"Same":    {max: tensor.Vec3{10, 10, 10}, full: true},
			"Smaller": {max: tensor.Vec3{8, 8, 8}},
		}

		for name, tc := range cases {
			t.Run(name, func(t *testing.T) {
				if tc.skip != "" {
					t.Skip(tc.skip)
				}

				v := manifold.Volume{Min: tensor.Vec3{}, Max: tc.max}
				c := v.Cut(sphere).(*manifold.Manifold)

				if !tc.full {
					return
				}

				// The result should have somewhat more faces than an 8th of the
				// original, but not too much more
				require.Greater(t, len(c.Face), octantFaces, "Expected at least %d/8 faces", sphere.Len())
				require.Less(t, len(c.Face), octantFaces*9/8, "Expected at most 1.125 * %d/8 faces", sphere.Len())

				for i := c.Len() - 3; i < c.Len(); i++ {
					f := c.Get(i)

					// Expect the last 3 faces to be perimeters, with many points
					assert.Greater(t, f.Len(), 10, "Expected at least 10 faces in a perimeter facet")

					// Expect the last 3 faces to include the origin
					assert.Contains(t, manifold.Vertices(f), tensor.Vec3{}, "Expected facet to contain the origin")

					// Each should also include 2 other corners, but that's harder to
					// test for
				}
			})
		}
	})
}
