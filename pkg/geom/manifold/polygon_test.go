package manifold_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	. "gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/shape"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// circularly shifts the vertices of F by I
func rotate(f Polygon, i int) Polygon {
	g := make(SimplePoly, f.Len())
	v := Vertices(f)
	n := copy(g, v[i:])
	copy(g[n:], v[:i])
	return g
}

func BenchmarkIsConvex(b *testing.B) {
	for _, N := range []int{6, 12, 30, 60, 300, 600} {
		f := shape.Circle(N, 1, tensor.Vec3{}, tensor.Vec3{})
		b.Run(fmt.Sprintf("%d", N), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				IsConvex(f)
			}
		})
	}
}

func TestIsConvex(t *testing.T) {
	cases := []struct {
		desc     string
		face     Polygon
		expected bool
	}{
		{"square", SimplePoly{
			{0, 0},
			{0, 1},
			{1, 1},
			{1, 0},
		}, true},

		{"circle 1", shape.Circle(6, 1, tensor.Vec3{}, tensor.Vec3{1}), true},
		{"circle 2", shape.Circle(6, 1, tensor.Vec3{}, tensor.Vec3{0, 1}), true},
		{"circle 3", shape.Circle(6, 1, tensor.Vec3{}, tensor.Vec3{0, 0, 1}), true},
		{"circle 4", shape.Circle(6, 1, tensor.Vec3{}, tensor.Vec3{1, 2, 3}), true},
		{"reversed circle", Flip(shape.Circle(6, 1, tensor.Vec3{}, tensor.Vec3{1})), true},

		{"poly", SimplePoly{
			{0},
			{0, 2},
			{1, 1},
			{2, 2},
			{2, 0},
		}, false},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			for i := 0; i < c.face.Len(); i++ {
				t.Run(fmt.Sprintf("rot-%d", i), func(t *testing.T) {
					if IsConvex(rotate(c.face, i)) != c.expected {
						if c.expected {
							t.Fatal("expected convex, got concave")
						} else {
							t.Fatal("expected concave, got convex")
						}
					}
				})
			}
		})
	}
}

func TestWinding(t *testing.T) {
	c := shape.Circle(6, 1, tensor.Vec3{2, 0, 0}, tensor.Vec3{0, 0, 1})
	rc := Flip(c)
	_ = rc

	x := SimplePoly{
		{+1, +2},
		{+2, 0},
		{+1, -1},
		{-0.5, 0.5},
		{+0.5, 0.5},
		{-1, -1},
		{-2, 0},
		{-1, +2},
	}
	_ = x

	cases := []struct {
		face     Polygon
		point    tensor.Vec3
		expected int
	}{
		// {c, Vec{}, 0},
		// {c, Vec{2}, 1},
		// {c, Vec{4}, 0},
		// {rc, Vec{}, 0},
		// {rc, Vec{2}, -1},
		// {rc, Vec{4}, 0},

		// {x, Vec{0, 0.25}, 2},
		// {x, Vec{0, 1}, 1},

		{SimplePoly{{0, 0}, {-2, -2}, {-2, +2}}, tensor.Vec3{0, 0}, +1},
		{SimplePoly{{0, 0}, {-2, +2}, {-2, -2}}, tensor.Vec3{0, 0}, -1},
		{SimplePoly{{0, 0}, {+2, +2}, {+2, -2}}, tensor.Vec3{0, 0}, +1},
		{SimplePoly{{0, 0}, {+2, -2}, {+2, +2}}, tensor.Vec3{0, 0}, -1},
	}

	for i, c := range cases {
		t.Run(fmt.Sprint(i), func(t *testing.T) {
			w := Winding(c.face, c.point, 0, 1)
			if w != c.expected {
				t.Fatalf("expected %v, got %v", c.expected, w)
			}
		})
	}
}

func TestIntersectsPolygon(t *testing.T) {
	cases := []struct {
		A, B   Polygon
		expect bool
	}{
		{B: SimplePoly{{0, 0}, {3, 0}, {3, 3}, {0, 3}}, A: SimplePoly{{1, 1, 1}, {1, 1, -1}, {-1, 1, -1}, {-1, 1, 1}}, expect: true},    // interlocked
		{A: SimplePoly{{0, 0}, {3, 0}, {3, 3}, {0, 3}}, B: SimplePoly{{1, 1, 1}, {1, 1, -1}, {-1, 1, -1}, {-1, 1, 1}}, expect: true},    // interlocked
		{B: SimplePoly{{0, 0}, {3, 0}, {3, 3}, {0, 3}}, A: SimplePoly{{1, 1, 1}, {1, 1, -1}, {3, 1, -1}, {3, 1, 1}}, expect: true},      // contained
		{B: SimplePoly{{0, 0}, {3, 0}, {3, 3}, {0, 3}}, A: SimplePoly{{-2, 1, 1}, {-2, 1, -1}, {-1, 1, -1}, {-1, 1, 1}}, expect: false}, // non-intersecting
		{A: SimplePoly{{0, 0}, {3, 0}, {3, 3}, {0, 3}}, B: SimplePoly{{-2, 1, 1}, {-2, 1, -1}, {-1, 1, -1}, {-1, 1, 1}}, expect: false}, // non-intersecting
		{B: SimplePoly{{0, 0}, {3, 0}, {3, 3}, {0, 3}}, A: SimplePoly{{1, 1, -1}, {1, 1, 1}, {1, 2, 1}, {1, 2, -1}}, expect: true},      // intersecting
		{A: SimplePoly{{0, 0}, {3, 0}, {3, 3}, {0, 3}}, B: SimplePoly{{1, 1, -1}, {1, 1, 1}, {1, 2, 1}, {1, 2, -1}}, expect: true},      // intersecting
		{B: SimplePoly{{0, 0}, {3, 0}, {3, 3}, {0, 3}}, A: SimplePoly{{1, 1}, {2, 1}, {2, 2}, {1, 2}}, expect: true},                    // coplanar, intersecting
		{A: SimplePoly{{0, 0}, {3, 0}, {3, 3}, {0, 3}}, B: SimplePoly{{1, 1}, {2, 1}, {2, 2}, {1, 2}}, expect: true},                    // coplanar, intersecting
		{B: SimplePoly{{0, 0}, {3, 0}, {3, 3}, {0, 3}}, A: SimplePoly{{4, 1}, {5, 1}, {5, 2}, {4, 2}}, expect: false},                   // coplanar, non-intersecting
		{A: SimplePoly{{0, 0}, {3, 0}, {3, 3}, {0, 3}}, B: SimplePoly{{4, 1}, {5, 1}, {5, 2}, {4, 2}}, expect: false},                   // coplanar, non-intersecting
	}

	for _, c := range cases {
		t.Run("", func(t *testing.T) {
			v := IntersectsPolygon(c.A, c.B, Normal(c.A), Normal(c.B))
			if c.expect {
				require.True(t, v)
			} else {
				require.False(t, v)
			}
		})
	}
}
