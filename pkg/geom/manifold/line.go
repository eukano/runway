package manifold

import "gitlab.com/eukano/runway/pkg/geom/tensor"

// Line is a line segment.
type Line [2]tensor.Vec3

// Len returns 2.
func (v *Line) Len() int { return len(v) }

// Get returns the Ith vertex.
func (v *Line) Get(i int) tensor.Vec3 { return v[i] }
