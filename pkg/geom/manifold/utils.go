package manifold

import (
	"iter"
	"slices"

	"golang.org/x/exp/constraints"
)

// mapKeys returns a slice of the map's keys.
func mapKeys[K comparable, V any](m map[K]V) []K {
	kk := make([]K, 0, len(m))
	for k := range m {
		kk = append(kk, k)
	}
	return kk
}

// mapKeys returns a sorted slice of the map's keys.
func mapKeysSorted[K constraints.Ordered, V any](m map[K]V) []K {
	k := mapKeys(m)
	slices.Sort(k)
	return k
}

// mapContains returns true if the predicate returns true for any entry.
func mapContains[K comparable, V any](m map[K]V, predicate func(K, V) bool) bool {
	for k, v := range m {
		if predicate(k, v) {
			return true
		}
	}
	return false
}

func mapKeySeq[K comparable, V any](m map[K]V) iter.Seq[K] {
	return func(yield func(K) bool) {
		for k := range m {
			if !yield(k) {
				return
			}
		}
	}
}

// remap takes a K₀ → K₁ → V map and returns its entries remapped as K₁ → K₀ →
// V.
func remap[K comparable, V any, M ~map[K]map[K]V](m M) M {
	n := M{}
	for k0, mm := range m {
		for k1, v := range mm {
			nn, ok := n[k1]
			if !ok {
				nn = map[K]V{}
				n[k1] = nn
			}
			nn[k0] = v
		}
	}
	return n
}

func cmpNilInt[V constraints.Signed](a, b *V) V {
	switch {
	case a == b:
		return 0
	case a == nil:
		return -1
	case b == nil:
		return +1
	default:
		return *a - *b
	}
}

func sliceFind[S ~[]E, E any](s S, f func(E) bool) (E, bool) {
	for _, e := range s {
		if f(e) {
			return e, true
		}
	}
	var z E
	return z, false
}

func sliceFilter[S ~[]E, E any](s S, f func(E) bool) S {
	var t S
	for _, e := range s {
		if f(e) {
			t = append(t, e)
		}
	}
	return t
}

func aOrB[V any](x bool, a, b V) V {
	if x {
		return a
	}
	return b
}
