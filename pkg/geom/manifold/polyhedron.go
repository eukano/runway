package manifold

import "iter"

type Polyhedron interface {
	Len() int
	Get(int) Polygon
	Faces() iter.Seq2[int, Polygon]
}

func Faces(p Polyhedron) []Polygon {
	v := make([]Polygon, p.Len())
	for i := range v {
		v[i] = p.Get(i)
	}
	return v
}
