package manifold_test

import (
	"testing"

	. "gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/shape"
)

func TestShapes(t *testing.T) {
	t.Run("0", func(t *testing.T) {
		a := shape.Octahedron(
			-1, -1, -1,
			+1, +1, +1)
		b := shape.Cube(
			-2, -2, -2,
			-0, +2, +2)

		c := Subtract(a, b)

		want := []Polygon{
			SimplePoly{{+0.0, +1.0, +0.0}, {+1.0, +0.0, +0.0}, {+0.0, +0.0, +1.0}},
			SimplePoly{{+0.0, +1.0, +0.0}, {+0.0, +0.0, -1.0}, {+1.0, +0.0, +0.0}},
			SimplePoly{{+0.0, -1.0, +0.0}, {+0.0, +0.0, +1.0}, {+1.0, +0.0, +0.0}},
			SimplePoly{{+0.0, -1.0, +0.0}, {+1.0, +0.0, +0.0}, {+0.0, +0.0, -1.0}},
			SimplePoly{{+0.0, +1.0, +0.0}, {+0.0, +0.0, +1.0}, {+0.0, -1.0, +0.0}, {+0.0, +0.0, -1.0}},
		}
		AssertSamePolygonSet(t, want, Collect2(c.Faces()), "Expected correct X")
	})

	t.Run("1", func(t *testing.T) {
		a := shape.Octahedron(
			-1, -1, -1,
			+1, +1, +1)
		b := shape.Cube(
			-2, -2, -2,
			-0.1, +2, +2)

		c := Subtract(a, b)

		want := []Polygon{
			SimplePoly{{+0.0, +1.0, +0.0}, {+1.0, +0.0, +0.0}, {+0.0, +0.0, +1.0}},
			SimplePoly{{+0.0, +1.0, +0.0}, {+0.0, +0.0, -1.0}, {+1.0, +0.0, +0.0}},
			SimplePoly{{+0.0, -1.0, +0.0}, {+0.0, +0.0, +1.0}, {+1.0, +0.0, +0.0}},
			SimplePoly{{+0.0, -1.0, +0.0}, {+1.0, +0.0, +0.0}, {+0.0, +0.0, -1.0}},

			SimplePoly{{+0.0, -1.0, +0.0}, {+0.0, +0.0, -1.0}, {-0.1, -0.0, -0.9}, {-0.1, -0.9, +0.0}},
			SimplePoly{{+0.0, +1.0, +0.0}, {+0.0, +0.0, +1.0}, {-0.1, +0.0, +0.9}, {-0.1, +0.9, +0.0}},
			SimplePoly{{+0.0, -1.0, +0.0}, {-0.1, -0.9, +0.0}, {-0.1, +0.0, +0.9}, {+0.0, +0.0, +1.0}},
			SimplePoly{{+0.0, +1.0, +0.0}, {-0.1, +0.9, +0.0}, {-0.1, -0.0, -0.9}, {+0.0, +0.0, -1.0}},

			SimplePoly{{-0.1, -0.9, +0.0}, {-0.1, -0.0, -0.9}, {-0.1, +0.9, +0.0}, {-0.1, +0.0, +0.9}},
		}
		AssertSamePolygonSet(t, want, Collect2(c.Faces()), "Expected correct X")
	})
}
