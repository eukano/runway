package manifold

import (
	"math"

	. "gitlab.com/eukano/runway/pkg/geom/tensor"
)

// Polygon is a set of vertices representing a polygon.
type Polygon interface {
	Len() int
	Get(int) Vec3
}

// BoundedPolygon is a [Polygon] that can provide its bounds.
type BoundedPolygon interface {
	Polygon
	Bounds() Span3
}

// SimplePoly is an ordered set of vertices that describe a polygon. A valid
// polygon has at least three points all of which are coplanar and describe a
// non-self intersecting loop.
type SimplePoly []Vec3

func (p SimplePoly) Len() int       { return len(p) }
func (p SimplePoly) Get(i int) Vec3 { return p[i] }

// Vertices returns the vertices of a [Polygon]. Vertices will always return a
// new slice whether or not the operand is itself a slice.
func Vertices(p Polygon) []Vec3 {
	v := make([]Vec3, p.Len())
	for i := range v {
		v[i] = p.Get(i)
	}
	return v
}

// Flip returns a face with the vertices in reverse order.
func Flip(f Polygon) Polygon {
	if f, ok := f.(interface{ Flip() Polygon }); ok {
		return f.Flip()
	}

	n := f.Len()
	g := make(SimplePoly, n)
	for i := 0; i < n; i++ {
		g[i] = f.Get(n - i - 1)
	}
	return g
}

// Normal returns a vector normal to the first three verticies of a face as V₁V₀
// ✕ V₁V₂, normalized. If the face is concave, this may be the inverse of the
// proper normal.
func Normal(f Polygon) Vec3 {
	if f.Len() < 3 {
		return Vec3{}
	}

	return Area(f.Get(0), f.Get(1), f.Get(2)).Normalize()
}

// NormalSum returns the summation of VₙVₙ₋₁ ✕ VₙVₙ₊₁ for every vertex of a
// face. It doesn't actually normalize anything.
func NormalSum(f Polygon) Vec3 {
	var normal Vec3
	for i, n := 0, f.Len(); i < n; i++ {
		a, b, c := f.Get(i), f.Get((i+1)%n), f.Get((i+2)%n)
		normal = Area(a, b, c).Add(normal)
	}
	return normal
}

// Center returns the average of all the vertices of a face.
func Center(f Polygon) Vec3 {
	var c Vec3
	for i, n := 0, f.Len(); i < n; i++ {
		c = c.Add(f.Get(i))
	}
	return c.Mul(1 / float64(f.Len()))
}

// IsConvex returns whether a face is convex.
//
// To determine if a face is convex, take the first non-trivial Xₙ where Xₙ =
// VₙVₙ₋₁ ✕ VₙVₙ₊₁; then calculate X for every other vertex. If Xₙ∙Xₘ < 0 for
// any n, m where n ≠ m, the face is concave.
//
// Behavior is undefined if all verticies are co-linear, or if all verticies are
// not co-planar.
func IsConvex(f Polygon) bool {
	// Triangles cannot be concave
	if f.Len() <= 3 {
		return true
	}

	var plane Vec3
	for i, n := 0, f.Len(); i < n; i++ {
		x := Area(f.Get(i), f.Get((i+1)%n), f.Get((i+2)%n))

		// Colinear, ignore
		if x.LenSqr() < AdjacentEps {
			continue
		}

		// No normal yet, continue
		if plane == (Vec3{}) {
			plane = x
			continue
		}

		// The magnitude of the vectors won't change the sign of the dot
		// product, so there's no need to normalize
		if plane.Dot(x) < 0 {
			return false
		}
	}

	return true
}

// PolygonIncludesPoint checks whether V is one of the points of Y.
func PolygonIncludesPoint(y Polygon, v Vec3) bool {
	for i, n := 0, y.Len(); i < n; i++ {
		if y.Get(i) == v {
			return true
		}
	}
	return false
}

// Pointing determines the largest dimensional component of V - D₂ is the number
// of that dimension and D₀ and D₁ are the other two dimensions. This is useful
// for roughly projecting vectors to 2 dimensions.
func Pointing(v Vec3) (d0, d1, d2 int) {
	nx, ny, nz := v.Elem()
	nx, ny, nz = math.Abs(nx), math.Abs(ny), math.Abs(nz)
	if nx > ny && nx > nz {
		return 1, 2, 0
	} else if ny > nx && ny > nz {
		return 0, 2, 1
	} else {
		return 0, 1, 2
	}
}

// IsRightOf returns +1 if Q₀Q₁ is decending and to the right of P, -1 if Q₀Q₁
// is ascending and to the right of P, and 0 otherwise. D₀ and D₁ determine what
// dimensions are used - the third dimension is ignored. This assumes P, Q₀, and
// Q₁ are coplanar.
func IsRightOf(q0, q1, p Vec3, d0, d1 int) int {
	aa := Vec3{q0[d0], q0[d1]}
	bb := Vec3{q1[d0], q1[d1]}
	pp := Vec3{p[d0], p[d1]}

	if aa[1] > pp[1] {
		if pp[1] >= bb[1] {
			if aa[0] < pp[0] && pp == bb {
				// AB is decending, A is left of P, and P equals B
				return +1
			}
			if aa.Sub(bb).Cross(pp.Sub(bb)).Z() > 0 {
				// AB is decending and right of P
				return +1
			}
		}
	} else { // aa[1] <= pp[1]
		if pp[1] < bb[1] {
			if aa == pp && pp[0] > bb[0] {
				// AB is ascending, B is left of P, and P equals A
				return -1
			}
			if bb.Sub(aa).Cross(pp.Sub(aa)).Z() > 0 {
				// AB is ascending and right of P
				return -1
			}
		}
	}

	return 0
}

// Winding calculates the winding number of Y about P. D₀ and D₁ determine what
// dimensions are used - the third dimension is ignored. This assumes P and Y
// are coplanar.
func Winding(y Polygon, p Vec3, d0, d1 int) int {
	w := 0
	N := y.Len()
	a := y.Get(0)
	for i := 0; i < N; i++ {
		b := y.Get((i + 1) % N)
		u := IsRightOf(a, b, p, d0, d1)
		w += u
		a = b
	}

	return w
}

// LinesOnPerimeter returns true if P lines on the perimeter of Y. D₀ and D₁
// determine what dimensions are used - the third dimension is ignored. This
// assumes P and Y are coplanar.
//
// Based on https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
func LiesOnPerimeter(y Polygon, p Vec3, d0, d1 int) (int, bool) {
	x0, y0 := p[d0], p[d1]
	for i, n := 0, y.Len(); i < n; i++ {
		v, u := y.Get(i), y.Get((i+1)%n)
		x1, y1 := v[d0], v[d1]
		x2, y2 := u[d0], u[d1]
		if LessThanEps((x2-x1)*(y1-y0) - (x1-x0)*(y2-y1)) {
			return i, true
		}
	}
	return 0, false
}

// LiesWithin returns true if P lies within Y (and not on the perimeter)
func LiesWithin(y Polygon, p Vec3, d0, d1 int) bool {
	if Winding(y, p, d0, d1) == 0 {
		return false
	}
	_, ok := LiesOnPerimeter(y, p, d0, d1)
	return !ok
}

// IntersectsPlane returns 0 if Y intersects the plane described by point Q and
// normal N, +1 if Y is in front of the plane (same side as the normal), or -1
// if Y is behind the plane (opposite side from the normal).
func IntersectsPlane(y Polygon, q, n Vec3) int {
	var front, back bool
	for i, N := 0, y.Len(); i < N; i++ {
		d := DistancePlane(y.Get(i), q, n)
		if d > 0 {
			front = true
		} else if d < 0 {
			back = true
		} else {
			return 0
		}

		if front && back {
			return 0
		}
	}

	if front {
		return +1
	} else {
		return -1
	}
}

// IntersectsPolygon returns true if B contains the intersection of A with the
// plane described by the first point of B and the normal N. If the intersection
// lies outside of B, or if there is no intersection, IntersectsPolygon returns
// false.
//
// IntersectsPolygon assumes that B lies within a plane and that N is normal to
// that plane.
func IntersectsPolygon(a, b Polygon, na, nb Vec3) bool {
	return intersectsPolygon(a, b, nb) || intersectsPolygon(b, a, na)
}

func intersectsPolygon(a, b Polygon, n Vec3) bool {
	N := a.Len()
	q := b.Get(0)
	d0, d1, _ := Pointing(n)

	// Calculate distances
	dist := make([]float64, N)
	var pos, neg int
	for i := 0; i < N; i++ {
		dist[i] = DistancePlane(a.Get(i), q, n)
		if dist[i] < 0 {
			neg++
		} else if dist[i] > 0 {
			pos++
		}
	}

	// Coplanar
	if neg == 0 && pos == 0 {
		// B contains one of A's points?
		for i := 0; i < N; i++ {
			if Winding(b, a.Get(i), d0, d1) != 0 {
				return true
			}
		}

		// A contains one of B's points?
		for i, N := 0, b.Len(); i < N; i++ {
			if Winding(a, b.Get(i), d0, d1) != 0 {
				return true
			}
		}

		// If A and B are coplanar but neither contains a point of the other,
		// they must be disjoint.
		return false
	}

	// A lies entirely out of the plane of B
	if (neg == 0) != (pos == 0) {
		return false
	}

	for i := 0; i < N; i++ {
		v, u := a.Get(i), a.Get((i+1)%N)
		vd, ud := dist[i], dist[(i+1)%N]

		// V and U are on the same side of the plane of B
		if vd*ud > 0 {
			continue
		}

		// V is in the plane of B
		if vd == 0 {
			if Winding(b, v, d0, d1) != 0 {
				return true
			}
		}

		// U is in the plane of B
		if ud == 0 {
			if Winding(b, u, d0, d1) != 0 {
				return true
			}
		}

		w, _, ok := IntersectPlane(v, u, q, n)
		if !ok {
			continue
		}

		if _, ok := LiesOnPerimeter(b, w, d0, d1); ok {
			return true
		}

		if Winding(b, w, d0, d1) != 0 {
			return true
		}
	}
	return false
}
