package manifold

import (
	"iter"
	"math"
	"sort"

	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// Volume represents a volume of space.
type Volume tensor.Span3

func (v Volume) Bounds() tensor.Span3 { return tensor.Span3(v) }

// Extent returns the minimum volume that contains the shape.
func Extent(s Polyhedron) Volume {
	if s.Len() == 0 {
		panic("empty shape")
	}

	v := NanVolume()
	v.Encompass(s)
	return v
}

func NanVolume() Volume {
	return Volume{
		Min: tensor.Vec3{math.NaN(), math.NaN(), math.NaN()},
		Max: tensor.Vec3{math.NaN(), math.NaN(), math.NaN()},
	}
}

func (v *Volume) Encompass(s Polyhedron) {
	for i, n := 1, s.Len(); i < n; i++ {
		v.EncompassFace(s.Get(i))
	}
}

func (v *Volume) EncompassFace(f Polygon) {
	if math.IsNaN(v.Min[0]) {
		v.Min = f.Get(0)
		v.Max = v.Min
	}

	for i, n := 0, f.Len(); i < n; i++ {
		u := f.Get(i)
		for k := 0; k < 3; k++ {
			if u[k] < v.Min[k] {
				v.Min[k] = u[k]
			} else if u[k] > v.Max[k] {
				v.Max[k] = u[k]
			}
		}
	}
}

// Intersect returns the intersection volume of two volumes. If ok is false, V
// and U do not intersect.
func (v Volume) Intersect(u Volume) (x Volume, ok bool) {
	for k := 0; k < 3; k++ {
		if v.Min[k] > u.Max[k] || v.Max[k] < u.Min[k] {
			return Volume{}, false
		}

		x.Min[k] = math.Max(v.Min[k], u.Min[k])
		x.Max[k] = math.Min(v.Max[k], u.Max[k])
	}
	return x, true
}

// Cut returns the portion of the shape that lies within the volume.
//
// The shape must represent a closed surface - it must have a well defined
// inside and outside and no holes.
func (v Volume) Cut(s Polyhedron) Polyhedron {
	// See Cleave - this works similarly and the same assumptions hold.

	N := s.Len()
	vc := new(volumeCut)
	vc.Volume = v
	vc.result = make([]Polygon, 0, N)

	sm := AsManifold(s)
	contains := func(p tensor.Vec3) bool {
		return sm.Winding(p) != OutsidePoint
	}

	for i := 0; i < N; i++ {
		f := s.Get(i)
		switch v.containsFace(f) {
		case +1:
			// Facet is inside, keep it
			vc.result = append(vc.result, f)
			continue

		case -1:
			// Facet is outside, discard
			continue
		}

		// Facet is on the border
		f = vc.cutFace(f, false, 0)
		f = vc.cutFace(f, false, 1)
		f = vc.cutFace(f, false, 2)
		f = vc.cutFace(f, true, 0)
		f = vc.cutFace(f, true, 1)
		f = vc.cutFace(f, true, 2)
		if f != nil && f.Len() > 0 {
			vc.result = append(vc.result, f)
		}
	}

	for face := range vc.crossings {
		for edge := range vc.crossings[face] {
			x := vc.crossings[face][edge]
			sort.Slice(x, func(i, j int) bool {
				v, u := x[i].point, x[j].point
				return v < u || v == u && x[i].into
			})

			// Remove duplicates. It's possible this introduces bugs.
			for i := len(x) - 1; i > 0; i-- {
				v, u := x[i], x[i-1]
				if v.into == u.into && math.Abs(v.point-u.point) < tensor.AdjacentEps {
					x = append(x[:i], x[i+1:]...)
				}
			}
			vc.crossings[face][edge] = x
		}
	}

	vc.assembleEdgeCrossings()

	for face, perim := range vc.perimeter {
		var n tensor.Vec3
		if face%2 == 0 {
			n[face/2] = +1
		} else {
			n[face/2] = -1
		}

		for edge, crossings := range vc.crossings[face] {
			if len(crossings) > 0 {
				continue
			}

			l, _ := v.edge(face/2, edge, face%2 == 1)
			if contains(l[0]) && contains(l[1]) {
				perim = append(perim, l)
			}
		}

		vc.result = append(vc.result, assembleLoopsWithHoles(perim, n)...)
	}

	if len(vc.result) == 0 {
		return v
	}
	return New(vc.result)
}

type volumeCut struct {
	Volume
	result []Polygon

	// 6 faces: min x, max x, min y, max y, min z, max z
	// 4 edges: (max, max) => (max, min) => (min, min) => (min, max) =>
	// for edges of min faces, flip d0 & d1
	crossings [6][4][]edgeCrossing
	perimeter [6][]Line
}

type edgeCrossing struct {
	point float64
	into  bool
}

func (v Volume) contains(u tensor.Vec3) bool {
	for k := 0; k < 3; k++ {
		if !(v.Min[k] <= u[k] && u[k] <= v.Max[k]) {
			return false
		}
	}
	return true
}

func (v Volume) containsFace(f Polygon) int {
	var in, out int
	for i := 0; i < f.Len(); i++ {
		u := f.Get(i)
		if v.contains(u) {
			in++
		} else {
			out++
		}
	}

	if in > 0 && out > 0 {
		return 0
	}
	if in > 0 {
		return +1
	}

	var n tensor.Vec3
	if IsConvex(f) {
		n = Normal(f)
	} else {
		n = NormalSum(f)
	}

	if false ||
		IntersectsPolygon(f, v.face(0, v.Min[0]), n, tensor.Vec3{0: 1}) ||
		IntersectsPolygon(f, v.face(0, v.Max[0]), n, tensor.Vec3{0: 1}) ||
		IntersectsPolygon(f, v.face(1, v.Min[1]), n, tensor.Vec3{1: 1}) ||
		IntersectsPolygon(f, v.face(1, v.Max[1]), n, tensor.Vec3{1: 1}) ||
		IntersectsPolygon(f, v.face(2, v.Min[2]), n, tensor.Vec3{2: 1}) ||
		IntersectsPolygon(f, v.face(2, v.Max[2]), n, tensor.Vec3{2: 1}) {
		return 0
	}
	return -1
}

func (v Volume) Len() int { return 6 }

func (v Volume) Get(i int) Polygon {
	switch i {
	case 0:
		return v.face(0, v.Min[0])
	case 1:
		return v.face(0, v.Max[0])
	case 2:
		return v.face(1, v.Min[1])
	case 3:
		return v.face(1, v.Max[1])
	case 4:
		return v.face(2, v.Min[2])
	case 5:
		return v.face(2, v.Max[2])
	}
	panic("out of range")
}

func (v Volume) Faces() iter.Seq2[int, Polygon] {
	return func(yield func(int, Polygon) bool) {
		for i := range 6 {
			if !yield(i, v.Get(i)) {
				return
			}
		}
	}
}

func (v *volumeCut) cutFace(f Polygon, max bool, dim int) Polygon {
	if f == nil {
		return nil
	}

	var q, n tensor.Vec3
	var d0, d1 int
	if max {
		q = v.Max
		n[dim] = -1
		d0, d1 = otherDims(dim)
	} else {
		q = v.Min
		n[dim] = +1
		d1, d0 = otherDims(dim)
	}

	g, _, p, ok := cleaveFace(f, q, n)
	if !ok {
		// Coplanar, don't cut
		return f
	}

	if p == nil {
		return g
	}

	down := func(v tensor.Vec3) tensor.Vec2 { return tensor.Vec2{v[d0], v[d1]} }
	a, b, ok := tensor.ClipLine(down(p[0]), down(p[1]), down(v.Min), down(v.Max))
	if !ok || a.Sub(b).Len() < tensor.AdjacentEps {
		// Discard perimiter if it is outside the volume or is clipped into a
		// single point
		return g
	}

	// Update P to the clipped line
	p[0][d0], p[0][d1] = a.Elem()
	p[1][d0], p[1][d1] = b.Elem()

	// Add P to the appropriate perimeter segment set
	face := dim * 2
	if max {
		face++
	}
	v.perimeter[face] = append(v.perimeter[face], *p)

	// Add the ends of P to crossings
	var fn tensor.Vec3
	if IsConvex(f) {
		fn = Normal(f)
	} else {
		fn = NormalSum(f)
	}
	for _, p := range Vertices(p) {
		if math.Abs(p[d0]-v.Max[d0]) < tensor.AdjacentEps {
			v.addCrossing(p, fn, dim, max, 0)
		}
		if math.Abs(p[d1]-v.Min[d1]) < tensor.AdjacentEps {
			v.addCrossing(p, fn, dim, max, 1)
		}
		if math.Abs(p[d0]-v.Min[d0]) < tensor.AdjacentEps {
			v.addCrossing(p, fn, dim, max, 2)
		}
		if math.Abs(p[d1]-v.Max[d1]) < tensor.AdjacentEps {
			v.addCrossing(p, fn, dim, max, 3)
		}
	}

	return g
}

func (v *volumeCut) addCrossing(w tensor.Vec3, n tensor.Vec3, fdim int, max bool, edge int) {
	l, dim := v.edge(fdim, edge, max)
	len := l[1][dim] - l[0][dim]
	x := edgeCrossing{
		point: math.Abs((l[0][dim] - w[dim]) / len),
		into:  n[dim]*len < 0,
	}

	face := fdim * 2
	if max {
		face++
	}
	v.crossings[face][edge] = append(v.crossings[face][edge], x)
}

func (v Volume) face(d int, u float64) SimplePoly {
	d0, d1 := otherDims(d)

	q := make(SimplePoly, 4)
	q[0][d], q[1][d], q[2][d], q[3][d] = u, u, u, u
	q[0][d0], q[1][d0], q[2][d0], q[3][d0] = v.Min[d0], v.Min[d0], v.Max[d0], v.Max[d0]
	q[0][d1], q[1][d1], q[2][d1], q[3][d1] = v.Min[d1], v.Max[d1], v.Max[d1], v.Min[d1]
	return q
}

// return the start and end of the edge, and the dimension that varies
func (v Volume) edge(fdim, edge int, max bool) (Line, int) {
	var l0, l1 tensor.Vec3
	var d0, d1 int
	if max {
		l0[fdim], l1[fdim] = v.Max[fdim], v.Max[fdim]
		d0, d1 = otherDims(fdim)
	} else {
		l0[fdim], l1[fdim] = v.Min[fdim], v.Min[fdim]
		d1, d0 = otherDims(fdim)
	}

	switch edge {
	case 0: // (max, max) => (max, min)
		l0[d0], l0[d1] = v.Max[d0], v.Max[d1]
		l1[d0], l1[d1] = v.Max[d0], v.Min[d1]
		return Line{l0, l1}, d1
	case 1: // (max, min) => (min, min)
		l0[d0], l0[d1] = v.Max[d0], v.Min[d1]
		l1[d0], l1[d1] = v.Min[d0], v.Min[d1]
		return Line{l0, l1}, d0
	case 2: // (min, min) => (min, max)
		l0[d0], l0[d1] = v.Min[d0], v.Min[d1]
		l1[d0], l1[d1] = v.Min[d0], v.Max[d1]
		return Line{l0, l1}, d1
	case 3: // (min, max) => (max, max)
		l0[d0], l0[d1] = v.Min[d0], v.Max[d1]
		l1[d0], l1[d1] = v.Max[d0], v.Max[d1]
		return Line{l0, l1}, d0
	}
	panic("bad edge")
}

func (v *volumeCut) assembleEdgeCrossings() {
	for face := range v.crossings {
		for edge := range v.crossings[face] {
			x := v.crossings[face][edge]
			if len(x) == 0 {
				continue
			}

			l, dim := v.edge(face/2, edge, face%2 == 1)
			m, b := l[1][dim]-l[0][dim], l[0]
			if !x[0].into {
				if x[0].point > tensor.AdjacentEps {
					v.perimeter[face] = append(v.perimeter[face], Line{l[0], crossingPoint(m, x[0], b, dim)})
				}
				x = x[1:]
			}

			for len(x) >= 2 {
				x0, x1 := x[0], x[1]
				x = x[2:]
				if !(x0.into && !x1.into) {
					panic("invalid crossing pair - not into then out")
				}
				if math.Abs(x0.point-x1.point) < tensor.AdjacentEps {
					// close enough to be the same point
					continue
				}

				v.perimeter[face] = append(v.perimeter[face], Line{crossingPoint(m, x0, b, dim), crossingPoint(m, x1, b, dim)})
			}

			if len(x) == 0 {
				continue
			}
			if !x[0].into {
				panic("invalid trailing crossing - direction is out")
			}
			if 1-x[0].point > tensor.AdjacentEps {
				v.perimeter[face] = append(v.perimeter[face], Line{crossingPoint(m, x[0], b, dim), l[1]})
			}
		}
	}
}

func otherDims(dim int) (d0, d1 int) {
	switch dim {
	case 0:
		return 1, 2
	case 1:
		return 2, 0
	case 2:
		return 0, 1
	}
	panic("bad dim")
}

func crossingPoint(m float64, x edgeCrossing, b tensor.Vec3, dim int) tensor.Vec3 {
	p := b
	p[dim] += m * x.point
	return p
}
