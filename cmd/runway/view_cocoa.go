//go:build darwin && !gtk
// +build darwin,!gtk

package main

import (
	_ "embed"
	"log"
	"time"

	"github.com/progrium/macdriver/cocoa"
	"github.com/progrium/macdriver/core"
	"github.com/progrium/macdriver/objc"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"gitlab.com/eukano/runway/internal/model"
	"gitlab.com/eukano/runway/internal/ui"
	cocoaui "gitlab.com/eukano/runway/internal/ui/cocoa"
	"golang.org/x/image/font"
)

// #include <stdint.h>
// #include <pthread.h>
import "C"

func tid(location string) {
	var id uint64
	t := C.pthread_self()
	ok := C.pthread_threadid_np(t, (*C.uint64_t)(&id))
	if ok == 0 {
		println(location, "Thread", id)
	} else {
		println(location, "Unknown thread")
	}
}

//go:generate ibtool --output-format human-readable-text --compile viewer.nib viewer.xib

type ViewApp struct {
	cocoa.NSApplication
	UI *ui.UI
	GL cocoa.NSView
}

type ViewHostOptions struct {
	NoCull bool
	Font   font.Face
	Lights []model.Light
}

type ViewHost struct {
	*ViewApp
	*ViewHostOptions
}

var view = cobra.Command{
	Use: "view [model]",
}

var viewHostOpts = &ViewHostOptions{
	Lights: ui.DefaultLights,
}

var viewSkybox = view.Flags().Bool("skybox", true, "render with a skybox")

//go:embed viewer.nib
var viewNib []byte

func init() {
	view.Run = viewRun
	cmd.AddCommand(&view)

	glFlags := pflag.NewFlagSet("OpenGL flags", pflag.ExitOnError)
	glFlags.BoolVar(&viewHostOpts.NoCull, "gl-disable-face-culling", false, "disable OpenGL face culling")

	view.Flags().AddFlagSet(glFlags)
}

func viewRun(cmd *cobra.Command, args []string) {
	tid("[Main]")
	app := new(ViewApp)
	app.UI = ui.New(&ViewHost{ViewApp: app, ViewHostOptions: viewHostOpts})

	if len(args) == 0 {
		app.UI.AddModel(ui.DefaultModel)
	} else {
		if *viewSkybox {
			app.UI.AddModel(ui.Skybox)
		}

		m, err := ui.InterpretModels(args)
		if err != nil {
			log.Fatal(err)
		}

		for _, m := range m {
			app.UI.AddModel(m)
		}
	}

	app.NSApplication = cocoa.NSApp_WithDidLaunch(func(notification objc.Object) {
		tid("[DidLaunch]")
		var win cocoa.NSWindow
		cocoaui.LoadEmbeddedNib(app.NSApplication, viewNib, func(o objc.Object) {
			switch o.Class().String() {
			case "NSWindow":
				win = cocoa.NSWindow{Object: o}
				app.GL = win.ContentView()
			}
		})

		if app.GL.Pointer() == 0 {
			log.Fatal("Could not locate window")
		}

		// if app.GL.Class().String() != "RWOpenGLView" {
		// 	log.Fatal("Content view is the wrong type")
		// }

		frame := app.GL.Frame()
		newGL := objc.Get("RWOpenGLView").Alloc().Send("initWithFrame:", frame)
		newGL.Send("awakeFromNib")
		app.GL = cocoa.NSView{Object: newGL}
		win.SetContentView(newGL)

		app.GL.Send("openGLContext").Send("makeCurrentContext")
		err := app.UI.Prepare()
		if err != nil {
			log.Fatal(err)
		}

		var isSetup bool
		cocoaui.SetRenderFunc(app.GL, func(view objc.Object) bool {
			if !isSetup {
				isSetup = true
			}

			err := app.UI.Render()
			if err != nil {
				log.Fatal(err)
			}
			return true
		})

		app.ActivateIgnoringOtherApps(true)
	})

	app.SetActivationPolicy(cocoa.NSApplicationActivationPolicyRegular)

	app.Run()
}

func (h *ViewHost) DebugEnabled() bool   { return false }
func (h *ViewHost) CullingEnabled() bool { return !h.NoCull }

func (h *ViewHost) GetFont() (font.Face, error) { return h.Font, nil }
func (h *ViewHost) Lights() []model.Light       { return h.ViewHostOptions.Lights }

func (h *ViewHost) Size() (width, height int) {
	frame := h.GL.Frame()
	return int(frame.Size.Width), int(frame.Size.Height)
}

func (h *ViewHost) QueueDraw() {
	h.GL.Send("setNeedsDisplay:", true)
}

func (h *ViewHost) AddTick(fn func(time.Duration)) (clear func(), err error) {
	t := time.NewTicker(16 * time.Millisecond)

	start := time.Now()
	go func() {
		for now := range t.C {
			core.Dispatch(func() { fn(now.Sub(start)) })
		}
	}()

	return func() { t.Stop() }, nil
}
