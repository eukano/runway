package main

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/spf13/pflag"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

func parseVec(s string, v []float64) error {
	bits := strings.Split(s, ",")
	if len(bits) != len(v) {
		return fmt.Errorf("expected %d elements, got %d", len(v), len(bits))
	}

	for i := range v {
		u, err := strconv.ParseFloat(bits[i], 32)
		if err != nil {
			return fmt.Errorf("component %d: %w", i, err)
		}
		v[i] = float64(u)
	}
	return nil
}

type flagVec2 struct {
	value *tensor.Vec2
}

func fVec2(val tensor.Vec2, ptr *tensor.Vec2) *flagVec2 {
	v := new(flagVec2)
	v.value = ptr
	*v.value = val
	return v
}

func Vec2(f *pflag.FlagSet, name string, value tensor.Vec2, usage string) *tensor.Vec2 {
	ptr := new(tensor.Vec2)
	f.VarP(fVec2(value, ptr), name, "", usage)
	return ptr
}

func Vec2P(f *pflag.FlagSet, name, shorthand string, value tensor.Vec2, usage string) *tensor.Vec2 {
	ptr := new(tensor.Vec2)
	f.VarP(fVec2(value, ptr), name, shorthand, usage)
	return ptr
}

func (v *flagVec2) Type() string   { return "vec2" }
func (v *flagVec2) String() string { return fmt.Sprint(v.value[:]) }

func (v *flagVec2) Set(val string) error {
	var u tensor.Vec2
	err := parseVec(val, u[:])
	if err != nil {
		return err
	}

	*v.value = u
	return nil
}

type flagVec3 struct {
	value *tensor.Vec3
}

func fVec3(val tensor.Vec3, ptr *tensor.Vec3) *flagVec3 {
	v := new(flagVec3)
	v.value = ptr
	*v.value = val
	return v
}

func Vec3(f *pflag.FlagSet, name string, value tensor.Vec3, usage string) *tensor.Vec3 {
	ptr := new(tensor.Vec3)
	f.VarP(fVec3(value, ptr), name, "", usage)
	return ptr
}

func Vec3P(f *pflag.FlagSet, name, shorthand string, value tensor.Vec3, usage string) *tensor.Vec3 {
	ptr := new(tensor.Vec3)
	f.VarP(fVec3(value, ptr), name, shorthand, usage)
	return ptr
}

func (v *flagVec3) Type() string   { return "vec3" }
func (v *flagVec3) String() string { return fmt.Sprint(v.value[:]) }

func (v *flagVec3) Set(val string) error {
	var u tensor.Vec3
	err := parseVec(val, u[:])
	if err != nil {
		return err
	}

	*v.value = u
	return nil
}
