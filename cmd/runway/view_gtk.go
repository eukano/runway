//go:build (linux && !nogtk) || gtk
// +build linux,!nogtk gtk

package main

import (
	"bytes"
	"context"
	"embed"
	"image"
	"image/jpeg"
	"image/png"
	"log"
	"os"
	"time"
	"unsafe"

	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"github.com/icza/mjpeg"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"gitlab.com/eukano/runway/internal/glu"
	"gitlab.com/eukano/runway/internal/model"
	"gitlab.com/eukano/runway/internal/ui"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
	"golang.org/x/image/font"
)

type ViewApp struct {
	*gtk.Application
	Win     *gtk.ApplicationWindow
	Actions map[string]*glib.SimpleAction
	UI      *ui.UI
	GL      *gtk.GLArea

	recordCancel context.CancelFunc
	files        []string
}

type ViewHostOptions struct {
	Debug  bool
	NoCull bool
	Font   font.Face
	Lights []model.Light
}

type ViewHost struct {
	*ViewApp
	*ViewHostOptions
}

const (
	captureNone = iota
	captureRender
	captureRecordStart
	captureRecording
)

//go:embed viewer.glade
var viewFiles embed.FS

var view = cobra.Command{
	Use: "view [model]",
}

var viewSkybox = view.Flags().Bool("skybox", true, "render with a skybox")

func init() {
	view.PreRun = glPreRun
	view.Run = viewRun
	cmd.AddCommand(&view)

	glFlags := pflag.NewFlagSet("OpenGL flags", pflag.ExitOnError)
	glFlags.BoolVar(&viewHostOpts.Debug, "gl-debug", false, "print OpenGL debug messages")
	glFlags.BoolVar(&viewHostOpts.NoCull, "gl-disable-face-culling", false, "disable OpenGL face culling")

	view.Flags().AddFlagSet(glFlags)
}

var gtkDomain = glib.QuarkFromString("runway")

var viewHostOpts = &ViewHostOptions{
	Lights: ui.DefaultLights,
}

var testText = &model.Text{
	Value: "The quick brown fox jumps over the lazy dog",
	Color: glu.Color.W.Vec4(1),
	Pos:   tensor.Vec2{50, 300},
	Size:  1,
}

func glPreRun(*cobra.Command, []string) {
	font := loadFont()
	viewHostOpts.Font = font
	testText.Font = font
}

func viewRun(cmd *cobra.Command, args []string) {
	args = append([]string{os.Args[0]}, args...)
	gtk.Init(&args)

	_app, err := gtk.ApplicationNew("com.eukano.runway.viewer", glib.APPLICATION_HANDLES_OPEN)
	if err != nil {
		log.Fatal(err)
	}

	app := ViewApp{
		Application: _app,
	}

	app.Connect("startup", app.startup)
	app.Connect("activate", app.activate)
	app.Connect("open", app.open)

	os.Exit(app.Run(args))
}

func (app *ViewApp) startup() {
	var vars struct {
		MenuBar  *glib.MenuModel
		DrawArea *gtk.GLArea
	}
	_, err := LoadFS(viewFiles, "viewer.glade", &vars)
	if err != nil {
		log.Fatal(err)
	}
	app.GL = vars.DrawArea
	app.GL.SetRequiredVersion(4, 6)

	actions := map[string]interface{}{
		"quit":        app.Quit,
		"reload":      app.reload,
		"render":      app.render,
		"recordStart": app.startRecording,
		"recordStop":  nil,
	}
	app.Actions = make(map[string]*glib.SimpleAction, len(actions))
	for name, activate := range actions {
		act := glib.SimpleActionNew(name, nil)
		app.Actions[name] = act
		if activate != nil {
			act.Connect("activate", activate)
		}
		app.AddAction(act)
	}

	app.Actions["recordStop"].SetEnabled(false)

	app.SetAccelsForAction("app.quit", []string{"<Primary>Q"})
	app.SetAccelsForAction("app.reload", []string{"<Primary>R"})
	app.SetMenubar(vars.MenuBar)

	app.UI = ui.New(&ViewHost{
		ViewApp:         app,
		ViewHostOptions: viewHostOpts,
	})

	glfn := func(fn func() error) func() {
		return func() {
			app.GL.MakeCurrent()
			err := fn()
			if err != nil {
				log.Print(err)
				app.GL.SetError(gtkDomain, 0, err)
			}
		}
	}

	app.GL.Connect("realize", glfn(app.UI.Prepare))
	app.GL.Connect("render", glfn(app.UI.Render))
	app.GL.Connect("unrealize", glfn(app.UI.Cleanup))

	app.Win, err = gtk.ApplicationWindowNew(app.Application)
	if err != nil {
		log.Fatal(err)
	}

	app.Win.SetTitle("Runway Viewer")
	app.Win.SetDefaultSize(1600, 1200)

	app.Win.Connect("destroy", app.Quit)
	app.Win.Connect("key-press-event", func(_ *gtk.ApplicationWindow, e *gdk.Event) {
		app.UI.Event(convertKey(e), true)
	})
	app.Win.Connect("key-release-event", func(_ *gtk.ApplicationWindow, e *gdk.Event) {
		app.UI.Event(convertKey(e), false)
	})

	app.Win.Add(vars.DrawArea)
}

func convertKey(e *gdk.Event) ui.Key {
	switch gdk.EventKeyNewFromEvent(e).KeyVal() {
	case gdk.KEY_Up:
		return ui.KeyUp
	case gdk.KEY_Down:
		return ui.KeyDown
	case gdk.KEY_Right:
		return ui.KeyRight
	case gdk.KEY_Left:
		return ui.KeyLeft
	case gdk.KEY_w:
		return ui.KeyW
	case gdk.KEY_s:
		return ui.KeyS
	case gdk.KEY_a:
		return ui.KeyA
	case gdk.KEY_d:
		return ui.KeyD
	case gdk.KEY_q:
		return ui.KeyQ
	case gdk.KEY_e:
		return ui.KeyE
	}
	return ui.KeyNone
}

func (app *ViewApp) activate() {
	app.UI.AddModel(ui.DefaultModel)
	app.Win.ShowAll()
}

func (app *ViewApp) open(_ *gtk.Application, filePtrs unsafe.Pointer, count int, hint string) {
	files := make([]string, count)
	for i := range files {
		ptr := (*unsafe.Pointer)(unsafe.Pointer(uintptr(filePtrs) + unsafe.Sizeof(uintptr(0))*uintptr(i)))
		f := &glib.File{
			Object: &glib.Object{
				GObject: glib.ToGObject(*ptr),
			},
		}
		files[i] = f.GetPath()
	}

	app.files = files
	app.openFiles()
}

func (app *ViewApp) openFiles() {
	if *viewSkybox {
		app.UI.AddModel(ui.Skybox)
	}

	m, err := ui.InterpretModels(app.files)
	if err == nil {
		for _, m := range m {
			app.UI.AddModel(m)
		}
		app.Win.ShowAll()
		return
	}

	log.Print(err)

	app.Win.GetChildren().FreeFull(func(item interface{}) { app.Win.Remove(item.(gtk.IWidget)) })

	label, err := gtk.LabelNew(err.Error())
	if err != nil {
		log.Fatal(err)
	}
	app.Win.Add(label)

	app.Win.ShowAll()
}

func (app *ViewApp) reload() {
	app.UI.Cleanup()

	if len(app.files) > 0 {
		app.openFiles()
	} else {
		app.activate()
	}

	app.UI.Prepare()
}

func (app *ViewApp) setCaptureMode(mode int) {
	switch mode {
	case captureNone:
		app.Actions["render"].SetEnabled(true)
		app.Actions["recordStart"].SetEnabled(true)
		app.Actions["recordStop"].SetEnabled(false)
	case captureRender, captureRecordStart:
		app.Actions["render"].SetEnabled(false)
		app.Actions["recordStart"].SetEnabled(false)
		app.Actions["recordStop"].SetEnabled(false)
	case captureRecording:
		app.Actions["render"].SetEnabled(false)
		app.Actions["recordStart"].SetEnabled(false)
		app.Actions["recordStop"].SetEnabled(true)
	}
}

func (app *ViewApp) showError(format string, args ...interface{}) {
	msg := gtk.MessageDialogNew(
		app.Win, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_CLOSE, format, args...)
	msg.Run()
	msg.Close()
}

func (app *ViewApp) saveFile(title string) string {
	dialog, err := gtk.FileChooserDialogNewWith2Buttons(
		"Render", app.Win, gtk.FILE_CHOOSER_ACTION_SAVE,
		"Save", gtk.RESPONSE_OK,
		"Cancel", gtk.RESPONSE_CANCEL,
	)
	if err != nil {
		log.Fatal(err)
	}

	if dialog.Run() != gtk.RESPONSE_OK {
		return ""
	}
	dialog.Close()

	return dialog.GetFilename()
}

func (app *ViewApp) createFile(title string) *os.File {
	name := app.saveFile(title)
	if name == "" {
		return nil
	}

	f, err := os.Create(name)
	if err != nil {
		app.showError("Failed to create %q: %v", name, err)
		return nil
	}

	return f
}

func (app *ViewApp) render() {
	app.setCaptureMode(captureRender)
	defer app.setCaptureMode(captureNone)

	f := app.createFile("Render")
	if f == nil {
		return
	}

	go app.captureFrame(f)
}

func (app *ViewApp) captureFrame(f *os.File) {
	defer f.Close()

	ch := make(chan *image.RGBA, 1)
	app.UI.SetFrameCapture(func(i *image.RGBA) bool {
		ch <- i
		close(ch)
		return false
	})

	err := png.Encode(f, <-ch)
	if err != nil {
		glib.IdleAddPriority(glib.PRIORITY_HIGH, func() {
			app.showError("Failed to encode PNG: %v", err)
		})
	}
}

func (app *ViewApp) startRecording() {
	app.setCaptureMode(captureRecordStart)

	name := app.saveFile("Record")
	if name == "" {
		app.setCaptureMode(captureNone)
		return
	}

	width, height := app.GL.GetAllocatedWidth(), app.GL.GetAllocatedHeight()
	w, err := mjpeg.New(name, int32(width), int32(height), 60)
	if err != nil {
		app.showError("Failed to open AVI: %v", err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	stopAct := app.Actions["recordStop"]
	stopSig := stopAct.Connect("activate", cancel)

	app.setCaptureMode(captureRecording)

	go func() {
		app.captureFrames(w, ctx)
		glib.IdleAdd(func() {
			stopAct.HandlerDisconnect(stopSig)
			app.setCaptureMode(captureNone)
		})
	}()
}

func (app *ViewApp) captureFrames(w mjpeg.AviWriter, ctx context.Context) {
	ch := make(chan *image.RGBA, 1)
	app.UI.SetFrameCapture(func(i *image.RGBA) bool {
		select {
		case <-ctx.Done():
			close(ch)
			return false

		case ch <- i:
			return true
		}
	})

	var err error
	var buf bytes.Buffer
	for img := range ch {
		buf.Reset()
		err = jpeg.Encode(&buf, img, nil)
		if err != nil {
			break
		}

		err = w.AddFrame(buf.Bytes())
		if err != nil {
			break
		}
	}
	if err == nil {
		err = w.Close()
	}
	if err != nil {
		glib.IdleAddPriority(glib.PRIORITY_HIGH, func() {
			app.showError("Failed to encode AVI: %v", err)
		})
	}
}

func (h *ViewHost) DebugEnabled() bool          { return h.Debug }
func (h *ViewHost) CullingEnabled() bool        { return !h.NoCull }
func (h *ViewHost) GetFont() (font.Face, error) { return h.Font, nil }
func (h *ViewHost) Lights() []model.Light       { return h.ViewHostOptions.Lights }

func (h *ViewHost) QueueDraw() {
	h.GL.QueueDraw()
}

func (h *ViewHost) Size() (width, height int) {
	return h.Win.GetAllocatedWidth(), h.Win.GetAllocatedHeight()
}

func (h *ViewHost) frameTime() time.Duration {
	return time.Duration(h.GL.GetFrameClock().GetFrameTime()) * time.Microsecond
}

func (h *ViewHost) AddTick(fn func(runtime time.Duration)) (clear func(), err error) {
	start := h.frameTime()
	id := h.GL.AddTickCallback(func(*gtk.Widget, *gdk.FrameClock) bool { fn(h.frameTime() - start); return true })
	return func() { h.GL.RemoveTickCallback(id) }, nil
}
