package main

import (
	"fmt"
	"os"
	"runtime"

	"github.com/spf13/cobra"
)

func init() {
	// On macOS, calls to Cocoa will crash unless they're made on the main
	// OS thread. Locking during init guarantees that main will be locked to
	// the main OS thread. It may be possible to relax this on Linux and
	// Windows.
	runtime.LockOSThread()
}

var cmd = cobra.Command{
	Use: "runway",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Usage()
		os.Exit(2)
	},
}

func main() {
	has := map[string]bool{}
	for _, cmd := range cmd.Commands() {
		has[cmd.Name()] = true
	}

	addUnsupported(has, "view", "unavailable - requires GTK", "compiled without GTK support")
	addUnsupported(has, "info", "unavailable - requires GTK", "compiled without GTK support")
	addUnsupported(has, "render", "unavailable - requires EGL", "compiled without EGL support")

	cmd.ExecuteC()
}

func addUnsupported(has map[string]bool, name, short, reason string) {
	if has[name] {
		return
	}

	cmd.AddCommand(&cobra.Command{
		Use:   name,
		Short: short,
		Long:  fmt.Sprintf("Subcommand `%s` is unavailable as runway was %s", name, reason),
		Run:   runUnsupported,
	})
}

func runUnsupported(cmd *cobra.Command, args []string) {
	fmt.Fprintln(os.Stderr, cmd.Long)
	os.Exit(1)
}
