//go:build (linux && !nogtk) || gtk
// +build linux,!nogtk gtk

package main

import (
	"fmt"
	"io/fs"
	"io/ioutil"
	"reflect"
	"strings"

	"github.com/gotk3/gotk3/gtk"
)

func LoadFS(files fs.FS, name string, vars interface{}) (*gtk.Builder, error) {
	f, err := files.Open(name)
	if err != nil {
		return nil, fmt.Errorf("could not open %q: %v", name, err)
	}
	defer f.Close()

	b, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, fmt.Errorf("could not read %q: %v", name, err)
	}

	return Load(string(b), vars)
}

func Load(s string, vars interface{}) (*gtk.Builder, error) {
	rvars := reflect.ValueOf(vars)
	rtyp := rvars.Type()
	if rtyp.Kind() == reflect.Ptr && rtyp.Elem().Kind() == reflect.Struct {
		rtyp = rtyp.Elem()
	} else {
		return nil, fmt.Errorf("vars must be a struct pointer; got %T", vars)
	}

	bld, err := gtk.BuilderNew()
	if err != nil {
		return nil, fmt.Errorf("failed to create builder: %w", err)
	}

	err = bld.AddFromString(s)
	if err != nil {
		return nil, fmt.Errorf("failed to add from string: %w", err)
	}

	for i := 0; i < rtyp.NumField(); i++ {
		fld := rtyp.Field(i)

		name, ok := fld.Tag.Lookup("gtk")
		if !ok {
			name = fld.Name
		} else if name == "-" {
			continue
		}

		o, err := bld.GetObject(name)
		if err != nil && !ok {
			o, err = bld.GetObject(strings.ToLower(name))
		}
		if err != nil {
			return nil, fmt.Errorf("could not locate %q: %w", name, err)
		}

		ro := reflect.ValueOf(o)
		if !ro.Type().AssignableTo(fld.Type) {
			return nil, fmt.Errorf("object %q: cannot assign %T to %v", name, o, fld.Type)
		}

		rvars.Elem().Field(i).Set(ro)
	}

	return bld, nil
}

func LoadSingle(s, name string, v interface{}) (*gtk.Builder, error) {
	rv := reflect.ValueOf(v)
	if rv.Kind() != reflect.Ptr {
		return nil, fmt.Errorf("v must be a pointer; got %T", v)
	}

	bld, err := gtk.BuilderNew()
	if err != nil {
		return nil, fmt.Errorf("failed to create builder: %w", err)
	}

	err = bld.AddFromString(s)
	if err != nil {
		return nil, fmt.Errorf("failed to add from string: %w", err)
	}

	o, err := bld.GetObject(name)
	if err != nil {
		return nil, fmt.Errorf("could not locate %q: %w", name, err)
	}

	ro := reflect.ValueOf(o)
	if !ro.Type().AssignableTo(rv.Type().Elem()) {
		return nil, fmt.Errorf("object %q: cannot assign %T to %T", name, o, v)
	}

	rv.Elem().Set(ro)
	return bld, nil
}
