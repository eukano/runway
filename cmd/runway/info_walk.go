//go:build windows && !gtk
// +build windows,!gtk

package main

import (
	"fmt"
	"log"
	"unsafe"

	"github.com/go-gl/gl/all-core/gl"
	"github.com/lxn/walk"
	"github.com/lxn/win"
	"github.com/spf13/cobra"
)

var info = cobra.Command{
	Use: "info",
}

func init() {
	info.Run = infoRun
	cmd.AddCommand(&info)
}

func infoRun(cmd *cobra.Command, args []string) {
	w, err := walk.NewMainWindow()
	if err != nil {
		log.Fatal(err)
	}

	hDC := win.GetDC(w.Handle())
	defer win.ReleaseDC(w.Handle(), hDC)

	pdf := win.PIXELFORMATDESCRIPTOR{
		NSize:       uint16(unsafe.Sizeof(win.PIXELFORMATDESCRIPTOR{})),
		NVersion:    1,
		DwFlags:     win.PFD_DRAW_TO_WINDOW | win.PFD_SUPPORT_OPENGL | win.PFD_DOUBLEBUFFER,
		DwLayerMask: win.PFD_MAIN_PLANE,
		IPixelType:  win.PFD_TYPE_RGBA,
		CColorBits:  32,
		CDepthBits:  8,
	}

	pixFmt := win.ChoosePixelFormat(hDC, &pdf)
	if pixFmt == 0 {
		log.Fatal("ChoosePixelFormat", win.GetLastError())
	}

	if !win.SetPixelFormat(hDC, pixFmt, &pdf) {
		log.Fatal("SetPixelFormat", win.GetLastError())
	}

	hRC := win.WglCreateContext(hDC)
	defer win.WglDeleteContext(hRC)

	win.WglMakeCurrent(hDC, hRC)
	defer win.WglMakeCurrent(0, 0)

	err = gl.Init()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(gl.GoStr(gl.GetString(gl.RENDERER)))
	fmt.Println(gl.GoStr(gl.GetString(gl.VERSION)))
}
