//go:build windows && !gtk
// +build windows,!gtk

package main

import (
	"fmt"
	"log"
	"os"
	"time"
	"unsafe"

	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
	"github.com/lxn/win"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"gitlab.com/eukano/runway/internal/model"
	"gitlab.com/eukano/runway/internal/ui"
	"gitlab.com/eukano/runway/script/interpreter"
	"golang.org/x/image/font"
)

//go:generate go run github.com/akavel/rsrc -arch 386 -manifest runway.exe.manifest
//go:generate go run github.com/akavel/rsrc -arch amd64 -manifest runway.exe.manifest

type ViewApp struct {
	UI *ui.UI
	GL *walk.OpenGL
}

type ViewHostOptions struct {
	Debug  bool
	NoCull bool
	Font   font.Face
	Lights []model.Light
}

type ViewHost struct {
	*ViewApp
	*ViewHostOptions
}

var view = cobra.Command{
	Use: "view [model]",
}

var viewSkybox = view.Flags().Bool("skybox", true, "render with a skybox")

func init() {
	walk.SetLogErrors(true)
	walk.SetPanicOnError(true)

	view.Run = viewRun
	cmd.AddCommand(&view)

	glFlags := pflag.NewFlagSet("OpenGL flags", pflag.ExitOnError)
	glFlags.BoolVar(&viewHostOpts.Debug, "gl-debug", false, "print OpenGL debug messages")
	glFlags.BoolVar(&viewHostOpts.NoCull, "gl-disable-face-culling", false, "disable OpenGL face culling")

	view.Flags().AddFlagSet(glFlags)
}

var viewHostOpts = &ViewHostOptions{
	Lights: ui.DefaultLights,
}

var viewPFD = win.PIXELFORMATDESCRIPTOR{
	NSize:       uint16(unsafe.Sizeof(win.PIXELFORMATDESCRIPTOR{})),
	NVersion:    1,
	DwFlags:     win.PFD_DRAW_TO_WINDOW | win.PFD_SUPPORT_OPENGL | win.PFD_DOUBLEBUFFER,
	DwLayerMask: win.PFD_MAIN_PLANE,
	IPixelType:  win.PFD_TYPE_RGBA,
	CColorBits:  32,
	CDepthBits:  8,
}

func winErr(fn string) error {
	return fmt.Errorf("%s: %d", fn, win.GetLastError())
}

func viewRun(cmd *cobra.Command, args []string) {
	app := new(ViewApp)
	app.UI = ui.New(&ViewHost{app, viewHostOpts})

	if len(args) == 0 {
		app.UI.AddModel(ui.DefaultModel)
	} else {
		if *viewSkybox {
			app.UI.AddModel(ui.Skybox)
		}

		m, err := ui.InterpretModels(args)
		if err != nil {
			log.Fatal(err)
		}

		for _, m := range m {
			app.UI.AddModel(m)
		}
	}

	var contextFlags int32
	if viewHostOpts.Debug {
		contextFlags |= WGL_CONTEXT_DEBUG_BIT_ARB
	}

	var didSetup bool

	exit, err := MainWindow{
		Title:   "Runway Viewer",
		MinSize: Size{Width: 320, Height: 240},
		Layout:  HBox{},

		OnKeyDown: func(key walk.Key) { app.UI.Event(convertKey(key), true) },
		OnKeyUp:   func(key walk.Key) { app.UI.Event(convertKey(key), false) },

		Children: []Widget{
			OpenGL{
				AssignTo: &app.GL,
				PixelFormat: []int32{
					WGL_SUPPORT_OPENGL_ARB, 1,
					WGL_DRAW_TO_WINDOW_ARB, 1,
					WGL_DOUBLE_BUFFER_ARB, 1,
					WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
					WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
					WGL_COLOR_BITS_ARB, 32,
					WGL_DEPTH_BITS_ARB, 24,
					WGL_ALPHA_BITS_ARB, 8,
				},
				Context: []int32{
					WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
					WGL_CONTEXT_MAJOR_VERSION_ARB, 4,
					WGL_CONTEXT_MINOR_VERSION_ARB, 6,
					WGL_CONTEXT_FLAGS_ARB, contextFlags,
				},
				Setup: func(glc *walk.OpenGLContext) error {
					didSetup = true
					err := app.UI.Prepare()
					if err != nil {
						log.Fatal(err)
					}

					return nil
				},
				Paint: func(glc *walk.OpenGLContext) error {
					if !didSetup {
						log.Fatal("Did not setup")
					}
					err := app.UI.Render()
					win.SwapBuffers(glc.DC())
					return err
				},
				Teardown: func(*walk.OpenGLContext) error {
					return app.UI.Cleanup()
				},
			},
		},
	}.Run()
	if err != nil {
		log.Fatal(err)
	}
	if exit < 0 {
		log.Fatalf("Error %d", win.GetLastError())
	}
	os.Exit(exit)
}

func convertKey(key walk.Key) ui.Key {
	switch key {
	case walk.KeyUp:
		return ui.KeyUp
	case walk.KeyDown:
		return ui.KeyDown
	case walk.KeyRight:
		return ui.KeyRight
	case walk.KeyLeft:
		return ui.KeyLeft
	case walk.KeyW:
		return ui.KeyW
	case walk.KeyS:
		return ui.KeyS
	case walk.KeyA:
		return ui.KeyA
	case walk.KeyD:
		return ui.KeyD
	case walk.KeyQ:
		return ui.KeyQ
	case walk.KeyE:
		return ui.KeyE
	}
	return ui.KeyNone
}

func (h *ViewHost) DebugEnabled() bool          { return h.Debug }
func (h *ViewHost) CullingEnabled() bool        { return !h.NoCull }
func (h *ViewHost) Size() (width, height int)   { return h.GL.Width(), h.GL.Height() }
func (h *ViewHost) GetFont() (font.Face, error) { return h.Font, nil }
func (h *ViewHost) Lights() []model.Light       { return h.ViewHostOptions.Lights }

func (h *ViewHost) QueueDraw() {
	win.RedrawWindow(h.GL.Handle(), nil, 0, win.RDW_INVALIDATE|win.RDW_INTERNALPAINT)
}

func (h *ViewHost) AddTick(fn func(time.Duration)) (clear func(), err error) {
	start := time.Now()
	id, err := h.GL.AddTimer(16*time.Millisecond, func(*walk.WindowBase) { fn(time.Now().Sub(start)) })
	if err != nil {
		return nil, err
	}

	return func() { h.GL.ClearTimer(id) }, nil
}
