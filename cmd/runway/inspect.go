package main

import (
	"fmt"
	"runtime"

	"github.com/spf13/cobra"
	"gitlab.com/eukano/runway/internal/model"
	"gitlab.com/eukano/runway/internal/ui"
	"gitlab.com/eukano/runway/script/interpreter"
)

var inspect = cobra.Command{
	Use: "inspect [model]",
}

var inspectTransforms = inspect.Flags().Bool("transforms", false, "show transform matrices")

func init() {
	inspect.Run = inspectRun
	cmd.AddCommand(&inspect)
}

func inspectRun(cmd *cobra.Command, args []string) {
	runtime.UnlockOSThread()

	i := interpreter.New(nil)

	for _, path := range args {
		err := i.EvalPath(path)
		if err == nil {
			continue
		}

		ui.PrintInterpErr(path, err)
	}

	for _, m := range i.Models() {
		showModelSummary(m.Inspect(), "")
	}
}

func showModelSummary(s model.Summary, indent string) {
	fmt.Printf("%s- %v\n", indent, s)
	if *inspectTransforms {
		for _, x := range s.Transformations {
			fmt.Printf("%s  > %v\n", indent, x)
		}
	}
	for _, s := range s.Within {
		showModelSummary(s, indent+"  ")
	}
}
