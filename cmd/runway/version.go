package main

import (
	"fmt"
	"os"
	"runtime"
	"runtime/debug"

	// specifically import all-core to allow for any version

	"github.com/spf13/cobra"
)

var version = cobra.Command{
	Use: "version",
}

func init() {
	version.Run = versionRun
	cmd.AddCommand(&version)
}

func versionRun(cmd *cobra.Command, args []string) {
	runtime.UnlockOSThread()

	info, ok := debug.ReadBuildInfo()
	if !ok {
		fmt.Fprintf(os.Stderr, "Module information missing or invalid\n")
		os.Exit(1)
	}

	fmt.Printf("runway-%v", info.Main.Version)
	if info.Main.Sum != "" {
		fmt.Printf(" {%v}", info.Main.Sum)
	}
	fmt.Println()
}
