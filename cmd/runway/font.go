package main

import (
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"

	"github.com/adrg/sysfont"
	"golang.org/x/image/font"
	"golang.org/x/image/font/opentype"
)

var fontNames = cmd.PersistentFlags().StringArray("font", []string{"Hack"}, "default fonts, in order of preference")

func findFont() *sysfont.Font {
	finder := sysfont.NewFinder(nil)
	for _, f := range finder.List() {
		for _, name := range *fontNames {
			if strings.HasPrefix(filepath.Base(f.Filename), name+"-") {
				return f
			}
		}
	}
	return finder.Match("")
}

func loadFont() font.Face {
	b, err := ioutil.ReadFile(findFont().Filename)
	if err != nil {
		log.Fatal(err)
	}

	font, err := opentype.Parse(b)
	if err != nil {
		log.Fatal(err)
	}

	var opts *opentype.FaceOptions
	opts = &opentype.FaceOptions{
		Size: 12 * 5,
		DPI:  72,
	}

	face, err := opentype.NewFace(font, opts)
	if err != nil {
		log.Fatal(err)
	}

	return face
}
