package main

import (
	"fmt"
	"syscall"
	"unsafe"

	"github.com/lxn/win"
	"golang.org/x/sys/windows"
)

var libgdi32 = windows.NewLazySystemDLL("gdi32.dll")
var describePixelFormatAddr = libgdi32.NewProc("DescribePixelFormat")

// wglChoosePixelFormatARB
const (
	WGL_NUMBER_PIXEL_FORMATS_ARB    = 0x2000
	WGL_DRAW_TO_WINDOW_ARB          = 0x2001
	WGL_DRAW_TO_BITMAP_ARB          = 0x2002
	WGL_ACCELERATION_ARB            = 0x2003
	WGL_NEED_PALETTE_ARB            = 0x2004
	WGL_NEED_SYSTEM_PALETTE_ARB     = 0x2005
	WGL_SWAP_LAYER_BUFFERS_ARB      = 0x2006
	WGL_SWAP_METHOD_ARB             = 0x2007
	WGL_NUMBER_OVERLAYS_ARB         = 0x2008
	WGL_NUMBER_UNDERLAYS_ARB        = 0x2009
	WGL_TRANSPARENT_ARB             = 0x200A
	WGL_TRANSPARENT_RED_VALUE_ARB   = 0x2037
	WGL_TRANSPARENT_GREEN_VALUE_ARB = 0x2038
	WGL_TRANSPARENT_BLUE_VALUE_ARB  = 0x2039
	WGL_TRANSPARENT_ALPHA_VALUE_ARB = 0x203A
	WGL_TRANSPARENT_INDEX_VALUE_ARB = 0x203B
	WGL_SHARE_DEPTH_ARB             = 0x200C
	WGL_SHARE_STENCIL_ARB           = 0x200D
	WGL_SHARE_ACCUM_ARB             = 0x200E
	WGL_SUPPORT_GDI_ARB             = 0x200F
	WGL_SUPPORT_OPENGL_ARB          = 0x2010
	WGL_DOUBLE_BUFFER_ARB           = 0x2011
	WGL_STEREO_ARB                  = 0x2012
	WGL_PIXEL_TYPE_ARB              = 0x2013
	WGL_COLOR_BITS_ARB              = 0x2014
	WGL_RED_BITS_ARB                = 0x2015
	WGL_RED_SHIFT_ARB               = 0x2016
	WGL_GREEN_BITS_ARB              = 0x2017
	WGL_GREEN_SHIFT_ARB             = 0x2018
	WGL_BLUE_BITS_ARB               = 0x2019
	WGL_BLUE_SHIFT_ARB              = 0x201A
	WGL_ALPHA_BITS_ARB              = 0x201B
	WGL_ALPHA_SHIFT_ARB             = 0x201C
	WGL_ACCUM_BITS_ARB              = 0x201D
	WGL_ACCUM_RED_BITS_ARB          = 0x201E
	WGL_ACCUM_GREEN_BITS_ARB        = 0x201F
	WGL_ACCUM_BLUE_BITS_ARB         = 0x2020
	WGL_ACCUM_ALPHA_BITS_ARB        = 0x2021
	WGL_DEPTH_BITS_ARB              = 0x2022
	WGL_STENCIL_BITS_ARB            = 0x2023
	WGL_AUX_BUFFERS_ARB             = 0x2024
	WGL_NO_ACCELERATION_ARB         = 0x2025
	WGL_GENERIC_ACCELERATION_ARB    = 0x2026
	WGL_FULL_ACCELERATION_ARB       = 0x2027
	WGL_SWAP_EXCHANGE_ARB           = 0x2028
	WGL_SWAP_COPY_ARB               = 0x2029
	WGL_SWAP_UNDEFINED_ARB          = 0x202A
	WGL_TYPE_RGBA_ARB               = 0x202B
	WGL_TYPE_COLORINDEX_ARB         = 0x202C
	WGL_SAMPLE_BUFFERS_ARB          = 0x2041
	WGL_SAMPLES_ARB                 = 0x2042
)

// wglCreateContextAttribsARB
const (
	WGL_CONTEXT_DEBUG_BIT_ARB                 = 0x0001
	WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB    = 0x0002
	WGL_CONTEXT_MAJOR_VERSION_ARB             = 0x2091
	WGL_CONTEXT_MINOR_VERSION_ARB             = 0x2092
	WGL_CONTEXT_LAYER_PLANE_ARB               = 0x2093
	WGL_CONTEXT_FLAGS_ARB                     = 0x2094
	WGL_CONTEXT_PROFILE_MASK_ARB              = 0x9126
	WGL_CONTEXT_CORE_PROFILE_BIT_ARB          = 0x0001
	WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB = 0x0002
)

func attrIPtr(v []int32) uintptr {
	if len(v) == 0 {
		return 0
	}

	if len(v) < 1 || v[len(v)-1] != 0 {
		v = append(v, 0)
	}

	return uintptr(unsafe.Pointer(&v[0]))
}

func attrFPtr(v []float32) uintptr {
	if len(v) == 0 {
		return 0
	}

	if len(v) < 1 || v[len(v)-1] != 0 {
		v = append(v, 0)
	}

	return uintptr(unsafe.Pointer(&v[0]))
}

func mustGetProc(name string) uintptr {
	proc := win.WglGetProcAddress(syscall.StringBytePtr(name))
	if proc == 0 {
		panic(fmt.Sprintf("%s is unavailable", name))
	}
	return proc
}

func wglChoosePixelFormatARB(hdc win.HDC, attrI []int32, attrF []float32, formats []int32) []int32 {
	proc := mustGetProc("wglChoosePixelFormatARB")

	if formats == nil {
		formats = make([]int32, 4)
	}

	var numFormats int
	r, _, _ := syscall.Syscall6(
		proc, 6,
		uintptr(hdc),
		attrIPtr(attrI),
		attrFPtr(attrF),
		uintptr(len(formats)),
		uintptr(unsafe.Pointer(&formats[0])),
		uintptr(unsafe.Pointer(&numFormats)),
	)
	if r == 0 {
		return nil
	}

	return formats[:numFormats]
}

func wglGetPixelFormatAttribivARB(hDC win.HDC, pixFmt, layerPlane int32, attrs []int32) []int32 {
	values := make([]int32, len(attrs))
	syscall.Syscall6(
		mustGetProc("wglGetPixelFormatAttribivARB"), 6,
		uintptr(hDC),
		uintptr(pixFmt),
		uintptr(layerPlane),
		uintptr(len(attrs)),
		uintptr(unsafe.Pointer(&attrs[0])),
		uintptr(unsafe.Pointer(&values[0])),
	)
	return values
}

func wglCreateContextAttribsARB(hDC win.HDC, hShareContext win.HGLRC, attr []int32) win.HGLRC {
	proc := mustGetProc("wglCreateContextAttribsARB")
	r, _, _ := syscall.Syscall(
		proc, 3,
		uintptr(hDC),
		uintptr(hShareContext),
		attrIPtr(attr),
	)
	return win.HGLRC(r)
}

func describePixelFormat(hDC win.HDC, pixFmt int32) *win.PIXELFORMATDESCRIPTOR {
	var pfd win.PIXELFORMATDESCRIPTOR
	r, _, _ := describePixelFormatAddr.Call(
		uintptr(hDC),
		uintptr(pixFmt),
		unsafe.Sizeof(pfd),
		uintptr(unsafe.Pointer(&pfd)),
	)
	if r == 0 {
		return nil
	}
	return &pfd
}
