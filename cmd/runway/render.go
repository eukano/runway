//go:build !noegl && linux
// +build !noegl,linux

package main

import (
	"bytes"
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"log"
	"math"
	"os"
	"path/filepath"
	"time"

	"github.com/go-gl/gl/all-core/gl"
	"github.com/icza/mjpeg"
	"github.com/spf13/cobra"
	"gitlab.com/eukano/runway/internal/model"
	"gitlab.com/eukano/runway/internal/ui"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
	"gitlab.com/eukano/runway/script/interpreter"
)

// #cgo pkg-config: egl
// #include <EGL/egl.h>
// #include <EGL/eglext.h>
import "C"

var render = cobra.Command{
	Use: "render [model]",
}

func init() {
	render.Run = renderRun
	cmd.AddCommand(&render)
}

var renderDebug = render.Flags().Bool("gl-debug", false, "print OpenGL debug messages")
var renderWidth = render.Flags().IntP("width", "W", 800, "image width")
var renderHeight = render.Flags().IntP("height", "H", 600, "image height")
var renderPosition = Vec3P(render.Flags(), "position", "P", tensor.Vec3{0, 3, 10}, "camera position")
var renderHeading = Vec2P(render.Flags(), "heading", "D", tensor.Vec2{0, -20}, "camera heading (degrees)")
var renderFOV = render.Flags().Float64P("fov", "V", 45, "field of view (degrees)")
var renderStartTime = render.Flags().DurationP("start-time", "T", 0, "start time for dynamic transforms")
var renderTickTime = render.Flags().Duration("tick-time", time.Second/60, "time per tick, for video formats")
var renderFrameCount = render.Flags().Int("frames", 60, "number of frames to render, for video formats")

var renderFile = render.Flags().String("file", "", "file to write render to")

func renderRun(cmd *cobra.Command, args []string) {
	p := model.Position{Value: *renderPosition}
	o := new(model.Heading).SetDeg(*renderHeading).Orientation()
	p.Update(0, 0, 0, 0, o)

	fov := *renderFOV / 180 * math.Pi
	proj := tensor.Perspective(fov, float64(*renderWidth)/float64(*renderHeight), 0.1, 1000)
	ortho := tensor.Ortho(0, float64(*renderWidth), 0, float64(*renderHeight), -1, 1)

	r := new(ui.Renderer)

	if len(args) == 0 {
		r.Add(ui.DefaultModel)
	} else {
		ok := true
		i := interpreter.New(nil)
		for _, path := range args {
			t := time.Now()
			err := i.EvalPath(path)
			d := time.Now().Sub(t)
			if err == nil {
				fmt.Printf("eval %q took %v\n", path, d)
				continue
			}

			ui.PrintInterpErr(path, err)
			break
		}
		if !ok {
			os.Exit(1)
		}
		for _, m := range i.Models() {
			r.Add(m)
		}
	}

	count := 1
	if *renderFile != "" {
		switch ext := filepath.Ext(*renderFile); ext {
		case ".jpg", ".jpeg":
			r.SetFrameCapture(renderCaptureJPG)
		case ".png":
			r.SetFrameCapture(renderCapturePNG)
		case ".avi":
			count = *renderFrameCount
			w := newAVI()
			defer w.Close()
			r.SetFrameCapture(func(img *image.RGBA) bool { return renderCaptureAVI(w, img) })
		default:
			log.Fatalf("%q is not supported", ext)
		}
	}

	runEgl(func() {
		err := gl.Init()
		if err != nil {
			log.Fatal(err)
		}

		r.EnableDebug(*renderDebug)
		gl.Viewport(0, 0, int32(*renderWidth), int32(*renderHeight))

		err = r.Prepare(true)
		if err != nil {
			log.Fatal(err)
		}

		defer r.Cleanup()

		runtime := *renderStartTime
		r.Tick(runtime, runtime)

		for i := 0; i < count; i++ {
			duration := r.Render(*renderWidth, *renderHeight, model.Globals{
				Flatland: ortho,
				Lights:   ui.DefaultLights,
				World: model.World{
					Projection: proj,
					View:       p.View,
					Camera:     p.Value,
				},
			})

			fmt.Fprintf(os.Stderr, "Frame %d took %v\n", i, duration)

			runtime += *renderTickTime
			r.Tick(*renderTickTime, runtime)
		}
	})

}

func renderCaptureJPG(img *image.RGBA) bool {
	f, err := os.Create(*renderFile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	err = jpeg.Encode(f, img, nil)
	if err != nil {
		log.Fatal(err)
	}
	return false
}

func renderCapturePNG(img *image.RGBA) bool {
	f, err := os.Create(*renderFile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	err = png.Encode(f, img)
	if err != nil {
		log.Fatal(err)
	}
	return false
}

func renderCaptureAVI(w mjpeg.AviWriter, img *image.RGBA) bool {
	var buf bytes.Buffer
	err := jpeg.Encode(&buf, img, nil)
	if err != nil {
		log.Fatal(err)
	}

	err = w.AddFrame(buf.Bytes())
	if err != nil {
		log.Fatal(err)
	}
	return true
}

func newAVI() mjpeg.AviWriter {
	w, err := mjpeg.New(*renderFile, int32(*renderWidth), int32(*renderHeight), int32(time.Second/(*renderTickTime)))
	if err != nil {
		log.Fatal(err)
	}
	return w
}

func eglInit() C.EGLDisplay {
	d := C.eglGetDisplay(C.EGLNativeDisplayType(C.EGL_DEFAULT_DISPLAY))
	if d == 0 {
		log.Fatal("[EGL] failed to get display")
	}

	if C.eglInitialize(d, nil, nil) == 0 {
		log.Fatal("[EGL] failed to initialize")
	}
	return d
}

func eglConfig(d C.EGLDisplay) []C.EGLConfig {
	cfgAttr := [...]C.EGLint{
		C.EGL_RED_SIZE, 1,
		C.EGL_BLUE_SIZE, 1,
		C.EGL_GREEN_SIZE, 1,
		C.EGL_DEPTH_SIZE, 1,

		C.EGL_RENDERABLE_TYPE, C.EGL_OPENGL_BIT,
		C.EGL_SURFACE_TYPE, C.EGL_PBUFFER_BIT,

		C.EGL_NONE,
	}

	const maxConfigs = 10
	var configs [maxConfigs]C.EGLConfig
	var numConfigs C.EGLint
	if C.eglChooseConfig(d, &cfgAttr[0], &configs[0], maxConfigs, &numConfigs) == 0 || numConfigs == 0 {
		log.Fatal("[EGL] failed to chose a config")
	}
	return configs[:numConfigs]
}

func eglContext(d C.EGLDisplay, cfg C.EGLConfig) C.EGLContext {
	ctxAttr := [...]C.EGLint{
		C.EGL_CONTEXT_CLIENT_VERSION, 2,
		C.EGL_NONE,
	}

	ctx := C.eglCreateContext(d, cfg, nil, &ctxAttr[0])
	if ctx == nil {
		log.Fatal("[EGL] failed to create context")
	}
	return ctx
}

func eglSurface(d C.EGLDisplay, cfg C.EGLConfig) C.EGLSurface {
	surfAttr := [...]C.EGLint{
		C.EGL_WIDTH,
		C.int(*renderWidth),
		C.EGL_HEIGHT,
		C.int(*renderHeight),
		C.EGL_NONE,
	}

	surface := C.eglCreatePbufferSurface(d, cfg, &surfAttr[0])
	if surface == nil {
		log.Fatal("[EGL] failed to create pbuffer surface")
	}
	return surface
}

func runEgl(fn func()) {
	d := eglInit()
	defer C.eglTerminate(d)

	cfg := eglConfig(d)
	C.eglBindAPI(C.EGL_OPENGL_API)

	ctx := eglContext(d, cfg[0])
	defer C.eglDestroyContext(d, ctx)

	surface := eglSurface(d, cfg[0])
	defer C.eglDestroySurface(d, surface)

	if C.eglMakeCurrent(d, surface, surface, ctx) == 0 {
		log.Fatal("[EGL] make current failed")
	}

	fn()
}
