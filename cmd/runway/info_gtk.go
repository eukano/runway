//go:build (linux && !nogtk) || gtk
// +build linux,!nogtk gtk

package main

import (
	"fmt"
	"log"

	// specifically import all-core to allow for any version
	"github.com/go-gl/gl/all-core/gl"
	"github.com/gotk3/gotk3/gtk"
	"github.com/spf13/cobra"
)

// #cgo pkg-config: gtk+-3.0
// #include <gtk/gtk.h>
import "C"

var info = cobra.Command{
	Use: "info",
}

func init() {
	info.Run = infoRun
	cmd.AddCommand(&info)
}

func infoRun(cmd *cobra.Command, args []string) {
	gtk.Init(&args)

	win, err := gtk.WindowNew(gtk.WINDOW_TOPLEVEL)
	if err != nil {
		log.Fatal(err)
	}

	gla, err := gtk.GLAreaNew()
	if err != nil {
		log.Fatal(err)
	}
	win.Add(gla)

	gla.SetRequiredVersion(4, 6)

	win.Realize()
	gla.Realize()
	gla.MakeCurrent()

	err = gl.Init()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(gl.GoStr(gl.GetString(gl.RENDERER)))
	fmt.Println(gl.GoStr(gl.GetString(gl.VERSION)))
}
