package libs

import "reflect"

//-go:generate go run github.com/traefik/yaegi/cmd/yaegi extract github.com/go-gl/mathgl/mgl32
//-go:generate go run github.com/traefik/yaegi/cmd/yaegi extract gitlab.com/eukano/runway/internal/mesh

// xSymbols variable stores the map of stdlib symbols per package.
var xSymbols = map[string]map[string]reflect.Value{}

func init() {
	xSymbols["gitlab.com/eukano/runway/internal/libs"] = map[string]reflect.Value{
		"Symbols": reflect.ValueOf(xSymbols),
	}
}
