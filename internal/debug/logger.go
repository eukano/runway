package debug

import (
	"bytes"
	"fmt"
	"log"
)

// A Logger can be used to log debug messages.
type Logger struct {
	Impl LoggerImpl
}

// LoggerImpl implements the necessary methods to log messages.
type LoggerImpl interface {
	Helper()
	Write(string)
}

// line-buffered writer
type lineBufWriter struct {
	ch chan<- string
	b  []byte
}

// implements io.Writer
func (w *lineBufWriter) Write(b []byte) (int, error) {
	var n int
	for {
		i := bytes.IndexByte(b, '\n')
		if i < 0 {
			w.b = append(w.b, b...)
			n += len(b)
			return n, nil
		}

		n += i + 1
		w.b = append(w.b, b[:i]...)
		w.Flush()
		b = b[i+1:]
	}
}

// flush the buffer
func (w *lineBufWriter) Flush() {
	if len(w.b) == 0 {
		return
	}

	w.ch <- string(w.b)
	w.b = w.b[:0]
}

// Log logs a message with fmt.Sprint. This is a nop if Impl is nil.
//go:inline
func (l Logger) Log(v ...interface{}) {
	if l.Impl == nil {
		return
	}

	l.Impl.Helper()
	l.Impl.Write(fmt.Sprint(v...))
}

// Logf logs a message with fmt.Sprintf. This is a nop if Impl is nil.
//go:inline
func (l Logger) Logf(f string, v ...interface{}) {
	if l.Impl == nil {
		return
	}

	l.Impl.Helper()
	l.Impl.Write(fmt.Sprintf(f, v...))
}

// Logfn creates a log.Logger and calls FN with it. This is a nop if Impl is
// nil.
//go:inline
func (l Logger) Logfn(fn func(*log.Logger)) {
	if l.Impl == nil {
		return
	}

	l.Impl.Helper()

	ch := make(chan string)
	go func() {
		defer close(ch)
		lb := lineBufWriter{ch, nil}
		fn(log.New(&lb, "", 0))
		lb.Flush()
	}()

	for b := range ch {
		l.Impl.Write(b)
	}
}
