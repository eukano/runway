package testdbg

import (
	"strings"
	"testing"
)

// Logger implements debug.Logger
type Logger struct {
	testing.TB
}

// Write implements debug.Logger.Write
func (l Logger) Write(s string) {
	if strings.HasSuffix(s, "\n") {
		s = s[:len(s)-1]
	}

	l.Helper()
	l.Log(s)
}
