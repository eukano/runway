//go:build darwin

package cocoaui

import (
	"time"

	"github.com/progrium/macdriver/objc"
)

var timerFuncs bridge

func Tick(d time.Duration, fn func()) objc.Object {
	panic("there be bugs, don't use this")

	timer := objc.Get("RWTimer").Alloc().Init()
	timerFuncs.Store(timer, fn)
	nstimer := objc.Get("NSTimer").Send("scheduledTimerWithTimeInterval:target:selector:userInfo:repeats:",
		d.Seconds(),
		timer,
		objc.Sel("tick"),
		nil,
		true)
	timer.Send("setInternal:", nstimer)
	return timer
}

type Timer struct {
	objc.Object `objc:"RWTimer : NSObject"`
	internal    objc.Object
}

func init() {
	c := objc.NewClassFromStruct(Timer{})
	c.AddMethod("setInternal:", func(t *Timer, o objc.Object) {
		t.internal = o
		o.Retain()
	})
	c.AddMethod("tick", func(t *Timer) {
		fn, _ := timerFuncs.Load(t)
		fn.(func())()
	})
	c.AddMethod("stop", func(t *Timer) {
		t.internal.Send("invalidate")
	})
	c.AddMethod("dealloc", func(t *Timer) {
		t.internal.Release()
		timerFuncs.Delete(t)
		t.SendSuper("dealloc")
	})
	objc.RegisterClass(c)
}
