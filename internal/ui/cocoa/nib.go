//go:build darwin

package cocoaui

import (
	"github.com/progrium/macdriver/cocoa"
	"github.com/progrium/macdriver/core"
	"github.com/progrium/macdriver/objc"
)

func LoadEmbeddedNib(owner objc.Object, data []byte, fn func(objc.Object)) bool {
	nsdata := core.NSData_WithBytes(data, uint64(len(data)))
	bundle := cocoa.NSBundle_Main()
	nib := objc.Get("NSNib").Alloc().Send("initWithNibData:bundle:", nsdata, bundle)

	if fn == nil {
		return nib.Send("instantiateWithOwner:topLevelObjects:", owner, nil).Bool()
	}

	var ptr uintptr
	ok := nib.Send("instantiateWithOwner:topLevelObjects:", owner, &ptr).Bool()
	if !ok {
		return false
	}

	tlo := core.NSArray{Object: objc.ObjectPtr(ptr)}
	for i, n := uint64(0), tlo.Count(); i < n; i++ {
		fn(tlo.ObjectAtIndex(i))
	}

	return true
}
