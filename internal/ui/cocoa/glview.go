//go:build darwin

package cocoaui

import (
	"unsafe"

	"github.com/progrium/macdriver/objc"
)

/*
#cgo CFLAGS: -x objective-c -DGL_SILENCE_DEPRECATION
#cgo LDFLAGS: -framework OpenGL -framework AppKit -framework Foundation
#import "RWOpenGLView.h"
*/
import "C"

//export callGLRenderFunc
func callGLRenderFunc(id unsafe.Pointer) C.bool {
	o := objc.ObjectPtr(uintptr(id))
	fn, ok := glFuncs.Load(o)
	if !ok {
		return false
	}
	return (C.bool)(fn.(GLRenderFunc)(o))
}

//export deleteGLRenderFunc
func deleteGLRenderFunc(id unsafe.Pointer) {
	glFuncs.Delete(objc.ObjectPtr(uintptr(id)))
}

var glFuncs bridge

type GLRenderFunc func(objc.Object) bool

func SetRenderFunc(view objc.Object, fn GLRenderFunc) {
	if view.Class().String() != "RWOpenGLView" {
		panic("view is not a RWOpenGLView")
	}
	glFuncs.Store(view, fn)
}
