//go:build darwin

package cocoaui

import (
	"sync"

	"github.com/progrium/macdriver/objc"
)

type bridge struct {
	m sync.Map
}

func (b *bridge) Load(o objc.Object) (interface{}, bool) {
	return b.m.Load(o.Pointer())
}

func (b *bridge) Store(o objc.Object, v interface{}) {
	b.m.Store(o.Pointer(), v)
}

func (b *bridge) Delete(o objc.Object) {
	b.m.Delete(o.Pointer())
}
