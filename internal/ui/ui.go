package ui

import (
	"fmt"
	"math"
	"strings"
	"time"

	"gitlab.com/eukano/runway/internal/glu"
	"gitlab.com/eukano/runway/internal/model"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
	"golang.org/x/image/font"
)

type UI struct {
	host     Host
	renderer *Renderer

	initCount int
	camera
	font      font.Face
	viewDisp  *model.Text
	timeDisp  *model.Text
	timeTotal time.Duration
	timeCount int
	clearTick func()
}

type Host interface {
	// is debugging enabled?
	DebugEnabled() bool

	// is face culling enabled?
	CullingEnabled() bool

	// viewport size
	Size() (width, height int)

	// trigger a draw
	QueueDraw()

	// add a tick function
	AddTick(func(time.Duration)) (clear func(), err error)

	// get text font
	GetFont() (font.Face, error)

	// lights
	Lights() []model.Light
}

type camera struct {
	move struct {
		heading struct {
			v, h model.Movement
		}
		position struct {
			right, up, look model.Movement
		}
	}

	heading  model.Heading
	position model.Position
}

type Key int

const (
	KeyNone Key = iota
	KeyUp
	KeyDown
	KeyRight
	KeyLeft
	KeyW
	KeyS
	KeyA
	KeyD
	KeyQ
	KeyE
)

func New(host Host) *UI {
	return &UI{
		host:     host,
		renderer: new(Renderer),
	}
}

func (ui *UI) AddModel(m model.Model, t ...model.Transform) {
	ui.renderer.Add(m, t...)
}

func (ui *UI) SetFrameCapture(fn FrameCapture) bool {
	return ui.renderer.SetFrameCapture(fn)
}

func (ui *UI) Event(key Key, press bool) {
	dir := model.Forward
	if !press {
		dir = model.None
	}

	switch key {
	case KeyUp:
		ui.move.heading.v = +dir
	case KeyDown:
		ui.move.heading.v = -dir
	case KeyRight:
		ui.move.heading.h = -dir
	case KeyLeft:
		ui.move.heading.h = +dir
	case KeyW:
		ui.move.position.look = +dir
	case KeyS:
		ui.move.position.look = -dir
	case KeyA:
		ui.move.position.right = -dir
	case KeyD:
		ui.move.position.right = +dir
	case KeyQ:
		ui.move.position.up = +dir
	case KeyE:
		ui.move.position.up = -dir
	}
}

func (ui *UI) Prepare() (err error) {
	ui.initCount++
	if ui.initCount == 1 {
		ui.heading.RadPerSec.H = HorizRadPerSec
		ui.heading.RadPerSec.V = VertRadPerSec
		ui.position.MovePerSec = MovePerSec
		ui.position.Value = tensor.Vec3{0, 3, 10}
		ui.heading.Vertical = -math.Pi / 9

		font, err := ui.host.GetFont()
		if err != nil {
			return err
		} else {
			ui.font = font
		}
	}

	ui.timeDisp = &model.Text{
		Color: glu.Color.W.Vec4(1),
		Pos:   tensor.Vec2{5, 5},
		Size:  0.5,
	}
	ui.viewDisp = &model.Text{
		Value: "hi",
		Color: glu.Color.W.Vec4(1),
		Pos:   tensor.Vec2{5},
		Size:  0.5,
	}

	if ui.font != nil {
		ui.timeDisp.Font = ui.font
		ui.viewDisp.Font = ui.font
		ui.AddModel(ui.timeDisp)
		ui.AddModel(ui.viewDisp)
	}

	if ui.host.DebugEnabled() {
		err := ui.renderer.Init()
		if err != nil {
			return err
		}

		ui.renderer.EnableDebug(true)
	}

	err = ui.renderer.Prepare(ui.host.CullingEnabled())
	if err != nil {
		return err
	}

	var last time.Duration
	ui.clearTick, err = ui.host.AddTick(func(runtime time.Duration) {
		delta := runtime - last
		last = runtime

		if ui.tick(delta, runtime) {
			ui.host.QueueDraw()
		}
	})
	if err != nil {
		return err
	}

	return nil
}

func (ui *UI) Cleanup() error {
	if ui.clearTick != nil {
		ui.clearTick()
	}

	ui.renderer.Cleanup()
	return nil
}

func (ui *UI) tick(delta, runtime time.Duration) bool {
	mh, mp := ui.move.heading, ui.move.position
	ui.heading.Update(delta, mh.v, mh.h)
	o := ui.heading.Orientation()
	ui.position.Update(delta, mp.right, mp.up, mp.look, o)

	moving := mh.v != 0 || mh.h != 0 || mp.right != 0 || mp.up != 0 || mp.look != 0
	tick := ui.renderer.Tick(delta, runtime)
	return moving || tick
}

func (ui *UI) Render() error {
	const fov = math.Pi / 4
	width, height := ui.host.Size()
	ui.viewDisp.Pos[1] = float64(height) - 5 - 12
	ui.viewDisp.Value = fmt.Sprintf("eye: %v, v: %.2f, h: %.2f", ui.position.Value, ui.heading.Vertical*180/math.Pi, ui.heading.Horizontal*180/math.Pi)

	g := model.Globals{
		Flatland: tensor.Ortho(0, float64(width), 0, float64(height), -1, 1),
		Lights:   ui.host.Lights(),
		World: model.World{
			Projection: tensor.Perspective(fov, float64(width)/float64(height), 0.1, 1000),
			View:       ui.position.View,
			Camera:     ui.position.Value,
		},
	}

	t := ui.renderer.Render(width, height, g)
	ui.timeTotal += t
	ui.timeCount++

	if ui.timeCount >= avgFrameTimeWindow {
		average := ui.timeTotal / avgFrameTimeWindow
		ui.timeTotal, ui.timeCount = 0, 0
		ui.timeDisp.Value = strings.ReplaceAll(average.String(), "µ", "u")
	}

	return nil
}
