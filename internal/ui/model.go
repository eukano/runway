package ui

import (
	"gitlab.com/eukano/runway/internal/mesh"
	"gitlab.com/eukano/runway/internal/model"
	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/shape"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

func Cube() *model.Mesh {
	const v = 0.5
	return model.NewShape("Cube", shape.Cube(-v, +v, -v, +v, -v, +v), mesh.NormalFromFace, nil)
}

func Sphere() *model.Mesh {
	return model.NewShape("Sphere", shape.Sphere(1, tensor.Vec3{}), mesh.NormalFromCenter, nil)
}

func Cylinder() *model.Mesh {
	return model.NewShape("Cylinder", shape.Cylinder(tensor.Vec3{}, tensor.Vec3{0, 0, 1}), mesh.NormalFromAdjacent, nil)
}

func Torus(r1, r2 float64) *model.Mesh {
	return model.NewShape("Torus", shape.Torus(r1, r2), mesh.NormalFromAdjacent, nil)
}

func Extrude(profile manifold.Polygon, path tensor.Vec3) *model.Mesh {
	return model.NewShape("Extrusion", shape.Extrude(profile, path), mesh.NormalFromAdjacent, nil)
}

func Revolve(profile manifold.Polygon, axis tensor.Vec3, resolution int) *model.Mesh {
	return model.NewShape("Revolution", shape.Revolve(profile, axis, resolution), mesh.NormalFromAdjacent, nil)
}

func Scale(x, y, z float64) model.Transform {
	return model.Static(shape.Scale(x, y, z))
}

func ScaleV(v tensor.Vec3) model.Transform {
	return model.Static(shape.ScaleV(v))
}

func ScaleUniform(v float64) model.Transform {
	return model.Static(shape.ScaleUniform(v))
}

func Translate(x, y, z float64) model.Transform {
	return model.Static(shape.Translate(x, y, z))
}

func TranslateV(v tensor.Vec3) model.Transform {
	return model.Static(shape.TranslateV(v))
}

func Rotate(angle float64, axis tensor.Vec3) model.Transform {
	return model.Static(shape.Rotate(angle, axis))
}

func Spin(rate float64, axis tensor.Vec3) model.Transform {
	return model.Dynamic(shape.Spin(rate, axis))
}
