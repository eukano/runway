package ui

import (
	"fmt"
	"image"
	"log"
	"math"
	"os"
	"runtime/debug"
	"sync"
	"time"
	"unsafe"

	"github.com/go-gl/gl/all-core/gl"
	"gitlab.com/eukano/runway/internal/glu"
	"gitlab.com/eukano/runway/internal/model"
)

const VertRadPerSec = math.Pi / 2
const HorizRadPerSec = math.Pi / 2
const MovePerSec = 10

const avgFrameTimeWindow = 60 * 2

type FrameCapture func(*image.RGBA) bool

type Renderer struct {
	frameCapture FrameCapture

	rendering sync.Mutex
	cleanup   []func()
	models    []model.Model
	queries   []glu.Ref
}

func (r *Renderer) SetFrameCapture(fn FrameCapture) bool {
	r.rendering.Lock()
	defer r.rendering.Unlock()

	if r.frameCapture != nil {
		return false
	}

	r.frameCapture = fn
	return true
}

func (r *Renderer) Add(m model.Model, t ...model.Transform) {
	for _, t := range t {
		m = m.With(t)
	}
	r.models = append(r.models, m)
}

func (r *Renderer) addCleanup(c func()) {
	r.cleanup = append(r.cleanup, c)
}

func (r *Renderer) EnableDebug(ok bool) {
	if ok {
		gl.Enable(gl.DEBUG_OUTPUT)
		gl.Enable(gl.DEBUG_OUTPUT_SYNCHRONOUS)
	} else {
		gl.Disable(gl.DEBUG_OUTPUT)
		gl.Disable(gl.DEBUG_OUTPUT_SYNCHRONOUS)
	}
}

func (r *Renderer) debug(source, gltype, id, severity uint32, length int32, message string, _ unsafe.Pointer) {
	log.Printf("[%v-%v] %s {%v}", glu.DbgSev(severity), glu.DbgSrc(source), message, glu.DbgTyp(gltype))
	fmt.Fprintf(os.Stderr, "%s\n", debug.Stack())
}

func (r *Renderer) Init() error {
	return gl.Init()
}

func (r *Renderer) Prepare(cull bool) error {
	r.rendering.Lock()
	defer r.rendering.Unlock()

	err := r.Init()
	if err != nil {
		return fmt.Errorf("failed to load OpenGL: missing %w", err)
	}

	if !glu.VersionAtLeast(4, 1) {
		m, n := glu.Version()
		return fmt.Errorf("expected OpenGL version ≥ 4.1, got %d.%d", m, n)
	}

	if glu.VersionAtLeast(4, 3) {
		gl.DebugMessageCallback(r.debug, nil)
	}

	gl.Enable(gl.DEPTH_TEST)
	gl.DepthFunc(gl.LESS)

	gl.FrontFace(gl.CW)
	gl.CullFace(gl.BACK)
	if cull {
		gl.Enable(gl.CULL_FACE)
	} else {
		gl.Disable(gl.CULL_FACE)
	}

	gl.Enable(gl.BLEND)
	gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)

	r.queries = glu.Gen(2, gl.GenQueries, 0, "failed to create queries")

	for _, m := range r.models {
		err = m.Realize(r.addCleanup)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *Renderer) Cleanup() {
	for i := len(r.cleanup) - 1; i >= 0; i-- {
		r.cleanup[i]()
	}
	r.cleanup = nil
	r.models = nil
}

func (r *Renderer) Tick(delta, runtime time.Duration) bool {
	var draw bool
	for _, m := range r.models {
		if m.Tick(delta, runtime) {
			draw = true
		}
	}
	return draw
}

func (r *Renderer) Render(width, height int, g model.Globals) time.Duration {
	r.rendering.Lock()
	defer r.rendering.Unlock()

	gl.QueryCounter(r.queries[0].ID(), gl.TIMESTAMP)

	gl.ClearColor(0.6, 0.8, 1.0, 1.0)
	gl.ClearDepth(1)
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	for _, m := range r.models {
		m.Draw(g)
	}

	gl.QueryCounter(r.queries[1].ID(), gl.TIMESTAMP)
	gl.Flush()

	var v int32
	for v == 0 {
		gl.GetQueryObjectiv(r.queries[1].ID(), gl.QUERY_RESULT_AVAILABLE, &v)
	}

	var start, stop uint64
	gl.GetQueryObjectui64v(r.queries[0].ID(), gl.QUERY_RESULT, &start)
	gl.GetQueryObjectui64v(r.queries[1].ID(), gl.QUERY_RESULT, &stop)
	duration := time.Duration(stop-start) * time.Nanosecond

	if r.frameCapture == nil {
		return duration
	}

	buf := make([]uint8, width*height*4)
	gl.ReadPixels(0, 0, int32(width), int32(height), gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(&buf[0]))

	img := image.NewRGBA(image.Rect(0, 0, width, height))
	stride := width * 4
	for i := 0; i < height; i++ {
		src := buf[i*stride : (i+1)*stride]
		dst := img.Pix[(height-i-1)*stride : (height-i)*stride]
		copy(dst, src)
	}

	if !r.frameCapture(img) {
		r.frameCapture = nil
	}

	return duration
}
