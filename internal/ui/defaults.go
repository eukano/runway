package ui

import (
	"math"

	"gitlab.com/eukano/runway/internal/glu"
	"gitlab.com/eukano/runway/internal/model"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

var spin = Spin(math.Pi/2, tensor.Vec3{0, 1, 0})
var move = Translate(3, 0, 0)

var DefaultLights = []model.Light{
	{
		Position: tensor.Vec3{6, 5, 7},
		Ambient:  glu.Color.W.Mul(0.6),
		Diffuse:  glu.Color.W.Mul(0.5),
		Specular: glu.Color.W.Mul(0.1),
	},
}

var Skybox = new(model.Skybox).With(ScaleUniform(100))

var DefaultModel = model.Body{
	Skybox,
	Cube().With(Spin(math.Pi/2, tensor.Vec3{1, 1, 1}.Normalize()), move, spin),
	Cylinder().With(Spin(-math.Pi/2, tensor.Vec3{1, 0, 0}), move, Rotate(math.Pi*2/3, tensor.Vec3{0, 1, 0}), spin),
	Sphere().With(ScaleUniform(0.5), move, Rotate(math.Pi*4/3, tensor.Vec3{0, 1, 0}), spin),
	Torus(0.15, 0.4).With(Spin(math.Pi/2, tensor.Vec3{1, 0, 0}), Spin(math.Pi/3, tensor.Vec3{0, 0, 1})),

	// model.Illuminati(),
	// testText,
}
