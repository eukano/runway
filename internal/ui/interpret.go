package ui

import (
	"errors"
	"fmt"
	"go/scanner"
	"os"
	"time"

	"github.com/traefik/yaegi/interp"
	"gitlab.com/eukano/runway/internal/model"
	"gitlab.com/eukano/runway/script/interpreter"
)

func InterpretModels(paths []string) ([]model.Model, error) {
	var err error
	var i = interpreter.New(nil)
	for _, path := range paths {
		t := time.Now()
		err = i.EvalPath(path)
		d := time.Now().Sub(t)
		if err == nil {
			fmt.Printf("eval %q took %v\n", path, d)
			continue
		}

		PrintInterpErr(path, err)
		err = fmt.Errorf("eval %q: %w", path, err)
		break
	}
	if err != nil {
		return nil, err
	}

	return i.Models(), nil
}

func PrintInterpErr(path string, err error) {
	var errl scanner.ErrorList
	var errp interp.Panic
	if errors.As(err, &errl) {
		for _, err := range errl {
			fmt.Fprintf(os.Stderr, "%s:%d:%d: %s\n", path, err.Pos.Line, err.Pos.Line, err.Msg)
		}
	} else if errors.As(err, &errp) {
		fmt.Fprintf(os.Stderr, "%s: %v\n", path, errp.Value)
		fmt.Fprintf(os.Stderr, "%s", errp.Stack)
	} else {
		fmt.Fprintf(os.Stderr, "%s: %v\n", path, err)
	}
}
