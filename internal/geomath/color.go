package geomath

import (
	"math"

	. "gitlab.com/eukano/runway/pkg/geom/tensor"
)

// HSL2RGB converts from HSL color space to RGB.
func HSL2RGB(H, S, L float64) Vec3 {
	H *= 6
	C := (1 - math.Abs(2*L-1)) * S
	X := C * math.Pow(1-math.Abs(math.Mod(H, 2)-1), 0.75)
	m := L - C/2

	var R, G, B float64
	switch int(H) {
	case -1, 0:
		R, G, B = C, X, 0
	case 1:
		R, G, B = X, C, 0
	case 2:
		R, G, B = 0, C, X
	case 3:
		R, G, B = 0, X, C
	case 4:
		R, G, B = X, 0, C
	default:
		R, G, B = C, 0, X
	}

	return Vec3{R + m, G + m, B + m}
}
