package geomath

// Index is an index into a vertex array.
type Index = uint16

// A TriI is a set of vertex indicies that represent a triangle.
type TriI = [3]Index

// A Mesh is a set of vertex index triplets that represent triangles.
type Mesh []TriI

// Append appends index triplets
func (m *Mesh) Append(n ...TriI) {
	*m = append(*m, n...)
}

// Add adds an index triplet
func (m *Mesh) Add(i, j, k int) {
	m.Append(TriI{Index(i), Index(j), Index(k)})
}

// AddAt adds an offset index triplet
func (m *Mesh) AddAt(offset, i, j, k int) {
	m.Add(offset+i, offset+j, offset+k)
}
