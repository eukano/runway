#version 330 core

in vec3 position;

uniform samplerCube uSampler;

out vec4 fragColor;

void main(void) {
    fragColor = texture(uSampler, position);
}
