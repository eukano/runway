package assets_test

import (
	"log"

	"gitlab.com/eukano/runway/internal/assets"
	"gitlab.com/eukano/runway/internal/glu"
)

func ExampleShaders() {
	prog, err := assets.Shaders(map[glu.ShaderType]string{
		glu.VertexShader:   "mesh.vert",
		glu.FragmentShader: "mesh.frag",
	})
	if err != nil {
		log.Fatal(err)
	}
	_ = prog
}

func ExampleCubeMap() {
	tex, err := assets.CubeMap(map[glu.TextureTarget]string{
		glu.TextureCubeMapPosX: "right.jpg",
		glu.TextureCubeMapNegX: "left.jpg",
		glu.TextureCubeMapPosY: "top.jpg",
		glu.TextureCubeMapNegY: "bottom.jpg",
		glu.TextureCubeMapPosZ: "front.jpg",
		glu.TextureCubeMapNegZ: "back.jpg",
	})
	if err != nil {
		log.Fatal(err)
	}
	_ = tex
}

func ExampleLoad() {
	var model struct {
		program  glu.Program `vert:"mesh.vert" frag:"mesh.frag"`
		position glu.VertexAttrib
		color    glu.VertexAttrib
		normal   glu.VertexAttrib
		specular glu.Uniform
		ambient  glu.Uniform
		diffuse  glu.Uniform
	}

	cleanup := func(fn func()) {
		// call fn to cleanup allocated resources
	}

	err := assets.Load(&model, cleanup)
	if err != nil {
		log.Fatal(err)
	}
}
