#version 330 core

in vec4 avPosition;

uniform mat4 uxModelView;
uniform mat4 uxProjection;

out vec3 position;

void main(void) {
    gl_Position = uxProjection * uxModelView * avPosition;
    position = vec3(avPosition);
}
