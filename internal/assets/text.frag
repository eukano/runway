#version 330 core

in vec2 glyphPos;

uniform sampler2D text;
uniform vec4 color;

out vec4 fragColor;

void main()
{
    vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, glyphPos).r);
    fragColor = color * sampled;
}