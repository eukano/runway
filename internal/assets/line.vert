#version 330 core

in vec3 position;
in vec4 color;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out VS_OUT {
    vec3 position;
    vec4 color;
} frag;

void main(void) {
    gl_Position = projection * view * model * vec4(position, 1.0);

    frag.position = position;
    frag.color = color;
}
