#version 330 core
#define MAX_LIGHTS 10

uniform vec3 viewPosition;
uniform int numLights;
uniform vec3 lightPosition [MAX_LIGHTS];
uniform vec3 ambient [MAX_LIGHTS];
uniform vec3 diffuse [MAX_LIGHTS];
uniform vec3 specular [MAX_LIGHTS];

in VS_OUT {
    vec3 position;
    vec3 normal;
    vec4 color;
} vert;

float shininess = 32;

out vec4 fragColor;

void main(void) {
    vec3 lighting = vec3(0.0);

    for(int i = 0; i < numLights; i++) {
        vec3 lightDir = normalize(lightPosition[i] - vert.position);
        float magDiff = max(dot(vert.normal, lightDir), 0.0);

        vec3 viewDir = normalize(viewPosition - vert.position);
        vec3 reflectDir = reflect(-lightDir, vert.normal);
        float magSpec = pow(max(dot(viewDir, reflectDir), 0.0), shininess);

        lighting += ambient[i] + diffuse[i] * magDiff + specular[i] * magSpec;
    }

    fragColor = vec4(vert.color.rgb * lighting, vert.color.a);
}