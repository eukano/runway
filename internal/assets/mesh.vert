#version 330 core

in vec3 position;
in vec4 color;
in vec3 normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 normalAdj;

out VS_OUT {
    vec3 position;
    vec3 normal;
    vec4 color;
} frag;

void main(void) {
    gl_Position = projection * view * model * vec4(position, 1.0);

    frag.position = position;
    frag.normal = mat3(normalAdj) * normalize(normal);
    frag.color = color;
}
