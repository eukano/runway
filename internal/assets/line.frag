#version 330 core
#define MAX_LIGHTS 10

uniform int numLights;
uniform vec3 ambient [MAX_LIGHTS];

in VS_OUT {
    vec3 position;
    vec4 color;
} vert;

out vec4 fragColor;

void main(void) {
    vec3 lighting = vec3(0.0);

    for(int i = 0; i < numLights; i++) {
        lighting += ambient[i];
    }

    fragColor = vec4(vert.color.rgb * lighting, vert.color.a);
}