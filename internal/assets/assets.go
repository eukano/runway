// Package assets embeds shaders and textures, and constructs programs and
// textures from those embedded assets.
package assets

import (
	"embed"
	"fmt"
	"image"
	"io/ioutil"
	"reflect"
	"strings"
	"unsafe"

	"github.com/go-gl/gl/all-core/gl"
	"gitlab.com/eukano/runway/internal/glu"
)

//go:embed *.frag *.vert *.jpg
var files embed.FS

// Shaders loads the named shaders from embedded assets and compiles and links
// them into a program.
func Shaders(names map[glu.ShaderType]string) (glu.Program, error) {
	prog := glu.NewProgram()

	for typ, name := range names {
		f, err := files.Open(name)
		if err != nil {
			return glu.Program{}, fmt.Errorf("failed to open shader %q: %w", name, err)
		}
		defer f.Close()

		b, err := ioutil.ReadAll(f)
		if err != nil {
			return glu.Program{}, fmt.Errorf("failed to read shader %q: %w", name, err)
		}

		s := typ.New()
		defer s.Delete()

		err = s.Compile(string(b))
		if err != nil {
			return glu.Program{}, fmt.Errorf("failed to compile shader %q: %w", name, err)
		}

		prog.AttachShaders(s)
	}

	err := prog.Link()
	if err != nil {
		return glu.Program{}, err
	}

	return prog, nil
}

// CubeMap loads the named images from embedded assets and constructs a cubemap.
func CubeMap(names map[glu.TextureTarget]string) (glu.Texture, error) {
	const level = 0
	const internalFormat = gl.RGBA
	const width = 1
	const height = 1
	const border = 0
	const srcFormat = gl.RGBA
	const srcType = gl.UNSIGNED_BYTE

	tex := glu.TextureCubeMap.New()
	gl.ActiveTexture(gl.TEXTURE0)
	tex.Bind()
	defer tex.Unbind()

	glu.TextureCubeMap.Parameteri(gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	glu.TextureCubeMap.Parameteri(gl.TEXTURE_MIN_FILTER, gl.LINEAR)

	for typ, name := range names {
		f, err := files.Open(name)
		if err != nil {
			return glu.Texture{}, fmt.Errorf("failed to open %q: %w", name, err)
		}
		defer f.Close()

		img, _, err := image.Decode(f)
		if err != nil {
			return glu.Texture{}, fmt.Errorf("failed to read %q: %w", name, err)
		}

		typ.LoadImage2D(img, 0, gl.SRGB_ALPHA, 0)
	}

	return tex, nil
}

// Load loads programs from embedded assets based on struct tags.
//
// V must be a pointer to a struct. Each glu.Program field of V with one or more
// appropriate struct tags will be compiled. Valid program struct tags are vert,
// geom, and frag, and must refer to an embedded vertex, geometry, or fragment
// shader, respectively.
//
// If V has glu.VertexAttrib or glu.Uniform fields, those will be loaded. By
// default, the field name is used as the attribute or uniform name. The default
// name can be overridden via a struct tag, such as `prog:"newName"`. If V has a
// single program, attributes and uniforms will be loaded from that program. If
// V has multiple programs, each attribute or uniform must specify which program
// it should be loaded from via a struct tag, such as `prog:"@myProg"`. To
// override the default name and specify a program, use a struct tag such as
// `prog:"newName@myProg"`.
func Load(v interface{}, cleanup func(func())) error {
	progs := map[string]glu.Program{}
	rv := reflect.ValueOf(v)

	if rv.Kind() != reflect.Ptr || rv.Elem().Kind() != reflect.Struct {
		panic("value must be a struct pointer")
	}

	var prog glu.Program
	var err error
	forFieldOfType(rv.Elem(), glu.Program{}, func(sf reflect.StructField, v reflect.Value) (stop bool) {
		shaders := map[glu.ShaderType]string{}
		if name, ok := sf.Tag.Lookup("vert"); ok {
			shaders[glu.VertexShader] = name
		}
		if name, ok := sf.Tag.Lookup("geom"); ok {
			shaders[glu.GeometryShader] = name
		}
		if name, ok := sf.Tag.Lookup("frag"); ok {
			shaders[glu.FragmentShader] = name
		}

		prog, err = Shaders(shaders)
		if err != nil {
			return true
		}
		cleanup(prog.Delete)

		v.Set(reflect.ValueOf(prog))
		progs[sf.Name] = prog
		return false
	})
	if err != nil {
		return err
	}
	if len(progs) == 0 {
		panic("no programs!")
	}

	sprog := len(progs) == 1

	var ok bool
	set := func(sf reflect.StructField, v reflect.Value, get func(glu.Program, string) interface{}) bool {
		tag := strings.SplitN(sf.Tag.Get("prog"), "@", 2)
		if len(tag) == 2 {
			prog, ok = progs[tag[1]]
			if !ok {
				err = fmt.Errorf("unknown program %q", tag[1])
				return true
			}
		} else if !sprog {
			err = fmt.Errorf("field %q does not specify which program to use", sf.Name)
			return true
		}

		name := tag[0]
		if name == "" {
			name = sf.Name
		}

		x := get(prog, name)
		v.Set(reflect.ValueOf(x))
		return false
	}

	forFieldOfType(rv.Elem(), glu.VertexAttrib{}, func(sf reflect.StructField, v reflect.Value) (stop bool) {
		return set(sf, v, func(p glu.Program, s string) interface{} {
			return p.GetAttrib(s)
		})
	})
	if err != nil {
		return err
	}

	forFieldOfType(rv.Elem(), glu.Uniform{}, func(sf reflect.StructField, v reflect.Value) (stop bool) {
		return set(sf, v, func(p glu.Program, s string) interface{} {
			return p.GetUniform(s)
		})
	})
	return err
}

// forFieldOfType calls FN for each field in V of type TI until FN returns
// false.
func forFieldOfType(v reflect.Value, ti interface{}, fn func(reflect.StructField, reflect.Value) (stop bool)) {
	t := reflect.TypeOf(ti)
	vt := v.Type()
	n := vt.NumField()
	for i := 0; i < n; i++ {
		f := vt.Field(i)
		if !f.Type.AssignableTo(t) {
			continue
		}

		fv := v.Field(i)
		if !fv.CanSet() {
			fv = reflect.NewAt(f.Type, unsafe.Pointer(fv.UnsafeAddr())).Elem()
		}

		if fn(f, fv) {
			break
		}
	}
}
