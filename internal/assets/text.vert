
#version 330 core

in vec4 data;
out vec2 glyphPos;

uniform mat4 projection;
uniform mat4 model;

void main()
{
    gl_Position = projection * model * vec4(data.xy, 0, 1);
    // glyphPos = vec2(data.x, 1-data.y);
    glyphPos = data.zw;
}