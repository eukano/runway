package glu

import (
	"github.com/go-gl/gl/all-core/gl"
)

func NewProgram() Program {
	p := Program{NewRef(gl.CreateProgram(), 1, "could not create program")}
	debugLifecycle.Log("created program", p.ID())
	return p
}

func (p Program) InfoLog() string { return getstr(p, gl.INFO_LOG_LENGTH, gl.GetProgramInfoLog) }

func (p Program) AttachShaders(s ...Shader) {
	for _, s := range s {
		gl.AttachShader(p.ID(), s.ID())
	}
}

func (p Program) Link() error {
	gl.LinkProgram(p.ID())

	return check(p, gl.LINK_STATUS, "failed to link program: %v")
}

func (p Program) Delete() {
	gl.DeleteProgram(p.ID())
	debugLifecycle.Log("deleted program", p.ID())
}

func (p Program) Use()   { gl.UseProgram(p.ID()) }
func (_ Program) Unuse() { gl.UseProgram(0) }

func (p Program) GetAttrib(name string) VertexAttrib {
	nullTerminate(&name)
	return VertexAttrib{Ref(gl.GetAttribLocation(p.ID(), gl.Str(name)))}
}

func (p Program) GetUniform(name string) Uniform {
	nullTerminate(&name)
	return Uniform{Ref(gl.GetUniformLocation(p.ID(), gl.Str(name)))}
}
