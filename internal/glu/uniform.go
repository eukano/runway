package glu

import (
	"github.com/go-gl/gl/all-core/gl"
	"github.com/go-gl/mathgl/mgl32"
)

func (u Uniform) As1f(a float32)          { gl.Uniform1f(u.SID(), a) }
func (u Uniform) As2f(a, b float32)       { gl.Uniform2f(u.SID(), a, b) }
func (u Uniform) As3f(a, b, c float32)    { gl.Uniform3f(u.SID(), a, b, c) }
func (u Uniform) As4f(a, b, c, d float32) { gl.Uniform4f(u.SID(), a, b, c, d) }
func (u Uniform) As1i(a int32)            { gl.Uniform1i(u.SID(), a) }
func (u Uniform) As2i(a, b int32)         { gl.Uniform2i(u.SID(), a, b) }
func (u Uniform) As3i(a, b, c int32)      { gl.Uniform3i(u.SID(), a, b, c) }
func (u Uniform) As4i(a, b, c, d int32)   { gl.Uniform4i(u.SID(), a, b, c, d) }
func (u Uniform) As1ui(a uint32)          { gl.Uniform1ui(u.SID(), a) }
func (u Uniform) As2ui(a, b uint32)       { gl.Uniform2ui(u.SID(), a, b) }
func (u Uniform) As3ui(a, b, c uint32)    { gl.Uniform3ui(u.SID(), a, b, c) }
func (u Uniform) As4ui(a, b, c, d uint32) { gl.Uniform4ui(u.SID(), a, b, c, d) }
func (u Uniform) As1fv(v ...float32)      { gl.Uniform1fv(u.SID(), int32(len(v)), &v[0]) }
func (u Uniform) As2fv(v ...mgl32.Vec2)   { gl.Uniform2fv(u.SID(), int32(len(v)), &v[0][0]) }
func (u Uniform) As3fv(v ...mgl32.Vec3)   { gl.Uniform3fv(u.SID(), int32(len(v)), &v[0][0]) }
func (u Uniform) As4fv(v ...mgl32.Vec4)   { gl.Uniform4fv(u.SID(), int32(len(v)), &v[0][0]) }
func (u Uniform) As1iv(v ...int32)        { gl.Uniform1iv(u.SID(), int32(len(v)), &v[0]) }
func (u Uniform) As2iv(v ...[2]int32)     { gl.Uniform2iv(u.SID(), int32(len(v)), &v[0][0]) }
func (u Uniform) As3iv(v ...[3]int32)     { gl.Uniform3iv(u.SID(), int32(len(v)), &v[0][0]) }
func (u Uniform) As4iv(v ...[4]int32)     { gl.Uniform4iv(u.SID(), int32(len(v)), &v[0][0]) }
func (u Uniform) As1uiv(v ...uint32)      { gl.Uniform1uiv(u.SID(), int32(len(v)), &v[0]) }
func (u Uniform) As2uiv(v ...[2]uint32)   { gl.Uniform2uiv(u.SID(), int32(len(v)), &v[0][0]) }
func (u Uniform) As3uiv(v ...[3]uint32)   { gl.Uniform3uiv(u.SID(), int32(len(v)), &v[0][0]) }
func (u Uniform) As4uiv(v ...[4]uint32)   { gl.Uniform4uiv(u.SID(), int32(len(v)), &v[0][0]) }

func (u Uniform) AsMatrix2fv(transpose bool, v ...mgl32.Mat2) {
	gl.UniformMatrix2fv(u.SID(), int32(len(v)), transpose, &v[0][0])
}

func (u Uniform) AsMatrix3fv(transpose bool, v ...mgl32.Mat3) {
	gl.UniformMatrix3fv(u.SID(), int32(len(v)), transpose, &v[0][0])
}

func (u Uniform) AsMatrix4fv(transpose bool, v ...mgl32.Mat4) {
	gl.UniformMatrix4fv(u.SID(), int32(len(v)), transpose, &v[0][0])
}

func (u Uniform) AsMatrix2x3fv(transpose bool, v ...mgl32.Mat2x3) {
	gl.UniformMatrix2x3fv(u.SID(), int32(len(v)), transpose, &v[0][0])
}

func (u Uniform) AsMatrix3x2fv(transpose bool, v ...mgl32.Mat3x2) {
	gl.UniformMatrix3x2fv(u.SID(), int32(len(v)), transpose, &v[0][0])
}

func (u Uniform) AsMatrix2x4fv(transpose bool, v ...mgl32.Mat2x4) {
	gl.UniformMatrix2x4fv(u.SID(), int32(len(v)), transpose, &v[0][0])
}

func (u Uniform) AsMatrix4x2fv(transpose bool, v ...mgl32.Mat4x2) {
	gl.UniformMatrix4x2fv(u.SID(), int32(len(v)), transpose, &v[0][0])
}

func (u Uniform) AsMatrix3x4fv(transpose bool, v ...mgl32.Mat3x4) {
	gl.UniformMatrix3x4fv(u.SID(), int32(len(v)), transpose, &v[0][0])
}

func (u Uniform) AsMatrix4x3fv(transpose bool, v ...mgl32.Mat4x3) {
	gl.UniformMatrix4x3fv(u.SID(), int32(len(v)), transpose, &v[0][0])
}
