package glu

func NewRef(v uint32, skip int, msg string, args ...interface{}) Ref {
	id := Ref(v)
	id.check(skip+1, msg, args...)
	return id
}

func Gen(n int, fn func(int32, *uint32), skip int, msg string, args ...interface{}) []Ref {
	ids := make([]Ref, n)
	fn(int32(n), (*uint32)(&ids[0]))

	for _, id := range ids {
		id.check(skip+1, msg, args...)
	}
	return ids
}

func GenT(target uint32, n int, fn func(uint32, int32, *uint32), skip int, msg string, args ...interface{}) []Ref {
	return Gen(n, func(n int32, ptr *uint32) { fn(target, n, ptr) }, skip+1, msg, args...)
}
