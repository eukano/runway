package glu

import (
	"github.com/go-gl/gl/all-core/gl"
)

type Ref uint32

func (r Ref) ID() uint32    { return uint32(r) }
func (r Ref) SID() int32    { return int32(r) }
func (r *Ref) ptr() *uint32 { return (*uint32)(r) }

type Program struct{ Ref }
type Shader struct{ Ref }
type Uniform struct{ Ref }
type VertexArray struct{ Ref }
type VertexAttrib struct{ Ref }

func (p Program) Getiv(name uint32) (v int32) { return getiv(p, name, gl.GetProgramiv) }
func (s Shader) Getiv(name uint32) (v int32)  { return getiv(s, name, gl.GetShaderiv) }
