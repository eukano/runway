package glu

import (
	"github.com/go-gl/gl/all-core/gl"
)

type ShaderType uint32

func (s ShaderType) ID() uint32 { return uint32(s) }

func (s ShaderType) New() Shader {
	return Shader{NewRef(gl.CreateShader(s.ID()), 1, "could not create shader")}
}

func (s Shader) InfoLog() string { return getstr(s, gl.INFO_LOG_LENGTH, gl.GetShaderInfoLog) }

func (s Shader) Compile(source string) error {
	nullTerminate(&source)
	cstr, free := gl.Strs(source)
	defer free()

	gl.ShaderSource(s.ID(), 1, cstr, nil)
	gl.CompileShader(s.ID())

	return check(s, gl.COMPILE_STATUS, "failed to compile shader: %v")
}

func (s Shader) Delete() { gl.DeleteShader(s.ID()) }
