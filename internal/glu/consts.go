package glu

import "github.com/go-gl/gl/all-core/gl"

// (?:(\t)|_)([a-z])([a-z]+) -> $1\U$2\L$3

const (
	ArrayBuffer             = BufferTarget(gl.ARRAY_BUFFER)              // Vertex attributes
	AtomicCounterBuffer     = BufferTarget(gl.ATOMIC_COUNTER_BUFFER)     // Atomic counter storage
	CopyReadBuffer          = BufferTarget(gl.COPY_READ_BUFFER)          // Buffer copy source
	CopyWriteBuffer         = BufferTarget(gl.COPY_WRITE_BUFFER)         // Buffer copy destination
	DispatchIndirectBuffer  = BufferTarget(gl.DISPATCH_INDIRECT_BUFFER)  // Indirect compute dispatch commands
	DrawIndirectBuffer      = BufferTarget(gl.DRAW_INDIRECT_BUFFER)      // Indirect command arguments
	ElementArrayBuffer      = BufferTarget(gl.ELEMENT_ARRAY_BUFFER)      // Vertex array indices
	PixelPackBuffer         = BufferTarget(gl.PIXEL_PACK_BUFFER)         // Pixel read target
	PixelUnpackBuffer       = BufferTarget(gl.PIXEL_UNPACK_BUFFER)       // Texture data source
	QueryBuffer             = BufferTarget(gl.QUERY_BUFFER)              // Query result buffer
	ShaderStorageBuffer     = BufferTarget(gl.SHADER_STORAGE_BUFFER)     // Read-write storage for shaders
	TextureBuffer           = BufferTarget(gl.TEXTURE_BUFFER)            // Texture data buffer
	TransformFeedbackBuffer = BufferTarget(gl.TRANSFORM_FEEDBACK_BUFFER) // Transform feedback buffer
	UniformBuffer           = BufferTarget(gl.UNIFORM_BUFFER)            // Uniform block storage

)

const (
	Texture1D            = TextureTarget(gl.TEXTURE_1D)
	Texture2D            = TextureTarget(gl.TEXTURE_2D)
	Texture2DMultisample = TextureTarget(gl.TEXTURE_2D_MULTISAMPLE)
	Texture3D            = TextureTarget(gl.TEXTURE_3D)
	TextureRectangle     = TextureTarget(gl.TEXTURE_RECTANGLE)
	TextureCubeMap       = TextureTarget(gl.TEXTURE_CUBE_MAP)

	Texture1DArray            = TextureTarget(gl.TEXTURE_1D_ARRAY)
	Texture2DArray            = TextureTarget(gl.TEXTURE_2D_ARRAY)
	TextureCubeMapArray       = TextureTarget(gl.TEXTURE_CUBE_MAP_ARRAY)
	Texture2DMultisampleArray = TextureTarget(gl.TEXTURE_2D_MULTISAMPLE_ARRAY)

	TextureCubeMapPosX = TextureTarget(gl.TEXTURE_CUBE_MAP_POSITIVE_X)
	TextureCubeMapNegX = TextureTarget(gl.TEXTURE_CUBE_MAP_NEGATIVE_X)
	TextureCubeMapPosY = TextureTarget(gl.TEXTURE_CUBE_MAP_POSITIVE_Y)
	TextureCubeMapNegY = TextureTarget(gl.TEXTURE_CUBE_MAP_NEGATIVE_Y)
	TextureCubeMapPosZ = TextureTarget(gl.TEXTURE_CUBE_MAP_POSITIVE_Z)
	TextureCubeMapNegZ = TextureTarget(gl.TEXTURE_CUBE_MAP_NEGATIVE_Z)
)

const (
	ComputeShader        = ShaderType(gl.COMPUTE_SHADER)
	VertexShader         = ShaderType(gl.VERTEX_SHADER)
	TessControlShader    = ShaderType(gl.TESS_CONTROL_SHADER)
	TessEvaluationShader = ShaderType(gl.TESS_EVALUATION_SHADER)
	GeometryShader       = ShaderType(gl.GEOMETRY_SHADER)
	FragmentShader       = ShaderType(gl.FRAGMENT_SHADER)
)
