package glu

import (
	"sync/atomic"

	"github.com/go-gl/gl/all-core/gl"
)

var glVersion int32

func Version() (major, minor int) {
again:
	v := atomic.LoadInt32(&glVersion)
	if v != 0 {
		return int(v >> 8), int(v & 0xFF)
	}

	var m, n int32
	gl.GetIntegerv(gl.MAJOR_VERSION, &m)
	gl.GetIntegerv(gl.MINOR_VERSION, &n)
	if m > 255 || n > 255 {
		panic("open GL version higher than 255?!?")
	}

	u := m<<8 | n
	if !atomic.CompareAndSwapInt32(&glVersion, v, u) {
		goto again
	}

	return int(m), int(n)
}

func VersionAtLeast(major, minor int) bool {
	m, n := Version()
	switch {
	case m < major:
		return false
	case m > major:
		return true
	default:
		return n >= minor
	}
}
