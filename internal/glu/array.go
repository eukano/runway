package glu

import (
	"github.com/go-gl/gl/all-core/gl"
)

func NewVertexArray() VertexArray {
	v := VertexArray{Gen(1, gl.GenVertexArrays, 1, "could not create vertex array")[0]}
	debugLifecycle.Log("created VAO", v.ID())
	return v
}

func (a VertexArray) Delete() {
	gl.DeleteVertexArrays(1, a.ptr())
	debugLifecycle.Log("deleted VAO", a.ID())
}

func (a VertexArray) Bind()   { gl.BindVertexArray(a.ID()) }
func (_ VertexArray) Unbind() { gl.BindVertexArray(0) }
