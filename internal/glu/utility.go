package glu

import (
	"github.com/go-gl/gl/all-core/gl"
	"github.com/go-gl/mathgl/mgl64"
	"gitlab.com/eukano/runway/internal/debug"
)

var debugLifecycle debug.Logger

var Color = struct {
	R, G, B, W, C, M, Y, K mgl64.Vec3
}{
	mgl64.Vec3{1, 0, 0},
	mgl64.Vec3{0, 1, 0},
	mgl64.Vec3{0, 0, 1},
	mgl64.Vec3{1, 1, 1},
	mgl64.Vec3{0, 1, 1},
	mgl64.Vec3{1, 0, 1},
	mgl64.Vec3{1, 1, 0},
	mgl64.Vec3{0, 0, 0},
}

func BuildShaderProgram(source map[ShaderType]string) (Program, error) {
	prog := NewProgram()

	for typ, src := range source {
		s := typ.New()
		defer s.Delete()

		err := s.Compile(src)
		if err != nil {
			return Program{0}, err
		}

		prog.AttachShaders(s)
	}

	err := prog.Link()
	if err != nil {
		return Program{0}, err
	}

	return prog, nil
}

func Enable(cap uint32, fn func()) {
	if gl.IsEnabled(cap) {
		fn()
		return
	}

	gl.Enable(cap)
	fn()
	gl.Disable(cap)
}

func Disable(cap uint32, fn func()) {
	if !gl.IsEnabled(cap) {
		fn()
		return
	}

	gl.Disable(cap)
	fn()
	gl.Enable(cap)
}
