package glu

import (
	"unsafe"

	"github.com/go-gl/gl/all-core/gl"
)

func (a VertexAttrib) Enable()  { gl.EnableVertexAttribArray(a.ID()) }
func (a VertexAttrib) Disable() { gl.DisableVertexAttribArray(a.ID()) }

func (a VertexAttrib) AsArray(size int32, xtype uint32, normalized bool, stride int32, pointer unsafe.Pointer) {
	gl.VertexAttribPointer(a.ID(), size, xtype, normalized, stride, pointer)
}

// The commands containing N indicate that the arguments will be passed as
// fixed-point values that are scaled to a normalized range according to the
// component conversion rules defined by the OpenGL specification. Signed values
// are understood to represent fixed-point values in the range [-1,1], and
// unsigned values are understood to represent fixed-point values in the range
// [0,1].
//
// The commands containing I indicate that the arguments are extended to full
// signed or unsigned integers.
//
// The commands containing P indicate that the arguments are stored as packed
// components within a larger natural type.
//
// The commands containing L indicate that the arguments are full 64-bit
// quantities and should be passed directly to shader inputs declared as 64-bit
// double precision types.

func (a VertexAttrib) As1f(v0 float32)              { gl.VertexAttrib1f(a.ID(), v0) }
func (a VertexAttrib) As1s(v0 int16)                { gl.VertexAttrib1s(a.ID(), v0) }
func (a VertexAttrib) As1d(v0 float64)              { gl.VertexAttrib1d(a.ID(), v0) }
func (a VertexAttrib) AsI1i(v0 int32)               { gl.VertexAttribI1i(a.ID(), v0) }
func (a VertexAttrib) AsI1ui(v0 uint32)             { gl.VertexAttribI1ui(a.ID(), v0) }
func (a VertexAttrib) As2f(v0, v1 float32)          { gl.VertexAttrib2f(a.ID(), v0, v1) }
func (a VertexAttrib) As2s(v0, v1 int16)            { gl.VertexAttrib2s(a.ID(), v0, v1) }
func (a VertexAttrib) As2d(v0, v1 float64)          { gl.VertexAttrib2d(a.ID(), v0, v1) }
func (a VertexAttrib) AsI2i(v0, v1 int32)           { gl.VertexAttribI2i(a.ID(), v0, v1) }
func (a VertexAttrib) AsI2ui(v0, v1 uint32)         { gl.VertexAttribI2ui(a.ID(), v0, v1) }
func (a VertexAttrib) As3f(v0, v1, v2 float32)      { gl.VertexAttrib3f(a.ID(), v0, v1, v2) }
func (a VertexAttrib) As3s(v0, v1, v2 int16)        { gl.VertexAttrib3s(a.ID(), v0, v1, v2) }
func (a VertexAttrib) As3d(v0, v1, v2 float64)      { gl.VertexAttrib3d(a.ID(), v0, v1, v2) }
func (a VertexAttrib) AsI3i(v0, v1, v2 int32)       { gl.VertexAttribI3i(a.ID(), v0, v1, v2) }
func (a VertexAttrib) AsI3ui(v0, v1, v2 uint32)     { gl.VertexAttribI3ui(a.ID(), v0, v1, v2) }
func (a VertexAttrib) As4f(v0, v1, v2, v3 float32)  { gl.VertexAttrib4f(a.ID(), v0, v1, v2, v3) }
func (a VertexAttrib) As4s(v0, v1, v2, v3 int16)    { gl.VertexAttrib4s(a.ID(), v0, v1, v2, v3) }
func (a VertexAttrib) As4d(v0, v1, v2, v3 float64)  { gl.VertexAttrib4d(a.ID(), v0, v1, v2, v3) }
func (a VertexAttrib) As4Nub(v0, v1, v2, v3 uint8)  { gl.VertexAttrib4Nub(a.ID(), v0, v1, v2, v3) }
func (a VertexAttrib) AsI4i(v0, v1, v2, v3 int32)   { gl.VertexAttribI4i(a.ID(), v0, v1, v2, v3) }
func (a VertexAttrib) AsI4ui(v0, v1, v2, v3 uint32) { gl.VertexAttribI4ui(a.ID(), v0, v1, v2, v3) }
func (a VertexAttrib) AsL1d(v0 float64)             { gl.VertexAttribL1d(a.ID(), v0) }
func (a VertexAttrib) AsL2d(v0, v1 float64)         { gl.VertexAttribL2d(a.ID(), v0, v1) }
func (a VertexAttrib) AsL3d(v0, v1, v2 float64)     { gl.VertexAttribL3d(a.ID(), v0, v1, v2) }
func (a VertexAttrib) AsL4d(v0, v1, v2, v3 float64) { gl.VertexAttribL4d(a.ID(), v0, v1, v2, v3) }

func (a VertexAttrib) As1fv(v [1]float32)  { gl.VertexAttrib1fv(a.ID(), &v[0]) }
func (a VertexAttrib) As1sv(v [1]int16)    { gl.VertexAttrib1sv(a.ID(), &v[0]) }
func (a VertexAttrib) As1dv(v [1]float64)  { gl.VertexAttrib1dv(a.ID(), &v[0]) }
func (a VertexAttrib) AsI1iv(v [1]int32)   { gl.VertexAttribI1iv(a.ID(), &v[0]) }
func (a VertexAttrib) AsI1uiv(v [1]uint32) { gl.VertexAttribI1uiv(a.ID(), &v[0]) }
func (a VertexAttrib) As2fv(v [2]float32)  { gl.VertexAttrib2fv(a.ID(), &v[0]) }
func (a VertexAttrib) As2sv(v [2]int16)    { gl.VertexAttrib2sv(a.ID(), &v[0]) }
func (a VertexAttrib) As2dv(v [2]float64)  { gl.VertexAttrib2dv(a.ID(), &v[0]) }
func (a VertexAttrib) AsI2iv(v [2]int32)   { gl.VertexAttribI2iv(a.ID(), &v[0]) }
func (a VertexAttrib) AsI2uiv(v [2]uint32) { gl.VertexAttribI2uiv(a.ID(), &v[0]) }
func (a VertexAttrib) As3fv(v [3]float32)  { gl.VertexAttrib3fv(a.ID(), &v[0]) }
func (a VertexAttrib) As3sv(v [3]int16)    { gl.VertexAttrib3sv(a.ID(), &v[0]) }
func (a VertexAttrib) As3dv(v [3]float64)  { gl.VertexAttrib3dv(a.ID(), &v[0]) }
func (a VertexAttrib) AsI3iv(v [3]int32)   { gl.VertexAttribI3iv(a.ID(), &v[0]) }
func (a VertexAttrib) AsI3uiv(v [3]uint32) { gl.VertexAttribI3uiv(a.ID(), &v[0]) }
func (a VertexAttrib) As4fv(v [4]float32)  { gl.VertexAttrib4fv(a.ID(), &v[0]) }
func (a VertexAttrib) As4sv(v [4]int16)    { gl.VertexAttrib4sv(a.ID(), &v[0]) }
func (a VertexAttrib) As4dv(v [4]float64)  { gl.VertexAttrib4dv(a.ID(), &v[0]) }
func (a VertexAttrib) As4iv(v [4]int32)    { gl.VertexAttrib4iv(a.ID(), &v[0]) }
func (a VertexAttrib) As4bv(v [4]int8)     { gl.VertexAttrib4bv(a.ID(), &v[0]) }
func (a VertexAttrib) As4ubv(v [4]uint8)   { gl.VertexAttrib4ubv(a.ID(), &v[0]) }
func (a VertexAttrib) As4usv(v [4]uint16)  { gl.VertexAttrib4usv(a.ID(), &v[0]) }
func (a VertexAttrib) As4uiv(v [4]uint32)  { gl.VertexAttrib4uiv(a.ID(), &v[0]) }
func (a VertexAttrib) As4Nbv(v [4]int8)    { gl.VertexAttrib4Nbv(a.ID(), &v[0]) }
func (a VertexAttrib) As4Nsv(v [4]int16)   { gl.VertexAttrib4Nsv(a.ID(), &v[0]) }
func (a VertexAttrib) As4Niv(v [4]int32)   { gl.VertexAttrib4Niv(a.ID(), &v[0]) }
func (a VertexAttrib) As4Nubv(v [4]uint8)  { gl.VertexAttrib4Nubv(a.ID(), &v[0]) }
func (a VertexAttrib) As4Nusv(v [4]uint16) { gl.VertexAttrib4Nusv(a.ID(), &v[0]) }
func (a VertexAttrib) As4Nuiv(v [4]uint32) { gl.VertexAttrib4Nuiv(a.ID(), &v[0]) }
func (a VertexAttrib) AsI4bv(v [4]int8)    { gl.VertexAttribI4bv(a.ID(), &v[0]) }
func (a VertexAttrib) AsI4ubv(v [4]uint8)  { gl.VertexAttribI4ubv(a.ID(), &v[0]) }
func (a VertexAttrib) AsI4sv(v [4]int16)   { gl.VertexAttribI4sv(a.ID(), &v[0]) }
func (a VertexAttrib) AsI4usv(v [4]uint16) { gl.VertexAttribI4usv(a.ID(), &v[0]) }
func (a VertexAttrib) AsI4iv(v [4]int32)   { gl.VertexAttribI4iv(a.ID(), &v[0]) }
func (a VertexAttrib) AsI4uiv(v [4]uint32) { gl.VertexAttribI4uiv(a.ID(), &v[0]) }
func (a VertexAttrib) AsL1dv(v [1]float64) { gl.VertexAttribL1dv(a.ID(), &v[0]) }
func (a VertexAttrib) AsL2dv(v [2]float64) { gl.VertexAttribL2dv(a.ID(), &v[0]) }
func (a VertexAttrib) AsL3dv(v [3]float64) { gl.VertexAttribL3dv(a.ID(), &v[0]) }
func (a VertexAttrib) AsL4dv(v [4]float64) { gl.VertexAttribL4dv(a.ID(), &v[0]) }

// func (a VertexAttrib) AsP1ui(typ uint32, normalized bool, value uint32) {}
// func (a VertexAttrib) AsP2ui(typ uint32, normalized bool, value uint32) {}
// func (a VertexAttrib) AsP3ui(typ uint32, normalized bool, value uint32) {}
// func (a VertexAttrib) AsP4ui(typ uint32, normalized bool, value uint32) {}
