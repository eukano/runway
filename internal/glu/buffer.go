package glu

import (
	"github.com/go-gl/gl/all-core/gl"
	"gitlab.com/eukano/runway/internal/mesh"
)

type BufferTarget uint32

func (b BufferTarget) ID() uint32 { return uint32(b) }

func (b BufferTarget) New() Buffer {
	return Buffer{Target: b, Ref: Gen(1, gl.GenBuffers, 1, "could not create buffer")[0]}
}

func (b BufferTarget) NewWith(data interface{}, usage uint32) Buffer {
	bo := b.New()
	bo.Bind()
	b.write(data, usage)
	return bo
}

func (b BufferTarget) write(data interface{}, usage uint32) {
	size := mesh.Size(data)
	if size == 0 {
		return
	}
	gl.BufferData(b.ID(), size, gl.Ptr(data), usage)
}

type Buffer struct {
	Ref
	Target BufferTarget
}

func (b Buffer) Delete() { gl.DeleteBuffers(1, b.ptr()) }
func (b Buffer) Bind()   { gl.BindBuffer(b.Target.ID(), b.ID()) }
func (b Buffer) Unbind() { gl.BindBuffer(b.Target.ID(), 0) }
