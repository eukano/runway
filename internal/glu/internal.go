package glu

import (
	"fmt"
	"os"
	"path"
	"reflect"
	"runtime"
	"strings"

	"github.com/go-gl/gl/all-core/gl"
)

var modulePath = path.Join(reflect.TypeOf(Ref(0)).PkgPath(), "..", "..")
var rendererRealize = path.Join(modulePath, "script", "runway.(*Renderer).Realize")

func (r Ref) check(skip int, msg string, args ...interface{}) {
	if r != 0 {
		return
	}

	msg = fmt.Sprintf(msg, args...)
	e := gl.GetError()
	if e != 0 {
		msg += fmt.Sprintf("; gl error 0x%04x", e)
	}

	pc := make([]uintptr, 20)
	n := runtime.Callers(skip+2, pc)
	pc = pc[:n]
	if n == 0 {
		panic(msg)
	}

	fmt.Printf("[FATAL] {GL} %s\n", msg)
	frames := runtime.CallersFrames(pc)
	for {
		frame, more := frames.Next()
		fmt.Printf("  %s\n", frame.Function)
		fmt.Printf("    %s:%d\n", frame.File, frame.Line)
		if !more || frame.Function == rendererRealize {
			break
		}
	}
	os.Exit(1)
}

func nullTerminate(s *string) {
	if strings.HasSuffix(*s, "\x00") {
		return
	}

	*s += "\x00"
}

func getiv(obj hasID, name uint32, fn func(uint32, uint32, *int32)) int32 {
	var v int32
	fn(obj.ID(), name, &v)
	return v
}

func getstr(obj hasGetiv, lenID uint32, fn func(uint32, int32, *int32, *uint8)) string {
	n := obj.Getiv(lenID)
	s := strings.Repeat("\x00", int(n+1))
	fn(obj.ID(), n, nil, gl.Str(s))
	return s
}

type hasID interface {
	ID() uint32
}

type hasGetiv interface {
	hasID
	Getiv(uint32) int32
}

type hasInfoLog interface {
	hasGetiv
	InfoLog() string
}

func check(obj hasInfoLog, name uint32, format string, args ...interface{}) error {
	st := obj.Getiv(name)
	if st != gl.FALSE {
		return nil
	}

	return fmt.Errorf(format, append(args, obj.InfoLog())...)
}
