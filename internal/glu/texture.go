package glu

import (
	"image"
	"image/draw"
	"unsafe"

	"github.com/go-gl/gl/all-core/gl"
)

type TextureTarget uint32

func (t TextureTarget) ID() uint32 { return uint32(t) }

func (t TextureTarget) New() Texture {
	return Texture{Target: t, Ref: Gen(1, gl.GenTextures, 1, "could not create texture")[0]}
}

func (t TextureTarget) Parameteri(name uint32, val int32) {
	gl.TexParameteri(t.ID(), name, val)
}

func (t TextureTarget) Image2D(level, internalformat, width, height, border int32, format, xtype uint32, pixels unsafe.Pointer) {
	gl.TexImage2D(t.ID(), level, internalformat, width, height, border, format, xtype, pixels)
}

func (t TextureTarget) LoadImage2D(img image.Image, level, internalFormat, border int32) {
	bounds := img.Bounds()
	size := bounds.Size()
	rgba, ok := img.(*image.RGBA)
	if !ok {
		rgba = image.NewRGBA(bounds)
		draw.Draw(rgba, bounds, img, image.Point{}, draw.Src)
	}

	t.Image2D(level, internalFormat, int32(size.X), int32(size.Y), border, gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(rgba.Pix))
}

type Texture struct {
	Ref
	Target TextureTarget
}

func (t Texture) Delete() { gl.DeleteTextures(1, t.ptr()) }
func (t Texture) Bind()   { gl.BindTexture(t.Target.ID(), t.ID()) }
func (t Texture) Unbind() { gl.BindTexture(t.Target.ID(), 0) }
