package glu

import (
	"strconv"

	"github.com/go-gl/gl/all-core/gl"
)

// Debug message severity
type DbgSev uint32

// Debug message source
type DbgSrc uint32

// Debug message type
type DbgTyp uint32

const (
	DbgSevHigh               DbgSev = gl.DEBUG_SEVERITY_HIGH
	DbgSevMedium             DbgSev = gl.DEBUG_SEVERITY_MEDIUM
	DbgSevLow                DbgSev = gl.DEBUG_SEVERITY_LOW
	DbgSevNotification       DbgSev = gl.DEBUG_SEVERITY_NOTIFICATION
	DbgSrcApi                DbgSrc = gl.DEBUG_SOURCE_API
	DbgSrcWindowSystem       DbgSrc = gl.DEBUG_SOURCE_WINDOW_SYSTEM
	DbgSrcShaderCompiler     DbgSrc = gl.DEBUG_SOURCE_SHADER_COMPILER
	DbgSrcThirdParty         DbgSrc = gl.DEBUG_SOURCE_THIRD_PARTY
	DbgSrcApplication        DbgSrc = gl.DEBUG_SOURCE_APPLICATION
	DbgSrcOther              DbgSrc = gl.DEBUG_SOURCE_OTHER
	DbgTypError              DbgTyp = gl.DEBUG_TYPE_ERROR
	DbgTypDeprecatedBehavior DbgTyp = gl.DEBUG_TYPE_DEPRECATED_BEHAVIOR
	DbgTypUndefinedBehavior  DbgTyp = gl.DEBUG_TYPE_UNDEFINED_BEHAVIOR
	DbgTypPortability        DbgTyp = gl.DEBUG_TYPE_PORTABILITY
	DbgTypPerformance        DbgTyp = gl.DEBUG_TYPE_PERFORMANCE
	DbgTypMarker             DbgTyp = gl.DEBUG_TYPE_MARKER
	DbgTypPushGroup          DbgTyp = gl.DEBUG_TYPE_PUSH_GROUP
	DbgTypPopGroup           DbgTyp = gl.DEBUG_TYPE_POP_GROUP
	DbgTypOther              DbgTyp = gl.DEBUG_TYPE_OTHER
)

func (v DbgSev) String() string {
	switch v {
	case gl.DEBUG_SEVERITY_HIGH:
		return "ERR"
	case gl.DEBUG_SEVERITY_MEDIUM:
		return "WARN"
	case gl.DEBUG_SEVERITY_LOW:
		return "INFO"
	case gl.DEBUG_SEVERITY_NOTIFICATION:
		return "NOTE"
	default:
		return "0x" + strconv.FormatInt(int64(v), 16)
	}
}

func (v DbgSrc) String() string {
	switch v {
	case gl.DEBUG_SOURCE_API:
		return "API"
	case gl.DEBUG_SOURCE_WINDOW_SYSTEM:
		return "WINDOW"
	case gl.DEBUG_SOURCE_SHADER_COMPILER:
		return "SHADER"
	case gl.DEBUG_SOURCE_THIRD_PARTY:
		return "3RD"
	case gl.DEBUG_SOURCE_APPLICATION:
		return "APP"
	case gl.DEBUG_SOURCE_OTHER:
		return "OTHER"
	default:
		return "0x" + strconv.FormatInt(int64(v), 16)
	}
}

func (v DbgTyp) String() string {
	switch v {
	case gl.DEBUG_TYPE_ERROR:
		return "ERROR"
	case gl.DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		return "DEPRECATED_BEHAVIOR"
	case gl.DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		return "UNDEFINED_BEHAVIOR"
	case gl.DEBUG_TYPE_PORTABILITY:
		return "PORTABILITY"
	case gl.DEBUG_TYPE_PERFORMANCE:
		return "PERFORMANCE"
	case gl.DEBUG_TYPE_MARKER:
		return "MARKER"
	case gl.DEBUG_TYPE_PUSH_GROUP:
		return "PUSH_GROUP"
	case gl.DEBUG_TYPE_POP_GROUP:
		return "POP_GROUP"
	case gl.DEBUG_TYPE_OTHER:
		return "OTHER"
	default:
		return "0x" + strconv.FormatInt(int64(v), 16)
	}
}
