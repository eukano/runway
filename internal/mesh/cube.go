package mesh

import (
	"gitlab.com/eukano/runway/internal/geom"
	"gitlab.com/eukano/runway/internal/geomath"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// A QuadTriV is a set of triangle vertices that represent a quadrangle.
type QuadTriV = [2]geom.Tri

// A QuadTriI is the set of triangle vertex indicies that represent a quadrangle.
type QuadTriI = [2]geomath.TriI

// CubeWinding is the vertex winding order (CW) of the faces of a cube.
//
// Do not use directly.
var CubeWinding = [6][4]geomath.Index{
	{0b000, 0b001, 0b011, 0b010}, // right
	{0b100, 0b110, 0b111, 0b101}, // left
	{0b000, 0b100, 0b101, 0b001}, // top
	{0b010, 0b011, 0b111, 0b110}, // bottom
	{0b000, 0b010, 0b110, 0b100}, // front
	{0b001, 0b101, 0b111, 0b011}, // back
}

// CubeNormal is the normal vectors of the faces of a cube.
//
// Do not use directly.
var CubeNormal = [6]tensor.Vec3{
	{+1, 0, 0}, // right
	{-1, 0, 0}, // left
	{0, +1, 0}, // top
	{0, -1, 0}, // bottom
	{0, 0, +1}, // front
	{0, 0, -1}, // back
}

// CubeVertex is the vertices of the unit cube.
//
// Indexed by CubeTriIndex.
var CubeVertex = [8]tensor.Vec3{
	{+0.5, +0.5, +0.5},
	{+0.5, +0.5, -0.5},
	{+0.5, -0.5, +0.5},
	{+0.5, -0.5, -0.5},
	{-0.5, +0.5, +0.5},
	{-0.5, +0.5, -0.5},
	{-0.5, -0.5, +0.5},
	{-0.5, -0.5, -0.5},
}

// CubeTriIndex is the triangle vertex indicies of the faces of the unit cube.
//
// Indexes CubeVertex.
var CubeTriIndex = func() (i [6]QuadTriI) {
	for n := range i {
		w := CubeWinding[n]
		i[n] = QuadTriI{
			{w[0], w[1], w[3]},
			{w[2], w[3], w[1]},
		}
	}
	return i
}()

// CubeTriVertex is the triangle verticies of the faces of the unit cube.
//
// Can be used directly as a vertex buffer.
var CubeTriVertex = func() (t [6]QuadTriV) {
	v := CubeVertex
	for n := range t {
		w := CubeWinding[n]
		t[n] = QuadTriV{
			{v[w[0]], v[w[1]], v[w[3]]},
			{v[w[2]], v[w[3]], v[w[1]]},
		}
	}
	return t
}()

// CubeQuadIndex is the quadrangle triangle vertex indicies of the faces of the unit cube.
//
// Indexes CubeQuadVertex, CubeQuadNormal, CubeQuadColors.
var CubeQuadIndex = func() (i [6]QuadTriI) {
	for n := range i {
		x := geomath.Index(4 * n)
		i[n] = QuadTriI{
			{x + 0, x + 1, x + 3},
			{x + 2, x + 3, x + 1},
		}
	}
	return i
}()

// CubeQuadVertex is the quadrangle verticies of the faces of the unit cube.
//
// Indexed by CubeQuadIndex.
var CubeQuadVertex = func() (q [6]geom.Quad) {
	v := CubeVertex
	for n := range q {
		w := CubeWinding[n]
		q[n] = geom.Quad{v[w[0]], v[w[1]], v[w[2]], v[w[3]]}
	}
	return q
}()

// CubeQuadNormal is the quadrangle vertex normal vectors of the unit cube.
//
// Indexed by CubeQuadIndex.
var CubeQuadNormal = func() (v [6]geom.Quad) {
	for n := range v {
		r := CubeNormal[n]
		v[n] = geom.Quad{r, r, r, r}
	}
	return v
}()

// CubeColor is the coloring of the verticies of a cube
type CubeColor = [2][2][2]tensor.Vec3

// CubeQuadColors maps CubeColor to the quadrangle verticies of the faces of a cube.
//
// Indexed by CubeQuadIndex.
func CubeQuadColors(c CubeColor) (v [6]geom.Quad) {
	get := func(n geomath.Index) tensor.Vec3 {
		i, j, k := (n>>2)&1, (n>>1)&1, (n>>0)&1
		return c[i][j][k]
	}

	for n := range v {
		w := CubeWinding[n]
		v[n] = geom.Quad{get(w[0]), get(w[1]), get(w[2]), get(w[3])}
	}
	return v
}
