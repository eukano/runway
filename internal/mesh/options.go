package mesh

import (
	"math/rand"

	"gitlab.com/eukano/runway/internal/geom"
	"gitlab.com/eukano/runway/internal/tree"
	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

func NormalFromFace(manifold.Polyhedron) Normalizer {
	return func(f manifold.Polygon, i int, v tensor.Vec3) tensor.Vec3 {
		if manifold.IsConvex(f) {
			return manifold.Normal(f)
		}

		var normal tensor.Vec3
		for i, n := 0, f.Len(); i < n; i++ {
			normal = geom.TriOf(f, i, i+1, i+2).Area().Add(normal)
		}
		return normal.Normalize()
	}
}

func NormalFromCenter(s manifold.Polyhedron) Normalizer {
	var c tensor.Vec3
	for _, f := range manifold.Faces(s) {
		c = manifold.Center(f).Add(c)
	}
	c = c.Mul(1 / float64(s.Len()))

	return func(f manifold.Polygon, i int, v tensor.Vec3) tensor.Vec3 {
		return v.Sub(c).Normalize()
	}
}

func NormalFromAdjacent(s manifold.Polyhedron) Normalizer {
	var set tree.VertexDataSet[tensor.Vec3]
	for i, n := 0, s.Len(); i < n; i++ {
		norm := manifold.Normal(s.Get(i))
		for _, v := range manifold.Vertices(s.Get(i)) {
			set = set.Append(v, norm)
		}
	}
	tree := set.AsTree()

	return func(f manifold.Polygon, _ int, v tensor.Vec3) tensor.Vec3 {
		// calculate a weighted sum of the normals of the face and its
		// neighbors, where the weight is the dot product of a neighbor's normal
		// and the face's normal, squared
		n := manifold.Normal(f)
		for _, r := range tree.NearestSet(v, tensor.AdjacentEps) {
			m := r.Data
			d := n.Dot(m) / n.Len() / m.Len()
			if d < 0 {
				d = 0
			} else {
				d *= d
			}
			n = m.Mul(d).Add(n)
		}
		return n.Normalize()
	}
}

func UniformColor(c tensor.Vec3) func(manifold.Polyhedron) Colorizer {
	return func(manifold.Polyhedron) Colorizer {
		return func(manifold.Polygon, int, tensor.Vec3) tensor.Vec4 {
			return c.Vec4(1)
		}
	}
}

func UniformColorA(c tensor.Vec4) func(manifold.Polyhedron) Colorizer {
	return func(manifold.Polyhedron) Colorizer {
		return func(manifold.Polygon, int, tensor.Vec3) tensor.Vec4 {
			return c
		}
	}
}

func RandomColor(manifold.Polyhedron) Colorizer {
	return func(manifold.Polygon, int, tensor.Vec3) tensor.Vec4 {
		return tensor.Vec4{
			rand.Float64(),
			rand.Float64(),
			rand.Float64(),
			1,
		}
	}
}
