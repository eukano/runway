package mesh

import (
	"gitlab.com/eukano/runway/internal/geomath"
	"gitlab.com/eukano/runway/internal/mesh/triangulate"
	"gitlab.com/eukano/runway/pkg/geom/manifold"
	polymath "gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

type Normalizer func(manifold.Polygon, int, tensor.Vec3) tensor.Vec3
type Colorizer func(manifold.Polygon, int, tensor.Vec3) tensor.Vec4

type Mesh struct {
	Vertex []tensor.Vec3
	Normal []tensor.Vec3
	Color  []tensor.Vec4
	Index  geomath.Mesh
}

func Build(shape manifold.Polyhedron, mknorm func(manifold.Polyhedron) Normalizer, mkcolor func(manifold.Polyhedron) Colorizer) *Mesh {
	m := new(Mesh)

	if mknorm == nil {
		mknorm = NormalFromFace
	}
	if mkcolor == nil {
		mkcolor = UniformColor(tensor.Vec3{0.5, 0.5, 0.5})
	}

	norm := mknorm(shape)
	color := mkcolor(shape)

	n := 0
	for _, f := range manifold.Faces(shape) {
		v := manifold.Vertices(f)
		m.Vertex = append(m.Vertex, v...)

		for i, v := range v {
			m.Normal = append(m.Normal, norm(f, i, v))
			m.Color = append(m.Color, color(f, i, v))
		}

		if polymath.IsConvex(f) {
			m.Index.Append(triangulate.Convex(n, len(v))...)
		} else {
			m.Index.Append(triangulate.ClipEars(n, f)...)
		}
		n += len(v)
	}

	return m
}
