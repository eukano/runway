package triangulate

import (
	"fmt"

	"gitlab.com/eukano/runway/internal/geom"
)

// marker backed by a slice
type sliceMarker []int

func (m *sliceMarker) String() string { return fmt.Sprint(*m) }

func (m *sliceMarker) empty() bool { return len(*m) == 0 }

func (m *sliceMarker) locate(tri *geom.FaceTri) int {
	for i, j := range *m {
		if j == tri.Index[1] {
			return i
		}
	}
	return -1
}

func (m *sliceMarker) marked(tri *geom.FaceTri) bool {
	return m.locate(tri) >= 0
}

func (m *sliceMarker) mark(tri *geom.FaceTri) {
	if m.marked(tri) {
		return
	}

	*m = append(*m, tri.Index[1])
}

func (m *sliceMarker) unmark(tri *geom.FaceTri) {
	i := m.locate(tri)
	if i < 0 {
		return
	}

	*m = append((*m)[:i], (*m)[i+1:]...)
}

func (m *sliceMarker) next() int {
	return (*m)[0]
}

func (m *sliceMarker) each(fn func(int)) {
	for _, i := range *m {
		fn(i)
	}
}

func (m *sliceMarker) has(fn func(int) bool) bool {
	for _, i := range *m {
		if fn(i) {
			return true
		}
	}
	return false
}
