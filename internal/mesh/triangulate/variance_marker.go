package triangulate

import (
	"fmt"
	"sort"

	"gitlab.com/eukano/runway/internal/geom"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// The variance marker is a special ear marker - when it is asked for the next
// ear, it will return the ear with the smallest variance in interior angles.
// This minimizes the number of oblique and acute triangles used in
// triangulation.

type indexAndVariance struct {
	index    int
	variance float64
}

type varianceMarker []indexAndVariance

func (m *varianceMarker) String() string {
	v := make([]int, 0, len(*m))
	m.each(func(i int) { v = append(v, i) })
	return fmt.Sprint(v)
}

func (m *varianceMarker) empty() bool { return len(*m) == 0 }

func (varianceMarker) variance(tri *geom.FaceTri) float64 {
	return tensor.VarianceTri(tri.Get(0), tri.Get(1), tri.Get(2))
}

func (m varianceMarker) search(v float64) int {
	return sort.Search(len(m), func(i int) bool { return m[i].variance >= v })
}

func (m varianceMarker) searchAll(tri *geom.FaceTri) int {
	i := m.search(m.variance(tri))
	if i < len(m) && m[i].index == tri.Index[1] {
		return i
	}

	for i := range m {
		if m[i].index == tri.Index[1] {
			return i
		}
	}
	return -1
}

func (m *varianceMarker) mark(tri *geom.FaceTri) {
	// remove old entry
	for i, v := range *m {
		if v.index == tri.Index[1] {
			*m = append((*m)[:i], (*m)[i+1:]...)
			break
		}
	}

	n := len(*m)
	v := m.variance(tri)
	i := m.search(v)
	if i < n && (*m)[i].index == tri.Index[1] {
		return
	}

	if cap(*m) > n {
		*m = (*m)[:n+1]
		copy((*m)[i+1:], (*m)[i:n])
	} else {
		*m = append(*m, indexAndVariance{-1, -1})
		for j := n; j > i; j-- {
			(*m)[j] = (*m)[j-1]
		}
	}
	(*m)[i] = indexAndVariance{tri.Index[1], v}
}

func (m *varianceMarker) unmark(tri *geom.FaceTri) {
	// Optimize for expected usage (tracking ears for fillConcave)
	if len(*m) > 0 && (*m)[0].index == tri.Index[1] {
		// Copy left to preserve array capacity
		*m = append((*m)[:0], (*m)[1:]...)
		return
	}

	i := m.searchAll(tri)
	if i < 0 {
		return
	}

	*m = append((*m)[:i], (*m)[i+1:]...)
}

func (m *varianceMarker) marked(tri *geom.FaceTri) bool {
	return m.searchAll(tri) >= 0
}

func (m *varianceMarker) next() int {
	return (*m)[0].index
}

func (m *varianceMarker) each(fn func(int)) {
	for _, iv := range *m {
		fn(iv.index)
	}
}

func (m *varianceMarker) has(fn func(int) bool) bool {
	for _, iv := range *m {
		if fn(iv.index) {
			return true
		}
	}
	return false
}
