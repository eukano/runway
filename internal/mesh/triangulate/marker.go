package triangulate

import (
	"gitlab.com/eukano/runway/internal/geom"
)

// used for marking verticies (as convex, reflex, etc)
type marker interface {
	marked(*geom.FaceTri) bool
	mark(*geom.FaceTri)
	unmark(*geom.FaceTri)

	empty() bool
	next() int
	each(func(int))
	has(func(int) bool) bool
}

// marker backed by a map
type mapMarker map[int]bool

func (m mapMarker) marked(tri *geom.FaceTri) bool { return m[tri.Index[1]] }
func (m mapMarker) mark(tri *geom.FaceTri)        { m[tri.Index[1]] = true }
func (m mapMarker) unmark(tri *geom.FaceTri)      { delete(m, tri.Index[1]) }
func (m mapMarker) empty() bool                   { return len(m) == 0 }

func (m mapMarker) next() int {
	for i := range m {
		return i
	}
	panic("empty")
}

func (m mapMarker) each(fn func(int)) {
	for i := range m {
		fn(i)
	}
}

func (m mapMarker) has(fn func(int) bool) bool {
	for i := range m {
		if fn(i) {
			return true
		}
	}
	return false
}
