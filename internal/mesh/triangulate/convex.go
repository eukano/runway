package triangulate

import "gitlab.com/eukano/runway/internal/geomath"

// Convex constructs an indexed mesh of an N-sided convex polygon.
func Convex(off, n int) geomath.Mesh {
	var index geomath.Mesh
	index.AddAt(off, 0, 1, 2)

	if n > 3 {
		index.AddAt(off, 0, 2, n-1)
	}

	for i := 2; i < n-2; i++ {
		j := i / 2
		if i%2 == 0 {
			index.AddAt(off, j+1, j+2, n-j)
		} else {
			index.AddAt(off, j+2, n-j-1, n-j)
		}
	}

	return index
}
