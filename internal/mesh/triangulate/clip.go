package triangulate

import (
	"log"
	"math"
	"sort"

	"gitlab.com/eukano/runway/internal/debug"
	"gitlab.com/eukano/runway/internal/geom"
	"gitlab.com/eukano/runway/internal/geomath"
	"gitlab.com/eukano/runway/pkg/geom/manifold"
	polymath "gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// MinimizeTriangulationVariance will tell ClipEars to prefer triangles with low
// angle variance (closer to equilateral) when triangulating concave faces. This
// will increase the cost of triangulation, as calculating angle variance is
// computationally non-trivial.
var MinimizeTriangulationVariance = true

var debugFillConcaveVertices, debugFillConcaveSteps debug.Logger

// ClipEars triangulates a polygon using an ear clipping algorithm. It returns
// an array of triangle vertex indices, with the specified offset.
func ClipEars(offset int, face manifold.Polygon) geomath.Mesh {
	c := earClipper{
		face:   face,
		offset: offset,
		convex: mapMarker{},
		reflex: mapMarker{},
		ear:    mapMarker{},
	}

	if MinimizeTriangulationVariance {
		c.ear = &varianceMarker{}
	}

	c.clip()
	return c.result
}

// implements ClipEars
type earClipper struct {
	face   manifold.Polygon
	offset int

	normal              tensor.Vec3
	convex, reflex, ear marker

	result geomath.Mesh
}

// implements ClipEars
func (c *earClipper) clip() {
	// Based on https://www.geometrictools.com/Documentation/TriangulationByEarClipping.pdf
	N := c.face.Len()
	c.normal = polymath.NormalSum(c.face)

	debugFillConcaveVertices.Logfn(func(l *log.Logger) {
		l.Println("vertices\n---")
		for _, v := range manifold.Vertices(c.face) {
			l.Printf("{%+.17f,%+.17f,%+.17f},\n", v[0], v[1], v[2])
		}
		// l.Printf("\nnormal {%+.17f,%+.17f,%+.17f}\n\n", c.normal[0], c.normal[1], c.normal[2])
	})

	index := make(indexSet, 0, N)

	// Mark convex and reflex
	for i := 0; i < N; i++ {
		tri := geom.TriOf(c.face, N+i-1, i, i+1)
		x := tri.Area()
		l := x.LenSqr()

		// Omit colinear vertices
		if l < tensor.AdjacentEps {
			continue
		}

		// Normalize
		l = math.Sqrt(float64(l))
		x = tensor.Vec3{x[0] / l, x[1] / l, x[2] / l}

		index = append(index, tri.Index[1])
		if c.normal.Dot(x) > 0 {
			c.convex.mark(tri)
		} else {
			c.reflex.mark(tri)
		}
	}

	// Mark ears
	c.convex.each(func(j int) {
		x := sort.SearchInts(index, j)         // Find J
		i, k := index.get(x-1), index.get(x+1) // Get nearby vertex indices
		tri := geom.TriOf(c.face, i, j, k)
		if c.isEar(tri) {
			c.ear.mark(tri)
		}
	})

	// While there are ears to clip
	for len(index) > 3 {
		debugFillConcaveSteps.Logfn(func(l *log.Logger) {
			// l.Printf("index: %v\n", index)
			// l.Printf("convex: %v\n", c.convex)
			// l.Printf("reflex: %v\n", c.reflex)
			l.Printf("ear: %v\n", c.ear)
		})

		// Next ear
		if c.ear.empty() {
			panic("no ears!")
		}
		j := c.ear.next()

		// Find J in the index set
		x := sort.SearchInts(index, j)
		if x >= len(index) {
			panic("invalid ear!")
		}

		// Get nearby vertices (2 CW, 2 CCW)
		h, i, k, l := index.get(x-2), index.get(x-1), index.get(x+1), index.get(x+2)

		// Append IJK
		c.result.AddAt(c.offset, i, j, k)

		// We're clipping J, so remove it from the index set and unmark it
		index.delete(x)
		debugFillConcaveSteps.Logf("clipping %d,%d,%d\n\n", i, j, k)
		tj := geom.TriOf(c.face, i, j, k)
		c.ear.unmark(tj)
		c.convex.unmark(tj)

		// Update prior and later vertices
		c.update(geom.TriOf(c.face, h, i, k))
		c.update(geom.TriOf(c.face, i, k, l))
	}

	if !c.reflex.empty() {
		panic("reflex vertices remain!")
	}

	// Append the last triangle
	c.result.AddAt(c.offset, index[0], index[1], index[2])
}

// check if the vertex is an ear
func (c *earClipper) isEar(tri *geom.FaceTri) bool {
	// It is sufficient to only check reflex vertices - a convex vertex
	// cannot be contained without a reflex vertex also being contained
	return !c.reflex.has(func(j int) bool {
		if tri.Has(j) {
			return false
		}

		// TODO: Can the Z threshold be tighter?
		a, b, c := tri.Project(c.face.Get(j)).Elem()
		if a > tensor.AdjacentEps && b > tensor.AdjacentEps && a+b <= 1 && math.Abs(c) < 1e-3 {
			return true
		}
		return false
	})
}

// updates the status of the vertex
func (c *earClipper) update(tri *geom.FaceTri) {
	// If the vertex was previously reflex and is now convex, update it
	wasConvex := c.convex.marked(tri)
	isConvex := wasConvex || tri.Area().Dot(c.normal) > 0
	if !wasConvex && isConvex {
		c.convex.mark(tri)
		c.reflex.unmark(tri)
	}

	// If the ear status of the vertex has changed, update it
	wasEar, isEar := c.ear.marked(tri), isConvex && c.isEar(tri)
	if !wasEar && isEar {
		c.ear.mark(tri)
	} else if wasEar && !isEar {
		c.ear.unmark(tri)
	}
}

// make it easier to deal with indices
type indexSet []int

// index into array, modulo array length
func (x indexSet) get(i int) int {
	if i < 0 {
		i += len(x)
	}
	return x[i%len(x)]
}

// remove the specified element
func (x *indexSet) delete(i int) {
	*x = append((*x)[:i], (*x)[i+1:]...)
}
