package mesh

import (
	"fmt"
	"reflect"
	"unsafe"

	"gitlab.com/eukano/runway/internal/geomath"
)

func Size(data interface{}) int  { return size(reflect.ValueOf(data)) }
func Count(data interface{}) int { return count(reflect.ValueOf(data), reflect.Value{}) }

func CountOf[V any](data interface{}) int {
	return count(reflect.ValueOf(data), reflect.ValueOf(new(V)).Elem())
}

func Raw(data interface{}) []byte {
	v := reflect.ValueOf(data)
	if v.Kind() == reflect.Array {
		panic("arrays are not addressable")
	}

	// also ensures data is a slice of fixed-sized objects
	n := size(v)
	return raw(v, n)
}

func ConcatRaw(data ...interface{}) []byte {
	var n int
	r := make([][]byte, len(data))
	for i, data := range data {
		v := reflect.ValueOf(data)
		if v.Kind() == reflect.Array {
			panic("arrays are not addressable")
		}

		m := size(v)
		r[i] = raw(v, m)
		n += m
	}

	b := make([]byte, n)
	n = 0
	for _, r := range r {
		n += copy(b[n:], r)
	}
	return b
}

func FlattenV[V any](data ...interface{}) []V {
	var n int
	for _, data := range data {
		n += CountOf[V](data)
	}

	flat := make([]V, n)
	fv := reflect.ValueOf(flat)
	n = 0
	for _, data := range data {
		n += flatten(fv.Slice(n, fv.Len()), reflect.ValueOf(data))
	}
	return flat
}

func FlattenTriI(data ...interface{}) []geomath.TriI {
	var n int
	for _, data := range data {
		n += CountOf[geomath.TriI](data)
	}

	flat := make([]geomath.TriI, n)
	fv := reflect.ValueOf(flat)
	n = 0
	for _, data := range data {
		n += flatten(fv.Slice(n, fv.Len()), reflect.ValueOf(data))
	}
	return flat
}

func raw(data reflect.Value, n int) []byte {
	var b []byte
	hdr := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	hdr.Data = data.Index(0).UnsafeAddr()
	hdr.Len, hdr.Cap = n, n
	return b
}

func size(data reflect.Value) int {
	switch data.Kind() {
	case reflect.Slice, reflect.Array:
		if data.Len() == 0 {
			return 0
		}

		e := data.Index(0)
		if e.Kind() == reflect.Slice {
			panic(fmt.Errorf("cannot get size of heterogenous slice %T", data))
		}
		return data.Len() * size(e)

	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
		reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64,
		reflect.Float32, reflect.Float64:
		return int(data.Type().Size())

	default:
		panic(fmt.Errorf("cannot get size of %T", data))
	}
}

func count(data, of reflect.Value) int {
	if of.IsValid() && data.Type() == of.Type() {
		return 1
	}

	switch data.Kind() {
	case reflect.Slice, reflect.Array:
		if data.Len() == 0 {
			return 0
		}

		e := data.Index(0)
		if e.Kind() == reflect.Slice {
			panic(fmt.Errorf("cannot get count of heterogenous slice %T", data))
		}
		return data.Len() * count(e, of)

	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
		reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64,
		reflect.Float32, reflect.Float64:
		if of.IsValid() {
			panic(fmt.Errorf("value is not composed of %T", of))
		}
		return 1

	default:
		panic(fmt.Errorf("cannot get count of %T", data))
	}
}

func flatten(dst, src reflect.Value) int {
	if src.Type().Elem().AssignableTo(dst.Type().Elem()) {
		// fmt.Printf("Copy %d to %d:%d\n", src.Len(), off, dst.Len())
		n := reflect.Copy(dst, src)
		// fmt.Printf("Copied %d\n", n)
		return n
	}

	switch src.Kind() {
	case reflect.Slice, reflect.Array:
		l := src.Len()
		n := 0
		for i := 0; i < l; i++ {
			n += flatten(dst.Slice(n, dst.Len()), src.Index(i))
		}
		return n

	default:
		panic(fmt.Errorf("source is not composed of %T", dst.Type().Elem()))
	}
}
