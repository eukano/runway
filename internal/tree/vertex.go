package tree

import (
	"math"

	"gitlab.com/eukano/runway/pkg/geom/tensor"
	"gonum.org/v1/gonum/spatial/kdtree"
)

// A VertexTree is a k-d tree of (vertex, data) pairs.
type VertexTree[V any] struct {
	*kdtree.Tree
}

// VertexData is a vertex and associated data.
type VertexData[V any] struct {
	Vertex tensor.Vec3
	Data   V
}

// NearestSet returns (vertex, data) pairs within some distance of V.
func (t VertexTree[V]) NearestSet(v tensor.Vec3, dist float64) []VertexData[V] {
	k := kdtree.NewDistKeeper(dist)
	t.Tree.NearestSet(k, vertex(v))

	r := make([]VertexData[V], k.Len())
	for i, c := range k.Heap {
		vd := c.Comparable.(kdVertexData[V])
		r[i] = VertexData[V]{tensor.Vec3(vd.vertex), vd.Data}
	}
	return r
}

// VertexDataSet is a slice of (vertex, data) pairs used to construct a vertex
// tree.
type VertexDataSet[V any] kdVertexDataSet[V]

// AsTree constructs a vertex tree from the data set.
func (s VertexDataSet[V]) AsTree() VertexTree[V] {
	kdt := kdtree.New(kdVertexDataSet[V](s), false)
	return VertexTree[V]{kdt}
}

// Append appends a (vertex, data) pair to the data set.
func (s VertexDataSet[V]) Append(v tensor.Vec3, data V) VertexDataSet[V] {
	return append(s, kdVertexData[V]{vertex(v), data})
}

type vertex tensor.Vec3
type getvec interface{ get() vertex }

type kdVertexData[V any] struct {
	vertex
	Data V
}

func (v vertex) Dims() int           { return 3 }
func (v vertex) get() vertex         { return v }
func (v vertex) cmp(u getvec) vertex { return vertex(tensor.Vec3(v).Sub(tensor.Vec3(u.get()))) }

func (v vertex) min(u vertex) vertex {
	return vertex{math.Min(v[0], u[0]), math.Min(v[1], u[1]), math.Min(v[2], u[2])}
}

func (v vertex) max(u vertex) vertex {
	return vertex{math.Max(v[0], u[0]), math.Max(v[1], u[1]), math.Max(v[2], u[2])}
}

func (v vertex) Compare(c kdtree.Comparable, i kdtree.Dim) float64 {
	return float64(v.cmp(c.(getvec))[i])
}

func (v vertex) Distance(c kdtree.Comparable) float64 {
	return float64(tensor.Vec3(v.cmp(c.(getvec))).LenSqr())
}

type kdVertexDataSet[V any] []kdVertexData[V]

// Implements kdtree.Interface
func (s kdVertexDataSet[V]) Len() int { return len(s) }

// Implements kdtree.Interface
func (s kdVertexDataSet[V]) Index(i int) kdtree.Comparable { return s[i] }

// Implements kdtree.Interface
func (s kdVertexDataSet[V]) Slice(i, j int) kdtree.Interface { return s[i:j] }

// Implements kdtree.Interface
func (s kdVertexDataSet[V]) Pivot(i kdtree.Dim) int {
	return kdVertexDataPlane[V]{s, i}.Pivot()
}

// Implements kdtree.Bounder
func (s kdVertexDataSet[V]) Bounds() *kdtree.Bounding {
	if len(s) == 0 {
		return nil
	}

	min := s[0].get()
	max := min
	for _, v := range s[1:] {
		v := v.get()
		min = min.min(v)
		max = max.max(v)
	}
	return &kdtree.Bounding{Min: min, Max: max}
}

type kdVertexDataPlane[V any] struct {
	Set kdVertexDataSet[V]
	Dim kdtree.Dim
}

// Implements sort.Interface
func (p kdVertexDataPlane[V]) Len() int { return p.Set.Len() }

// Implements sort.Interface
func (p kdVertexDataPlane[V]) Less(i, j int) bool {
	return p.Set[i].get()[p.Dim] < p.Set[j].get()[p.Dim]
}

// Implements sort.Interface
func (p kdVertexDataPlane[V]) Swap(i, j int) {
	p.Set[i], p.Set[j] = p.Set[j], p.Set[i]
}

// Implements kdtree.SortSlicer
func (p kdVertexDataPlane[V]) Slice(i, j int) kdtree.SortSlicer {
	return kdVertexDataPlane[V]{p.Set[i:j], p.Dim}
}

func (p kdVertexDataPlane[V]) Pivot() int {
	return kdtree.Partition(p, kdtree.MedianOfMedians(p))
}
