package tree

import (
	"gitlab.com/eukano/runway/pkg/geom/tensor"
	"gitlab.com/eukano/runway/pkg/gonum/rtree"
)

type BoundingTree[B Bounded, V any] struct {
	rtree.Tree
}

func NewBoundingTree[B Bounded, V any](opts *rtree.Options) *BoundingTree[B, V] {
	b := new(BoundingTree[B, V])
	if opts != nil {
		b.Options = *opts
	}
	return b
}

func (b *BoundingTree[B, V]) Insert(bounded B, data V) {
	b.Tree.Insert(BoundedData[B, V]{bounded, data})
}

func (b *BoundingTree[B, V]) Overlaps(bounded Bounded) []BoundedData[B, V] {
	k := rtree.NewOverlap(b2b(bounded.Bounds()))
	b.Tree.Search(k)
	return k2b[B, V](k.Items)
}

func (b *BoundingTree[B, V]) Within(bounded Bounded) []BoundedData[B, V] {
	k := rtree.NewContained(b2b(bounded.Bounds()))
	b.Tree.Search(k)
	return k2b[B, V](k.Items)
}

func k2b[B Bounded, V any](kept []rtree.Bounded) []BoundedData[B, V] {
	b := make([]BoundedData[B, V], len(kept))
	for i, v := range kept {
		b[i] = v.(BoundedData[B, V])
	}
	return b
}

type Bounded interface {
	Bounds() tensor.Span3
}

type SimpleBounds tensor.Span3

func (b SimpleBounds) Bounds() tensor.Span3 { return tensor.Span3(b) }

type BoundedData[B Bounded, V any] struct {
	Bounded B
	Data    V
}

func (b BoundedData[B, V]) Bounds() rtree.Bounding {
	return b2b(b.Bounded.Bounds())
}

func b2b(b tensor.Span3) rtree.Bounding {
	return rtree.Bounding{
		Min: vecPoint(b.Min),
		Max: vecPoint(b.Max),
	}
}

type vecPoint tensor.Vec3

func (v vecPoint) Dims() int         { return len(v) }
func (v vecPoint) Dim(i int) float64 { return v[i] }
