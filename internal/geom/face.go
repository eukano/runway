package geom

import (
	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// NewFace constructs a Face with length N. If N is 2-6, an instance of the
// corresponding fixed-size face type is returned. Otherwise, a Poly with length
// N is returned.
func NewFace(n int) manifold.Polygon {
	switch n {
	case 2:
		return &Line{}
	case 3:
		return &Tri{}
	case 4:
		return &Quad{}
	case 5:
		return &Pent{}
	case 6:
		return &Hex{}
	default:
		return make(Poly, n)
	}
}

// Line is a line segment.
type Line [2]tensor.Vec3

// Tri is a triangle.
type Tri [3]tensor.Vec3

// Quad is a quadrangle.
type Quad [4]tensor.Vec3

// Pent is a pentangle.
type Pent [5]tensor.Vec3

// Hex is a hexangle.
type Hex [6]tensor.Vec3

// Poly is a polygon.
type Poly []tensor.Vec3

// Len returns 2.
func (v *Line) Len() int { return len(v) }

// Len returns 3.
func (v *Tri) Len() int { return len(v) }

// Len returns 4.
func (v *Quad) Len() int { return len(v) }

// Len returns 5.
func (v *Pent) Len() int { return len(v) }

// Len returns 6.
func (v *Hex) Len() int { return len(v) }

// Len returns len(v).
func (v Poly) Len() int { return len(v) }

// Get returns the Ith vertex.
func (v *Line) Get(i int) tensor.Vec3 { return v[i] }

// Get returns the Ith vertex.
func (v *Tri) Get(i int) tensor.Vec3 { return v[i] }

// Get returns the Ith vertex.
func (v *Quad) Get(i int) tensor.Vec3 { return v[i] }

// Get returns the Ith vertex.
func (v *Pent) Get(i int) tensor.Vec3 { return v[i] }

// Get returns the Ith vertex.
func (v *Hex) Get(i int) tensor.Vec3 { return v[i] }

// Get returns the Ith vertex.
func (v Poly) Get(i int) tensor.Vec3 { return v[i] }
