package geom_test

import (
	"testing"

	"gitlab.com/eukano/runway/internal/geom"
	"gitlab.com/eukano/runway/internal/mesh"
	"gitlab.com/eukano/runway/internal/model"
	"gitlab.com/eukano/runway/internal/tree"
	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

type shapeFunc func() manifold.Polyhedron

var shapes = map[string]shapeFunc{
	"Cube":     func() manifold.Polyhedron { return geom.Cube(1, 1, 1, 2, 2, 2) },
	"Cylinder": func() manifold.Polyhedron { return geom.Cylinder(tensor.Vec3{1, 2, 3}, tensor.Vec3{3, 2, 1}) },
	"Torus":    func() manifold.Polyhedron { return geom.Torus(2, 4) },
	"Sphere":   func() manifold.Polyhedron { return geom.Sphere(1, tensor.Vec3{1, 1, 1}) },
}

func BenchmarkShapeConstruction(b *testing.B) {
	for name, fn := range shapes {
		b.Run(name, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				fn()
			}
		})
	}
}

func BenchmarkNormalFromAdjacent(b *testing.B) {
	for name, s := range shapes {
		shape := s()
		b.Run(name, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				model.NewShape("", shape, mesh.NormalFromAdjacent, nil)
			}
		})
	}
}

func BenchmarkTreeConstruction(b *testing.B) {
	for name, s := range shapes {
		shape := s()
		b.Run(name, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				treeFromVertices(shape)
			}
		})
	}
}

func BenchmarkTreeLookup(b *testing.B) {
	for name, s := range shapes {
		shape := s()
		tree := treeFromVertices(shape)
		q := shape.Get(shape.Len() / 2).Get(0)

		b.Run(name, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				tree.NearestSet(q, tensor.AdjacentEps)
			}
		})
	}
}

func treeFromVertices(s manifold.Polyhedron) tree.VertexTree {
	var set tree.VertexDataSet
	for i, n := 0, s.Len(); i < n; i++ {
		for _, v := range s.Get(i).Vec() {
			set = set.Append(v, nil)
		}
	}

	return set.AsTree()
}
