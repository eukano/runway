package geom

import (
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// Area returns V₁V₀ ✕ V₁V₂.
func (v *Tri) Area() tensor.Vec3 {
	return tensor.Area(v.Get(0), v.Get(1), v.Get(2))
}

// Project returns the projection of P onto the vector space V₁V₀, V₁V₂, and
// V₁V₀ ✕ V₁V₂.
func (v *Tri) Project(p tensor.Vec3) tensor.Vec3 {
	return tensor.ProjectVec(p, v.Get(0), v.Get(1), v.Get(2))
}

// Contains returns whether P is within the triangle.
func (v *Tri) Contains(p tensor.Vec3) bool {
	return tensor.Contains(p, v.Get(0), v.Get(1), v.Get(2))
}

// Area returns V₁V₀ ✕ V₁V₂.
func (v *FaceTri) Area() tensor.Vec3 {
	return tensor.Area(v.Get(0), v.Get(1), v.Get(2))
}

// Project returns the projection of P onto the vector space V₁V₀, V₁V₂, and
// V₁V₀ ✕ V₁V₂.
func (v *FaceTri) Project(p tensor.Vec3) tensor.Vec3 {
	return tensor.ProjectVec(p, v.Get(0), v.Get(1), v.Get(2))
}

// Contains returns whether P is within the triangle.
func (v *FaceTri) Contains(p tensor.Vec3) bool {
	return tensor.Contains(p, v.Get(0), v.Get(1), v.Get(2))
}
