package geom

import (
	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

// FaceTri is a triangle inscribed by three verticies of a face.
type FaceTri struct {
	Face  manifold.Polygon // Face is the face
	Index [3]int           // Index is the indices of the three vertices
}

// TriOf returns a FaceTri.
func TriOf(f manifold.Polygon, i, j, k int) *FaceTri {
	n := f.Len()
	return &FaceTri{f, [3]int{i % n, j % n, k % n}}
}

// Len returns 3
func (v *FaceTri) Len() int { return 3 }

// Get returns the Jth vertex of the face, where J is the Ith index of the
// triangle.
func (v *FaceTri) Get(i int) tensor.Vec3 { return v.Face.Get(v.Index[i]) }

// Has returns true if I is one of the triangle's indices.
func (v *FaceTri) Has(i int) bool {
	return v.Index[0] == i ||
		v.Index[1] == i ||
		v.Index[2] == i
}
