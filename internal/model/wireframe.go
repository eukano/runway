package model

import (
	"github.com/go-gl/gl/all-core/gl"
	"gitlab.com/eukano/runway/internal/glu"
)

type Wireframe struct {
	Model
	Cull bool
}

func (m *Wireframe) With(x ...Transform) Model {
	n := *m
	n.Model = m.Model.With(x...)
	return &n
}

func (m *Wireframe) Draw(g Globals) {
	if m.Cull {
		m.draw(g)
	} else {
		glu.Disable(gl.CULL_FACE, func() { m.draw(g) })
	}
}

func (m *Wireframe) draw(g Globals) {
	gl.PolygonMode(gl.FRONT_AND_BACK, gl.LINE)
	m.Model.Draw(g)
	gl.PolygonMode(gl.FRONT_AND_BACK, gl.FILL)
}
