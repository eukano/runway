package model

import (
	"fmt"
	"time"

	"gitlab.com/eukano/runway/pkg/geom/shape"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

type Model interface {
	Realize(func(func())) error
	Tick(delta, runtime time.Duration) bool
	With(...Transform) Model
	Draw(g Globals)

	Inspect() Summary
}

type Transform interface {
	Realize(delta, runtime time.Duration) tensor.Mat4
}

type Static shape.Transform
type Dynamic shape.DynamicTransform

func (x Static) Realize(_, _ time.Duration) tensor.Mat4   { return tensor.Mat4(x) }
func (x Dynamic) Realize(d, rt time.Duration) tensor.Mat4 { return x(d, rt) }

type Summary struct {
	Description     string
	VertexCount     int
	TriangleCount   int
	Transformations []Transform

	Within []Summary
}

func (s Summary) String() string {
	return fmt.Sprintf("%s {%d vertices, %d triangles}", s.Description, s.VertexCount, s.TriangleCount)
}

type Globals struct {
	Flatland tensor.Mat4
	World    World
	Lights   Lights
}

type World struct {
	Projection, View tensor.Mat4
	Camera           tensor.Vec3
}

type Light struct {
	Position tensor.Vec3
	Ambient  tensor.Vec3
	Diffuse  tensor.Vec3
	Specular tensor.Vec3
}

type Lights []Light

func (l Lights) get(get func(Light) tensor.Vec3) []tensor.Vec3 {
	v := make([]tensor.Vec3, len(l))
	for i := range v {
		v[i] = get(l[i])
	}
	return v
}

func (l Lights) Position() []tensor.Vec3 {
	return l.get(func(l Light) tensor.Vec3 { return l.Position })
}
func (l Lights) Ambient() []tensor.Vec3 { return l.get(func(l Light) tensor.Vec3 { return l.Ambient }) }
func (l Lights) Diffuse() []tensor.Vec3 { return l.get(func(l Light) tensor.Vec3 { return l.Diffuse }) }
func (l Lights) Specular() []tensor.Vec3 {
	return l.get(func(l Light) tensor.Vec3 { return l.Specular })
}
