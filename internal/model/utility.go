package model

import (
	"reflect"
	"time"

	"gitlab.com/eukano/runway/internal/assets"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

type modelMat4 tensor.Mat4

func (m *modelMat4) Get() tensor.Mat4 {
	return tensor.Mat4(*m)
}

func (m *modelMat4) Set(n tensor.Mat4) bool {
	if m.Get() == n {
		return false
	}

	*m = modelMat4(n)
	return true
}

type transformSet []Transform

func (s transformSet) Copy() transformSet {
	dst := make(transformSet, len(s))
	copy(dst, s)
	return dst
}

func (s transformSet) With(x ...Transform) transformSet {
	dst := make(transformSet, len(s)+len(x))
	copy(dst, s)
	copy(dst[len(s):], x)
	return dst
}

func (s transformSet) Realize(delta, runtime time.Duration) tensor.Mat4 {
	m := tensor.Ident4()
	for _, x := range s {
		m = x.Realize(delta, runtime).Mul4(m)
	}
	return m
}

var shared = map[reflect.Type]reflect.Value{}

func loadShared(v interface{}, cleanup func(func())) error {
	rv := reflect.ValueOf(v)
	if rv.Kind() != reflect.Ptr {
		panic("v must be a pointer")
	}

	typ := rv.Type().Elem()
	if sh, ok := shared[typ]; ok {
		rv.Elem().Set(sh.Elem())
		return nil
	}

	err := assets.Load(v, cleanup)
	if err != nil {
		return err
	}

	cleanup(func() { delete(shared, typ) })
	sh := reflect.New(typ)
	sh.Elem().Set(rv.Elem())
	shared[typ] = sh
	return nil
}

func deleteAndSetNil(v interface{}, cleanup func(func())) {
	rv := reflect.ValueOf(v)
	if rv.Kind() != reflect.Ptr {
		panic("v must be a pointer to interface{ Delete() }")
	}

	del, ok := rv.Elem().Interface().(interface{ Delete() })
	if !ok {
		panic("v must be a pointer to interface{ Delete() }")
	}

	cleanup(func() {
		del.Delete()
		rv.Elem().Set(reflect.Zero(rv.Type().Elem()))
	})
}
