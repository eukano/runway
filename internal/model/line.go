package model

import (
	"time"

	"github.com/go-gl/gl/all-core/gl"
	"gitlab.com/eukano/runway/internal/geomath"
	"gitlab.com/eukano/runway/internal/glu"
	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

func NewLine(desc string, vertex manifold.Polygon, color []tensor.Vec3, closed bool) *Line {
	return &Line{
		description: desc,
		vertexData:  manifold.Vertices(vertex),
		colorData:   color,
		loop:        closed,
	}
}

func NewLineIndexed(desc string, vertex manifold.Polygon, color []tensor.Vec3, index [][2]geomath.Index) *Line {
	return &Line{
		description: desc,
		vertexData:  manifold.Vertices(vertex),
		colorData:   color,
		indexData:   index,
	}
}

type lineBase struct {
	program    glu.Program `vert:"line.vert" frag:"line.frag"`
	position   glu.VertexAttrib
	color      glu.VertexAttrib
	ambient    glu.Uniform
	numLights  glu.Uniform
	model      glu.Uniform
	view       glu.Uniform
	projection glu.Uniform
}

type Line struct {
	description string
	loop        bool
	vertexData  []tensor.Vec3
	colorData   []tensor.Vec3
	indexData   [][2]geomath.Index
	transforms  transformSet

	// program
	lineBase
	vao glu.VertexArray

	// transient
	modelMat modelMat4
}

func (m *Line) Inspect() Summary {
	return Summary{
		Description:     m.description,
		VertexCount:     len(m.vertexData),
		Transformations: m.transforms.Copy(),
	}
}

func (m *Line) With(x ...Transform) Model {
	n := *m
	n.transforms = m.transforms.With(x...)
	return &n
}

func (m *Line) Tick(delta, runtime time.Duration) bool {
	return m.modelMat.Set(m.transforms.Realize(delta, runtime))
}

func (m *Line) Realize(cleanup func(func())) error {
	err := loadShared(&m.lineBase, cleanup)
	if err != nil {
		return err
	}

	if m.vao.ID() != 0 {
		return nil
	}

	m.vao = glu.NewVertexArray()
	deleteAndSetNil(&m.vao, cleanup)
	m.vao.Bind()

	if m.indexData != nil {
		ebo := glu.ElementArrayBuffer.NewWith(m.indexData, gl.STATIC_DRAW)
		defer ebo.Delete()
	}

	vbo := glu.ArrayBuffer.NewWith(reduce(m.vertexData), gl.STATIC_DRAW)
	defer vbo.Delete()
	m.position.AsArray(3, gl.FLOAT, false, 0, nil)
	m.position.Enable()

	if m.colorData != nil {
		cbo := glu.ArrayBuffer.NewWith(reduce(m.colorData), gl.STATIC_DRAW)
		defer cbo.Delete()
		m.color.AsArray(3, gl.FLOAT, false, 0, nil)
		m.color.Enable()

	} else {
		// no color, make it grey
		m.color.As3f(0.5, 0.5, 0.5)
	}

	m.vao.Unbind()
	return nil
}

func (m *Line) Draw(g Globals) {
	m.program.Use()
	m.vao.Bind()

	m.model.AsMatrix4fv(false, mat4(m.modelMat.Get()))

	m.projection.AsMatrix4fv(false, mat4(g.World.Projection))
	m.view.AsMatrix4fv(false, mat4(g.World.View))

	m.numLights.As1i(int32(len(g.Lights)))
	m.ambient.As3fv(vec3v(g.Lights.Ambient())...)

	glu.Disable(gl.CULL_FACE, func() {
		if m.indexData != nil {
			gl.DrawElements(gl.LINES, int32(len(m.indexData)*2), gl.UNSIGNED_SHORT, nil)
		} else if m.loop {
			gl.DrawArrays(gl.LINE_LOOP, 0, int32(len(m.vertexData)))
		} else {
			gl.DrawArrays(gl.LINE_STRIP, 0, int32(len(m.vertexData)))
		}
	})

	m.vao.Unbind()
	m.program.Unuse()
}
