package model

import (
	_ "image/jpeg"
	"time"

	"github.com/go-gl/gl/all-core/gl"
	"gitlab.com/eukano/runway/internal/assets"
	"gitlab.com/eukano/runway/internal/glu"
	"gitlab.com/eukano/runway/internal/mesh"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

type Skybox struct {
	// config
	transforms transformSet

	// program
	program         glu.Program `vert:"skybox.vert" frag:"skybox.frag"`
	texture         glu.Texture
	vao             glu.VertexArray
	positionIndex   glu.VertexAttrib `prog:"avPosition"`
	projectionIndex glu.Uniform      `prog:"uxProjection"`
	modelViewIndex  glu.Uniform      `prog:"uxModelView"`
	samplerIndex    glu.Uniform      `prog:"uSampler"`

	// transient
	modelMat modelMat4
}

func (m *Skybox) Inspect() Summary {
	return Summary{
		Description:     "Skybox",
		TriangleCount:   mesh.CountOf[[3]tensor.Vec3](mesh.CubeTriVertex[:]),
		VertexCount:     mesh.CountOf[tensor.Vec3](mesh.CubeTriVertex[:]),
		Transformations: m.transforms.Copy(),
	}
}

func (m *Skybox) With(x ...Transform) Model {
	n := *m
	n.transforms = m.transforms.With(x...)
	return &n
}

func (m *Skybox) Tick(delta, runtime time.Duration) bool {
	return m.modelMat.Set(m.transforms.Realize(delta, runtime))
}

func (m *Skybox) Realize(cleanup func(func())) error {
	err := assets.Load(m, cleanup)
	if err != nil {
		return err
	}

	m.texture, err = assets.CubeMap(map[glu.TextureTarget]string{
		glu.TextureCubeMapPosX: "right.jpg",
		glu.TextureCubeMapNegX: "left.jpg",
		glu.TextureCubeMapPosY: "top.jpg",
		glu.TextureCubeMapNegY: "bottom.jpg",
		glu.TextureCubeMapPosZ: "front.jpg",
		glu.TextureCubeMapNegZ: "back.jpg",
	})
	if err != nil {
		return err
	}
	deleteAndSetNil(&m.texture, cleanup)

	m.vao = glu.NewVertexArray()
	deleteAndSetNil(&m.vao, cleanup)
	m.vao.Bind()

	vbo := glu.ArrayBuffer.NewWith(reduce(mesh.CubeTriVertex[:]), gl.STATIC_DRAW)
	defer vbo.Delete()
	m.positionIndex.AsArray(3, gl.FLOAT, false, 0, gl.PtrOffset(0))
	m.positionIndex.Enable()

	m.vao.Unbind()

	return nil
}

func (m *Skybox) Draw(g Globals) {
	m.program.Use()
	m.vao.Bind()
	gl.ActiveTexture(gl.TEXTURE0)
	m.texture.Bind()

	m.samplerIndex.As1i(0)
	m.projectionIndex.AsMatrix4fv(false, mat4(g.World.Projection))
	m.modelViewIndex.AsMatrix4fv(false, mat4(g.World.View.Mul4(m.modelMat.Get())))

	gl.FrontFace(gl.CCW)
	count := mesh.CountOf[tensor.Vec3](mesh.CubeTriVertex[:])
	gl.DrawArrays(gl.TRIANGLES, 0, int32(count))
	gl.FrontFace(gl.CW)

	m.texture.Unbind()
	m.vao.Unbind()
	m.program.Unuse()
}
