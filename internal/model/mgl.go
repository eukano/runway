package model

import (
	"github.com/go-gl/mathgl/mgl32"
	"gitlab.com/eukano/runway/internal/mesh"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

func vec3(v tensor.Vec3) mgl32.Vec3 {
	var u mgl32.Vec3
	for i := range v {
		u[i] = float32(v[i])
	}
	return u
}

func vec3v(v []tensor.Vec3) []mgl32.Vec3 {
	u := make([]mgl32.Vec3, len(v))
	for i := range v {
		u[i] = vec3(v[i])
	}
	return u
}

func vec4(v tensor.Vec4) mgl32.Vec4 {
	var u mgl32.Vec4
	for i := range v {
		u[i] = float32(v[i])
	}
	return u
}

func vec4v(v []tensor.Vec4) []mgl32.Vec4 {
	u := make([]mgl32.Vec4, len(v))
	for i := range v {
		u[i] = vec4(v[i])
	}
	return u
}

func mat4(v tensor.Mat4) mgl32.Mat4 {
	var u mgl32.Mat4
	for i := range v {
		u[i] = float32(v[i])
	}
	return u
}

func reduce(v interface{}) []mgl32.Vec3 {
	return vec3v(mesh.FlattenV[tensor.Vec3](v))
}

func reduce4(v interface{}) []mgl32.Vec4 {
	return vec4v(mesh.FlattenV[tensor.Vec4](v))
}
