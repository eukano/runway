package model

import (
	"time"
)

type Body []Model

func (b Body) Inspect() Summary {
	var s Summary
	s.Description = "Body"
	for _, m := range b {
		ms := m.Inspect()
		s.TriangleCount += ms.TriangleCount
		s.VertexCount += ms.VertexCount
		s.Within = append(s.Within, ms)
	}
	return s
}

func (b Body) With(x ...Transform) Model {
	c := make(Body, len(b))
	for i := range b {
		c[i] = b[i].With(x...)
	}
	return c
}

func (b Body) Tick(delta, runtime time.Duration) bool {
	var update bool
	for _, m := range b {
		if m.Tick(delta, runtime) {
			update = true
		}
	}
	return update
}

func (b Body) Realize(cleanup func(func())) error {
	for _, m := range b {
		err := m.Realize(cleanup)
		if err != nil {
			return err
		}
	}
	return nil
}

func (b Body) Draw(g Globals) {
	for _, m := range b {
		m.Draw(g)
	}
}
