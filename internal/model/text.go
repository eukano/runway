package model

import (
	"fmt"
	"image"
	"image/draw"
	"log"
	"time"
	"unsafe"

	"github.com/go-gl/gl/all-core/gl"
	"gitlab.com/eukano/runway/internal/assets"
	"gitlab.com/eukano/runway/internal/glu"
	"gitlab.com/eukano/runway/pkg/geom/shape"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
	"golang.org/x/image/font"
	"golang.org/x/image/math/fixed"
)

type textProgram struct {
	program    glu.Program      `vert:"text.vert" frag:"text.frag"`
	position   glu.VertexAttrib `prog:"data"`
	text       glu.Uniform
	color      glu.Uniform
	projection glu.Uniform
	model      glu.Uniform
	vao        glu.VertexArray
	vbo        glu.Buffer
}

type Text struct {
	textProgram

	Font  font.Face
	Value string
	Color tensor.Vec4
	Pos   tensor.Vec2
	Size  float64

	chars [127 - ' ']textChar

	// transient
	modelMat tensor.Mat4
}

type textChar struct {
	*textProgram

	char    rune
	texture glu.Texture
	advance fixed.Int26_6
	metrics fixed.Rectangle26_6
	bounds  image.Rectangle
}

func (m *Text) Inspect() Summary {
	return Summary{
		Description:   "Text",
		VertexCount:   6,
		TriangleCount: 2,
	}
}

func (m *Text) With(x ...Transform) Model { return m }

func (m *Text) Tick(delta, runtime time.Duration) bool { return false }

func (m *Text) Realize(cleanup func(func())) error {
	if m.program.ID() != 0 {
		// already realized
		return nil
	}

	var err error
	for i := range m.chars {
		m.chars[i].textProgram = &m.textProgram
		err = m.chars[i].Realize(m.Font, ' '+rune(i), cleanup)
		if err != nil {
			return err
		}
	}

	err = assets.Load(&m.textProgram, cleanup)
	if err != nil {
		return err
	}
	deleteAndSetNil(&m.textProgram.program, cleanup)

	m.vao = glu.NewVertexArray()
	deleteAndSetNil(&m.vao, cleanup)
	m.vao.Bind()

	data := [2][3][2][2]float32{
		{
			{{0, 1}, {0, 0}},
			{{1, 0}, {1, 1}},
			{{0, 0}, {0, 1}},
		},
		{
			{{0, 1}, {0, 0}},
			{{1, 1}, {1, 0}},
			{{1, 0}, {1, 1}},
		},
	}

	vbo := glu.ArrayBuffer.NewWith(data[:], gl.STATIC_DRAW)
	defer vbo.Delete()
	m.position.AsArray(4, gl.FLOAT, false, 4*int32(unsafe.Sizeof(float32(0))), nil)
	m.position.Enable()

	m.vao.Unbind()
	return nil
}

func (m *textChar) Realize(face font.Face, c rune, cleanup func(func())) error {
	m.char = c

	var ok bool
	m.metrics, m.advance, ok = face.GlyphBounds(c)
	if !ok {
		return fmt.Errorf("font does not have glyph %q (%d)", string(c), c)
	}

	_, mask, maskp, _, ok := face.Glyph(fixed.Point26_6{}, c)
	if !ok {
		return fmt.Errorf("font does not have glyph %q (%d)", string(c), c)
	}
	m.bounds = mask.Bounds()

	if m.bounds.Empty() {
		// non-printing
		return nil
	}

	dst := image.NewRGBA(m.bounds)
	src := image.White
	draw.DrawMask(dst, m.bounds, src, image.Point{}, mask, maskp, draw.Src)

	m.texture = glu.Texture2D.New()
	cleanup(m.texture.Delete)
	m.texture.Bind()
	m.texture.Target.LoadImage2D(dst, 0, gl.RED, 0)
	m.texture.Target.Parameteri(gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	m.texture.Target.Parameteri(gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	m.texture.Target.Parameteri(gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	m.texture.Target.Parameteri(gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	m.texture.Unbind()
	return nil
}

func (m *Text) Draw(g Globals) {
	m.program.Use()
	m.vao.Bind()

	gl.ActiveTexture(gl.TEXTURE0)
	m.text.As1i(0)

	m.projection.AsMatrix4fv(false, mat4(g.Flatland))
	m.color.As4f(float32(m.Color[0]), float32(m.Color[1]), float32(m.Color[2]), float32(m.Color[3]))

	var scale float64 = m.Size / 2
	var x, y = m.Pos[0], m.Pos[1]
	for _, c := range m.Value {
		if int(c-' ') > len(m.chars) {
			log.Printf("Cannot print character %q", string(c))
			c = '?'
		}

		tc := m.chars[c-' ']
		tc.Draw(x, y, scale)

		x += fixed2float(tc.advance) * scale
	}

	m.vao.Unbind()
	m.program.Unuse()
}

func (m *textChar) Draw(x, y, scale float64) {
	if m.bounds.Empty() {
		// non-printing
		return
	}

	xpos := x + fixed2float(m.metrics.Min.X)*scale
	ypos := y - fixed2float(m.metrics.Max.Y)*scale
	w := float64(m.bounds.Dx()) * scale
	h := float64(m.bounds.Dy()) * scale

	size := shape.Scale(w, h, 1).Mat()
	move := shape.Translate(xpos, ypos, 0).Mat()
	m.model.AsMatrix4fv(false, mat4(move.Mul4(size)))

	gl.BindTexture(gl.TEXTURE_2D, m.texture.ID())
	glu.Disable(gl.CULL_FACE, func() { gl.DrawArrays(gl.TRIANGLES, 0, 6) })
	gl.BindTexture(gl.TEXTURE_2D, 0)
}

func fixed2float(v fixed.Int26_6) float64 {
	const fpart, fmask = 6, 1<<6 - 1
	return float64(v>>fpart) + float64(v&fmask)/(1<<fpart)
}
