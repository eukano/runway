package model

import (
	"github.com/go-gl/gl/all-core/gl"
	"gitlab.com/eukano/runway/internal/glu"
	"gitlab.com/eukano/runway/internal/mesh"
	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/shape"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

func Illuminati() *Mesh {
	data := [2][3]tensor.Vec3{
		{
			{+0.0, +0.5, +0.0},
			{+0.5, -0.5, +0.0},
			{-0.5, -0.5, +0.0},
		},
		{
			glu.Color.C,
			glu.Color.M,
			glu.Color.Y,
		},
	}
	copy(data[0][:], manifold.Vertices(shape.Circle(3, 1, tensor.Vec3{}, tensor.Vec3{0, 0, 1})))

	bind := func(position, normal, color glu.VertexAttrib) (vao glu.VertexArray, err error) {
		vao = glu.NewVertexArray()
		vao.Bind()

		vbo := glu.ArrayBuffer.NewWith(reduce(data[:]), gl.STATIC_DRAW)
		defer vbo.Delete()

		position.AsArray(3, gl.FLOAT, false, 0, gl.PtrOffset(0))
		color.AsArray(3, gl.FLOAT, false, 0, gl.PtrOffset(mesh.Size(data[0])))
		normal.As3f(0, 0, 1)

		position.Enable()
		color.Enable()

		vao.Unbind()
		return vao, nil
	}

	return &Mesh{
		description:   "Triangle",
		vertexCount:   3,
		triangleCount: 1,
		bind:          bind,
		noCull:        true,
	}
}
