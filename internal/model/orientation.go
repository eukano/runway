package model

import (
	"math"
	"time"

	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

type Movement int

const (
	Forward  Movement = +1
	None     Movement = 0
	Backward Movement = -1
)

func update(v *float64, r float64, d time.Duration, m Movement) {
	*v += r * d.Seconds() * float64(m)
}

type Heading struct {
	Vertical, Horizontal float64
	RadPerSec            struct {
		V, H float64
	}
}

func (h *Heading) SetDeg(v tensor.Vec2) *Heading {
	h.Horizontal = float64(v[0]) / 180 * math.Pi
	h.Vertical = float64(v[1]) / 180 * math.Pi
	return h
}

func (h *Heading) Update(delta time.Duration, mV, mH Movement) {
	update(&h.Vertical, h.RadPerSec.V, delta, mV)
	update(&h.Horizontal, h.RadPerSec.H, delta, mH)
}

func (h *Heading) Orientation() *Orientation {
	lv := h.Vertical
	lh := h.Horizontal

	look := tensor.Vec3{
		math.Cos(lv) * -math.Sin(lh),
		math.Sin(lv),
		math.Cos(lv) * -math.Cos(lh),
	}
	right := tensor.Vec3{
		math.Cos(lh),
		0,
		-math.Sin(lh),
	}
	up := right.Cross(look)

	return &Orientation{
		Look:  look,
		Right: right,
		Up:    up,
	}
}

type Orientation struct {
	Look, Right, Up tensor.Vec3
}

type Position struct {
	MovePerSec float64
	Value      tensor.Vec3
	View       tensor.Mat4
}

func (p *Position) Update(delta time.Duration, mRight, mUp, mLook Movement, o *Orientation) {
	move := p.MovePerSec * delta.Seconds()
	vR := o.Right.Mul(move * float64(mRight))
	vU := o.Up.Mul(move * float64(mUp))
	vL := o.Look.Mul(move * float64(mLook))

	p.Value = p.Value.Add(vR).Add(vU).Add(vL)
	p.View = tensor.LookAt(p.Value, p.Value.Add(o.Look), o.Up)
}
