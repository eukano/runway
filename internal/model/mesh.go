package model

import (
	"time"

	"github.com/go-gl/gl/all-core/gl"
	"gitlab.com/eukano/runway/internal/geomath"
	"gitlab.com/eukano/runway/internal/glu"
	"gitlab.com/eukano/runway/internal/mesh"
	"gitlab.com/eukano/runway/pkg/geom/manifold"
	"gitlab.com/eukano/runway/pkg/geom/tensor"
)

type MeshOptions struct {
	Description string
	NoCull      bool
	Vertex      []tensor.Vec3
	Normal      []tensor.Vec3
	Color       []tensor.Vec4
	Index       geomath.Mesh
}

func NewShape(desc string, shape manifold.Polyhedron, fnorm func(manifold.Polyhedron) mesh.Normalizer, fcolor func(manifold.Polyhedron) mesh.Colorizer) *Mesh {
	m := mesh.Build(shape, fnorm, fcolor)

	return newMesh(MeshOptions{
		Description: desc,
		Vertex:      m.Vertex,
		Normal:      m.Normal,
		Color:       m.Color,
		Index:       m.Index,
	})
}

func newMesh(opt MeshOptions) *Mesh {
	m := new(Mesh)
	m.description = opt.Description
	m.noCull = opt.NoCull
	m.indexed = opt.Index != nil

	if m.indexed {
		m.triangleCount = len(opt.Index)
		m.vertexCount = len(opt.Vertex)
	} else {
		m.triangleCount = len(opt.Vertex)
		m.vertexCount = len(opt.Vertex) * 3
	}

	m.bind = func(av, an, ac glu.VertexAttrib) (vao glu.VertexArray, err error) {
		vao = glu.NewVertexArray()
		vao.Bind()

		if m.indexed {
			ebo := glu.ElementArrayBuffer.NewWith(opt.Index, gl.STATIC_DRAW)
			defer ebo.Delete()
		}

		// must have vertices
		vbo := glu.ArrayBuffer.NewWith(reduce(opt.Vertex), gl.STATIC_DRAW)
		defer vbo.Delete()
		av.AsArray(3, gl.FLOAT, false, 0, nil)
		av.Enable()

		if opt.Normal == nil {
			an.As3f(0, 0, 0)

		} else if len(opt.Vertex) == len(opt.Normal) && &opt.Vertex[0] == &opt.Normal[0] {
			// normals === verticies, bind normal attrib to VBO
			an.AsArray(3, gl.FLOAT, false, 0, nil)
			an.Enable()

		} else {
			nbo := glu.ArrayBuffer.NewWith(reduce(opt.Normal), gl.STATIC_DRAW)
			defer nbo.Delete()
			an.AsArray(3, gl.FLOAT, false, 0, nil)
			an.Enable()
		}

		if opt.Color == nil {
			// no color, make it grey
			ac.As4f(0.5, 0.5, 0.5, 1.0)

		} else {
			// bind colors
			cbo := glu.ArrayBuffer.NewWith(reduce4(opt.Color), gl.STATIC_DRAW)
			defer cbo.Delete()
			ac.AsArray(4, gl.FLOAT, false, 0, nil)
			ac.Enable()
		}

		vao.Unbind()
		return vao, nil
	}

	return m
}

type meshBase struct {
	program       glu.Program `vert:"mesh.vert" frag:"mesh.frag"`
	position      glu.VertexAttrib
	color         glu.VertexAttrib
	normal        glu.VertexAttrib
	specular      glu.Uniform
	ambient       glu.Uniform
	diffuse       glu.Uniform
	numLights     glu.Uniform
	lightPosition glu.Uniform
	viewPosition  glu.Uniform
	model         glu.Uniform
	view          glu.Uniform
	projection    glu.Uniform
	normalAdj     glu.Uniform
}

type MeshBinder func(position, normal, color glu.VertexAttrib) (vao glu.VertexArray, err error)

type Mesh struct {
	// config
	description   string
	vertexCount   int
	triangleCount int
	indexed       bool
	noCull        bool
	transforms    transformSet
	bind          MeshBinder

	// program
	meshBase
	vao glu.VertexArray

	// transient
	modelMat modelMat4
}

func (m *Mesh) Inspect() Summary {
	return Summary{
		Description:     m.description,
		VertexCount:     m.vertexCount,
		TriangleCount:   m.triangleCount,
		Transformations: m.transforms.Copy(),
	}
}

func (m *Mesh) With(x ...Transform) Model {
	n := *m
	n.transforms = m.transforms.With(x...)
	return &n
}

func (m *Mesh) Tick(delta, runtime time.Duration) bool {
	return m.modelMat.Set(m.transforms.Realize(delta, runtime))
}

func (m *Mesh) Realize(cleanup func(func())) error {
	err := loadShared(&m.meshBase, cleanup)
	if err != nil {
		return err
	}

	if m.vao.ID() != 0 {
		return nil
	}

	m.vao, err = m.bind(m.position, m.normal, m.color)
	if m.vao.ID() != 0 {
		deleteAndSetNil(&m.vao, cleanup)
	}
	return err
}

func (m *Mesh) DrawTriangles() {
	if m.indexed {
		gl.DrawElements(gl.TRIANGLES, int32(m.triangleCount)*3, gl.UNSIGNED_SHORT, nil)
	} else {
		gl.DrawArrays(gl.TRIANGLES, 0, int32(m.triangleCount)*3)
	}
}

func (m *Mesh) Setup(g Globals) {
	m.program.Use()
	m.vao.Bind()

	m.model.AsMatrix4fv(false, mat4(m.modelMat.Get()))
	m.normalAdj.AsMatrix4fv(false, mat4(m.modelMat.Get().Inv().Transpose()))

	m.projection.AsMatrix4fv(false, mat4(g.World.Projection))
	m.view.AsMatrix4fv(false, mat4(g.World.View))
	m.viewPosition.As3fv(vec3(g.World.Camera))

	m.numLights.As1i(int32(len(g.Lights)))
	m.lightPosition.As3fv(vec3v(g.Lights.Position())...)
	m.ambient.As3fv(vec3v(g.Lights.Ambient())...)
	m.diffuse.As3fv(vec3v(g.Lights.Diffuse())...)
	m.specular.As3fv(vec3v(g.Lights.Specular())...)
}

func (m *Mesh) Teardown() {
	m.vao.Unbind()
	m.program.Unuse()
}

func (m *Mesh) Draw(g Globals) {
	m.Setup(g)

	if m.noCull {
		glu.Disable(gl.CULL_FACE, m.DrawTriangles)
	} else {
		m.DrawTriangles()
	}

	m.Teardown()
}
